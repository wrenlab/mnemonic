import os
from setuptools import setup, find_packages

VERSION = "0.1.0"

DEPENDENCIES = [
    "numpy",
    "scipy",
    "pandas",
    "patsy",
    "statsmodels",
    "joblib",
    "seaborn",
    "pyyaml",
    "sqlalchemy",
    "rpy2",
    "pylatexenc",
    "bidict",
    "click",
    "fancyimpute",
    "sklearn",
    "tqdm",
    "tzlocal",
    "scikit-bio",
    "xlrd",
]

####################
# Package definition
####################

setup(
    name="mnemonic",
    version=VERSION,
    description="EBI metagenomics meta-analysis",
    author="Aleksandra Perz and Cory Giles",
    author_email="aleksandra-perz@omrf.org",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.6",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ],
    license="AGPLv3+",

    include_package_data=False,
    packages=find_packages(),
    entry_points={
        "console_scripts": ["mnemonic=mnemonic.cli:main"]
    },
    install_requires=DEPENDENCIES,
    setup_requires=["pytest-runner"],
    tests_require=["pytest"]
)
