#!/usr/bin/env python3

'''
Tools for manipulating metadata, notably adding keywords.
Command line usage: python3 metadata_samples.py --help
'''

import pandas as pd
import numpy as np
import argparse
import pickle
import sys
import os
import subprocess as sp
import re
from math import isnan
from toolbox import *

################################################################################
### Metadata general operations

def s2p(s_list, md):
    md = md[['sample_id', 'project_id']].drop_duplicates()
    if isinstance(s_list, str):
        return set(md.project_id[md.sample_id == s_list])
    else:
        return set(md.project_id[[s in s_list for s in md.sample_id]])


def p2s(p_list, md):
    md = md[['sample_id', 'project_id']].drop_duplicates()
    if isinstance(p_list, str):
        return set(md.sample_id[md.project_id == p_list])
    else:
        return set(md.sample_id[[p in p_list for p in md.project_id]])




################################################################################
### Metadata keywords
def pool_words_from_pd(df, use_cols='all'):
    '''
    Retunrs a list of all words (with duplicates) from the selected columns
    of a pandas.DataFrame.
    '''
    all_words = []
    to_exclude = ['null', ' ', '-']
    to_strip = ['"', ":", ";", " ", "?", ".", ",", "(", ")"]
    #split_on = [' ', '/']

    if use_cols != 'all':
        df = df[use_cols].drop_duplicates()

    for c in df:
        all_words += [i for i in df[c]]

    all_words = [i for i in all_words if not pd.isnull(i)]

    # split first
    all_words = pool_multilevel_list([
                    i.split(" ") for i in all_words
                    ])

    all_words = [i.lower() for i in all_words]

    for s in to_strip:
        all_words = [i.strip(s) for i in all_words]

    all_words = [i for i in all_words if i not in to_exclude]
    return all_words


def word_summary_from_metadata(md):
    use_cols = ['project_name', 'experimental_factor',
                'study_linkout', 'study_abstract', 'biome']
    o = pool_words_from_pd(md, use_cols)
    return pd.Series(o).value_counts()


def get_keyword_groups():
    o = {
    'animal': ['cattle', 'rodent', 'mouse', 'rat', 'sheep', 'seal', 'insect',
                'animal', 'mammal'],
    'disease': ['obesity', 'diabetes', 'disease', 'infection', 'crohn'],
    'environmental': ['bioreactor', 'rhizosphere', 'soil', 'sand', 'oil',
                'aquatic', 'saline'],
    }
    return o

def create_dictionary(keywords_path):
    '''
    keywords_path:  path to a file with all the keywords listed on separate lines

    Returns a dictionary with keys being the representative keywords and values
    being all the words belonging to a keyword, e.g. d['intestine'] = ['intestine', ['gut']
    '''
    d = pd.read_csv(keywords_path, sep=" ", header = None)
    d.index = d.ix[:,0]

    d = d.to_dict()[0]
    d = {k:[v] for k, v in d.items()}

    # remove commented lines
    d = {key:val for key, val in d.items() if not key.startswith("#")}

    # problematic: oral, crohn, disease, hiv, rat, sheep

    keywords_to_merge = [
        ['intestine', 'gut'], ['intestine', 'fecal'], ['intestine', 'faecal'],
        ['intestine', 'intestinal'], ['intestine', 'stool'], #intestine, american
        ['diet', 'calorie'], ['diet', 'caloric'],
        ['obesity', 'obese'],
        ['diarrhea', 'diarrhoea'],
        ['diabetes', 'diabetic'], ['diabetes', 't2d'],
        ['aging', 'elderly'], ['aging', 'ageing'], ['aging', 'old'],
        ['aging', 'senescence'], ['aging', 'senescent'],
        ['infant', 'baby'], ['infant', 'postnatal'],
        ['disease', 'diseased'],
        ['vascular', 'hypertension'], ['vascular', 'hypertensive'],
        ['infection', 'clostridium'], ['infection', 'helicobacter'],
        ['nasal', 'olfactory'], ['lung', 'pulmonary'], ['nasal', 'nare'], ['nasal', 'nostril'], ['skin', 'forehead'], ['skin', 'hand'],
        ['oral', 'mouth'], ['vagina', 'vaginal'],
        ['renal', 'kidney'], ['renal', 'diurnal'],
        #['mouse', 'mice'], ['mouse', 'rodent'],
        ['rodent', 'mice'], ['rodent', 'mouse'], ['rodent', 'rat'], ['rodent', 'murine'],
        ['coprolite', 'coprolithe'],
        ['cecum', 'ceca'],
        ['cattle', 'cow'], ['cattle', 'buffalo'], ['mammal', 'mammalian'], ['rumen', 'ruminal'],
        ['insect', 'insecta'], ['insect', 'ant'], ['insect', 'arthropoda'],
        ['rhizosphere', 'root'], ['rhizosphere', 'plant'],
        ['freshwater', 'lake'],
        ['water', 'aquatic'], ['wetland', 'marsh'],
        ['marine', 'oceanic'], ['marine', 'sea'], ['marine', 'ocean'],
        ['saline', 'salt'], ['saline', 'hypersaline'], ['thermal', 'hydrothermal'], ['thermal', 'hot'],
        ['oil', 'hydrocarbon'], ['oil', 'biogas'],
        ['dairy', 'cheese'],
        ['increase', 'increased'], ['increase', 'enrich'], ['increase', 'enriched'],
        ['decrease', 'reduced'], ['decrease', 'decreased'], ['decrease', 'reduce'],
        ['control', 'con'],
        ['behavior', 'aggression'], ['behavior', 'aggressive'],
        ['behavior', 'behaviour'], ['behavior', 'behavioural'], ['behavior', 'behavioral'],
        ['neurodegenerative', 'alzheimer'], ['neurodegenerative', 'memory'],
        ['neurodegenerative', 'dementia'], ['neurodegenerative', 'parkinson'],
        ['mental', 'delusion'], ['mental', 'delusional'],
        ['mental', 'psychosis'], ['mental', 'psychotic'],
        ['depression', 'depressive'],
        ['altered', 'alter'],
        ['archaea', 'archaeal'],

    ]


    #keywords_to_delete = ['solstice', 'workshop', 'young', 'positive', 'negative', 'non-tumor',
    #    'patient', 'infection', 'control', 'adult', 'infant', 'disease', 'diagnostic', 'american'] # young is rhizosphere
    keywords_to_delete = ['highly', 'hydrogen']
    #keywords_to_delete = []



    for k in keywords_to_merge: # k is [key to merge to, key to merge]
        d[k[0]].append(k[1])
        d.pop(k[1])

    for k in keywords_to_delete:
        d.pop(k)

    #print(len(d))
    return d


def add_keywords_to_metadata(metadata, dictionary):
    '''
    Returns md with an additional column 'keywords'
    '''
    md = metadata
    #use_columns = ['sample_name'] #ATT
    use_columns = ['project_name', 'study_linkout', 'experimental_factor',
                    'study_abstract', 'biome', 'sample_name']

    if 'sample_id' in md.columns:
        md.index = md.sample_id
        md = md.drop('sample_id', 1)

    md = md[use_columns]

    # match with words in Dictionary
    md_dict = md.T.apply(lambda r:
                " ".join(
                        match_all_keywords(
                                    dictionary,
                                    " | ".join([str(r_) for r_ in r])
                                    )
                        )
                ).T

    kwds = pd.DataFrame(md_dict, columns = ['keywords'])
    o = pd.merge(metadata,
            kwds,
            right_index=True, left_index=True
            )
    return o

def match_all_keywords(dictionary, string):
    '''
    Returns list of matched keywords.
    for words ending with -y, match -y  or -s (kindeys) or -ies
    for all words, match also word+s and word+es (tomatoes)
    '''

    o = []
    for kw, v in dictionary.items():

        pattern = "|".join([
            #"|".join([ r'\b' + v_[:-1] + suffix + r'\b'
                        #for suffix in ['', 's', 'ys', 'ies'] ])
            "\\b" + v_[:-1] + "(|y|ys|ies)\\b"

            if v_[-1] == 'y'
            else
            #"|".join([ r'\b' + v_ + suffix + r'\b'
                        #for suffix in ['', 's', 'es'] ])
            "\\b" + v_ + "(|s|es)\\b"

            for v_ in v
            ])

        if re.search(pattern, string, re.IGNORECASE) != None:
            #print(pattern)
            #print(kw)
            o.append(kw)

    return o


def binarize_keyword_metadata(md_with_keywords):
    o = {}
    cols = restore_keywords_from_metadata(md_with_keywords, keyword_colname='keywords')
    for c in cols:
        o[c] = has_keyword(md_with_keywords, c)

    return pd.DataFrame.from_dict(o)


def is_in_keyword_list_raw(kw, keywords_path='keywords_list_all'):
    d = set(pd.read_csv(keywords_path, sep=" ", header = None)[0])
    return kw in d

def is_in_dictionary(kw, keywords_path='keywords_list_all'):
    d = create_dictionary(keywords_path)
    return kw in d.keys()


def has_keyword(md_with_keywords, keyword, keyword_colname="keywords"):
    '''
    Returns a boolean pd.Series of whether a keyword is found for a sample.
    '''
    #sample_colname='sample_id'
    #md_with_keywords.index = md_with_keywords[sample_colname]
    return md_with_keywords[keyword_colname]\
            .apply(lambda x: bool(str(x).find(keyword) != -1))

def projects_for_keyword(md, kw):
    o = md[['project_id', 'keywords']].drop_duplicates()
    o = o.ix[has_keyword(o, kw)]
    return set(o.project_id)


def list_all_keywords(md, keyword_colname = "keywords"):
    return set(" ".join([str(i) for i in set(md[keyword_colname])]).split(" "))

def is_in_keyword_group(md_with_keywords, keyword_group, keyword_colname="keywords"):
    '''
    Returns a boolean pd.Series of whether one of the keywords (e.g. 'mouse')
    constituting a keyword group (e.g. 'animal') is found for a sample.
    '''
    k = get_keyword_groups()[keyword_group]
    d = {}
    for k_ in k:
        d[k_] = has_keyword(md_with_keywords, k_, keyword_colname)

    d = pd.DataFrame.from_dict(d)
    o = d.sum(1).apply(lambda x: x != 0)
    return o

def subset_samples_with_keyword(A, sample_list_or_keyword = 'intestine', md = None):
    if isinstance(sample_list_or_keyword, str):
        sample_list_or_keyword = md[has_keyword(md, 'intestine')]\
                                    .sample_id.drop_duplicates()
    return A[[i in sample_list_or_keyword for i in A.index]]




def main(metadata_path, output_path, keywords_path):
    md = pd.read_csv(metadata_path, sep="\t", index_col=False)
    d = create_dictionary(keywords_path)
    print("--- Matching and adding keywords (this will take several minutes, 10 or so)...")
    md_kw = add_keywords_to_metadata(md, d)

    print("--- Saving data frame...")
    md_kw.to_csv(output_path, sep="\t", index=False)
    print("--- Done.")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-m", "--metadata_path",
                        help="Output of metadata_download.py with cols like sample_name etc",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        help="Output metadata; like the input but with keywords columns",
                        required=True)
    parser.add_argument("-k", "--keywords_path",
                        help="Path to a file with all the keywords listed on separate lines",
                        required=True)
    args = vars(parser.parse_args())

    metadata_path   = args['metadata_path']
    output_path      = args['output_path']
    keywords_path = args['keywords_path']
    tmp_path        = "cache/metadata_add_dictionary_TMPFILE"
    helper_scripts_dir = "helper" # depr

    main(metadata_path, output_path, keywords_path)
