#!/usr/bin/env python3

'''
Take all of the regular files present in any depth within the input directory,
extract them and move to the output directory.
'''

import os
import subprocess
import argparse

def move_all_files_to(idir, odir):
    odir = odir#+"/"
    for fi in os.listdir(idir):
        if os.path.isdir(idir+"/"+fi):
            print(idir+"/"+fi+" is a dir")
            move_all_files_to(idir+"/"+fi, odir)
        elif os.path.isfile(idir+"/"+fi):
            #print(fi)
            subprocess.call("cp '{0}' '{1}'".format(idir+"/"+fi, odir+"/"+fi), shell=True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
                        ")
    parser.add_argument("-i", "--input_dir",
                        help="",
                        required=False)
    parser.add_argument("-o", "--output_dir",
                        default = "",
                        help="",
                        required=True)
    args = vars(parser.parse_args())

    idir = args['input_dir']
    odir = args['output_dir']

    if not os.path.isdir(odir):
        os.mkdir(odir)

    move_all_files_to(idir, odir)

    print("---The next step is to produce a count matrix with abundance_matrix_make.py")
