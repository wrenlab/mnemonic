#!/usr/bin/env python3

###
#TODO
# metadata truncated: example ERP001506, sample_name
# too many numbers on sample_name: ERP005635

# Runtime for all metadata: parallelise?

#TODO compare to clustering - unsupervised grouping
#TODO Levenstein distance with all textual metadata?
###


'''

r_['counts'] = r_.groupby(['words']).transform('count') #interesting
'''


import pandas as pd
import argparse
from collections import Counter
import re
import sys
import os
from os.path import splitext
from difflib import SequenceMatcher
from sklearn.neighbors import NearestNeighbors
import numpy as np

from sklearn.cluster import MeanShift, estimate_bandwidth, AffinityPropagation, DBSCAN
from skrzynka import mx_for_prj, pool_multilevel_list


def compare_strings(m):
    result = {}
    for project in use_projects:
        sample_list = m.sample_id[m.project_id == project].tolist()
        similarity = pd.DataFrame(index = sample_list)
        # naive
        for sample1 in sample_list:
            print(1, sample1)
            new_col = []
            for sample2 in sample_list:
                #print(2, sample2)
                new_col.append(
                        SequenceMatcher(None,
                            m.sample_name[m.sample_id == sample1].tolist()[0],
                            m.sample_name[m.sample_id == sample2].tolist()[0]
                            ).ratio()
                        )

            print(new_col)
            similarity.insert(0, sample1, new_col)

        result[project] = similarity
    return result


def insert_groups_word_count(X, column_name, project_id, add_project_id = True):
    '''
    Get unique groups based on counted words. Each unique combination of words is a
    separate group.

    Input: data frame with counted words subsetted only to one project
    Output: Same portion with group names for groups with same values in "column_name"
    '''
    y = X[[column_name]].drop_duplicates()
    y.insert(0, "group_word_count", range(y.shape[0])) # counts how many different groups there are

    if add_project_id:
        y.group_word_count = [str(project_id)+"_"+str(i) for i in y.group_word_count]

    return pd.merge(X, y, on=column_name)


def count_words_2(m, use_projects, md_col_loc, add_project_id):
    #TODO if no words at all selected then use a differenet metadate field
    # or: pool all words and manually determine which may be used as a trmt label
    result = {}
    for i,project in enumerate(use_projects): # better use groupby
        if i % 100 ==0:
            print("--- --- Processing project", i, "out of", len(use_projects))

        m_ = m.ix[m.project_id == project, :]

        # fro sample:
        words_for_sample = {}
        sample_list = m_.sample_id.tolist()
        if len(sample_list) == 0:
            grouped = None
        else:
            for sample in sample_list:
                # split letters and numbers, use '(\d+)' for the first occurence of a digit
                #words_for_sample_ =  re.split('(\d+)',
                #TODO splitting: make so that it only splits if the digit is at the beginning or the end of word
                words_for_sample_ = re.split('(\d+)| ',
                                        re.sub("\W|_", " ",
                                            #m_.ix[m_.sample_id == sample, md_col_loc]\ # F2
                                            #F1
                                            m_.ix[m_.sample_id == sample, [md_col_loc]]\
                                            .to_string(header=False, index = False)\
                                            .lower()
                                        )
                                    )
                # remove None's and ""'s
                words_for_sample_ = [i for i in words_for_sample_ if not (i == None or i == "")]
                words_for_sample[sample] = words_for_sample_

            # remove the entries that are non-specific (their number == n_samples in prj)
            # and the ones that are sample-specific (count ==1)
            word_counts = dict(
                                Counter(
                                    pool_multilevel_list([i for i in words_for_sample.values()])
                                )
                            )

            words_to_remove = [k for k,v in word_counts.items()\
                                    if (v >= len(words_for_sample)\
                                    or v < int(len(words_for_sample)/10)\
                                    or v < 2)
                                ] #TODO if more than one field is considered, more conditions

            words_for_sample = {k:'_'+'_'.join([v_ for v_ in v if v_ not in words_to_remove]) for k,v in words_for_sample.items()} # mke into strings
            # if dict.value length == 0, insert "_"

            # turn into a data frame
            grouped = pd.DataFrame.from_dict(words_for_sample, orient = "index")

            grouped.insert(0, "sample_id", grouped.index)
            grouped.insert(0, "project_id", project)
            grouped.columns = ("project_id", "sample_id", "words")
            grouped = insert_groups_word_count(grouped, "words", project, add_project_id = True)

        result[project] = grouped

    return result


def select_reasonably_grouped(treatment_extracted, group_col_name):
    '''
    Input:  data frame with columns: project_id, sample_id, group_col_name
    Output: data frame with reasonably grouped projects

    Remove projects that don't seem like they have any
    conditions extracted
    (e.g. N_groups != 1, or N_groups !=N_samples, n samples in group > 3, ...)
    '''
    threshold = 0.7
    max_n_groups = 6
    min_samples_in_group = 3

    # get number of samples per project
    treatment_extracted.index = treatment_extracted.project_id
    trt_g = treatment_extracted.groupby('project_id')
    summary = trt_g[['sample_id']].count()

    # get number of groups per project
    n_groups = trt_g[[group_col_name]].apply(lambda x: len(x.drop_duplicates()))
    summary.insert(1, "n_groups", n_groups)

    # keep project if n_groups != 1 and n_groups < threshold * n_sampels
    cond = (summary["n_groups"] < threshold*summary["sample_id"]) #& summary["n_groups"] == 1
    x = summary.loc[cond]
    x = x.loc[x["n_groups"] != 1]
    x = x.loc[x["n_groups"] < max_n_groups]

    #TODO: keep only those treatment groups that have more samples than min_samples_in_group
    #return trt_g
    #= trt_g[[group_col_name]].apply(lambda x: len(x.drop_duplicates()))



    #print(x.head())
    #TODO

    #return x.index
    return treatment_extracted.align(x, join="inner", axis=0)[0]


#def classify(similarity_matrix):
#    r = similarity_matrix[]
#    nbrs = NearestNeighbors(n_neighbors = 3)
#    nbrs.fit(r)
#
#    graph = nbrs.kneighbors_graph(r).toarray()
#    distances, indices = nbrs.kneighbors(r)
#    #TODO make groups: this is like an undirected graph clique problem
#
#    return None


def cluster_into_treatment_(A, method):
    bandw = estimate_bandwidth(np.array(A), quantile = 0.4, n_samples = A.shape[0])
    #bandw = estimate_bandwidth(np.array(A))
    if method == "MeanShift":
        ms = MeanShift(bandwidth=bandw, bin_seeding=True).fit(A)
        groups = ms.labels_
    if method == "AffinityPropagation":
        #af = AffinityPropagation(preference=-50).fit(A)
        af = AffinityPropagation().fit(A)
        groups = af.labels_
    if method == "DBSCAN":
        db = DBSCAN(eps=0.5, min_samples = 2).fit(A)
        groups = db.labels_

    o = pd.DataFrame(groups, index = A.index)
    return o


def cluster_into_treatment(A, md, project_list = 'all'):
    '''
    Use scikit-learn's unsupervised clustering to try to infer groups of samples
    within each experiment.

    TODO: ordinate before? It's not a huge dataset for most projects though - or is it? max 8300, hundreds
    Input:  M: a .tsv matrix of all samples vs. taxonomic units
    Output: putative groupings (df(columns=['sample_id', 'treatment']) or just an md
    '''
    clusters = {}
    no_samples = [] # or too few samples
    success_on = []

    if isinstance(project_list, str):
        project_list = md[[i in A.index for i in md.sample_id]]\
                        .project_id.drop_duplicates().tolist()

    for project_id in project_list:
        A_ = mx_for_prj(A, md, project_id)
        if A_.shape[0] != 0:
            print("---", project_id, " matrix shape:", A_.shape)
            try:
                clusters_ = cluster_into_treatment_(A_, method="DBSCAN")
                #clusters_ = cluster_into_treatment_(A_, method="AffinityPropagation")
                clusters_.columns = ["group_clustering"]
                clusters_.insert(0, "sample_id", clusters_.index)
                clusters_.insert(0, "project_id", project_id)

                clusters[project_id] = clusters_
                success_on += [project_id]
            except ValueError as e:
                print("--- --- ValueError for", project_id, ":", e)
                no_samples += [project_id]
        else:
            no_samples += [project_id]

    o = pd.concat(clusters.values())
    o.group_clustering = [str(project_id)+"_"+str(i) for i in o.group_clustering]
    print("--- --- No samples or too few samples found for", no_samples)
    print("--- --- Success for", success_on)

    # insert project ids: have df with columns prj id, sam id, group
    #o.columns = ("project_id", "sample_id", "groups_clustering")
    # select reasonably grouped
    #o = select_reasonably_grouped(o, 'group_clustering')
    return o


def campare_methods():
    '''
    Evaluate groups iferred by the different trt extraction methods
    ML metrics, e.g. AUC
    '''
    pass


def update_metadata(md, treatment_info, group_col_name):
    return pd.merge(md, treatment_info[['sample_id', group_col_name]],
                    on='sample_id', how="left")



def treatment_extract_words(metadata_path, ofile_path, use_metadata_cols = ['sample_name'], use_projects='all'):
    load_columns = ['project_id', 'project_name', 'number_of_samples',
                    'experimental_factor', 'study_linkout', 'study_abstract',
                    'biome', 'sample_id', 'sample_name', 'keywords']

    m = pd.read_csv(metadata_path, sep='\t', index_col=False, usecols = load_columns, low_memory=False)
    m = m.drop_duplicates()

    assert sum(m.sample_id.value_counts() != 1) == 0

    if isinstance(use_projects, str):
        use_projects = set(m.project_id)

    # save table with number of samples for project
    #pd.DataFrame.from_dict(dict(Counter(m.project_id)), orient="index").to_csv("n_samples.tsv", sep="\t")

    ### separately evaluate each column
    # also change with flag F1
    group_info = {}
    for use_metadata_col in use_metadata_cols:
        print("--- Extracting for", use_metadata_col)
        md_col_loc = m.columns.get_loc(use_metadata_col)

        groups = count_words_2(m, use_projects, md_col_loc, add_project_id = True)

        group_info_ = pd.concat([df for df in groups.values()])
        group_info_ = select_reasonably_grouped(group_info_, "group_word_count")
        group_info[use_metadata_col] = group_info_


    # this wont do for multiple used_metadata_columns; (see F1)
    # some projects may be present in both and it would result in pooling of the groups!
    o = pd.concat(group_info.values())

    ### UPDATE THE EXISTING MD
    m_full = pd.read_csv(metadata_path, sep='\t', index_col=0, low_memory=False)
    print(o.head())
    update_metadata(m_full, o, group_col_name = "group_word_count")\
        .to_csv(ofile_path, sep="\t")

    print("--- Saved results for the word count-based treatment extraction.")



def treatment_extract_clusters(metadata_path, abundance_path, ofile_path):
    m = pd.read_csv(metadata_path, sep='\t', index_col=0, low_memory=False)
    A = pd.read_csv(abundance_path, sep='\t', index_col=0)
    clusters = cluster_into_treatment(A, m)
    print(clusters.head())


    update_metadata(m, clusters, group_col_name = "group_clustering")\
        .to_csv(ofile_path, sep="\t")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
                        TODO: parallelise!!\
                        TODO: Metadata issues (see TODOs in the file).                \
                        TODO: improve group extraction (see TODOs in the file).                \
                        Takes in a metadata file and returns a table with putative groups for samples\
                        for each project based on the sample metadata (currently on sample_name).\
                        ")
    parser.add_argument("-m", "--metadata_path",
                        default = "/D/ebi/DEFAULT_METADATA.tsv",
                        help="",
                        required=True)
    parser.add_argument("-a", "--abundance_path",
                        default = "/D/ebi/DEFAULT_METADATA.tsv",
                        help="",
                        required=False)
    parser.add_argument("-o", "--output_path",
                        default = "/P/metag/data/DEFAULT_OFILE.tsv",
                        help="",
                        required=True)
    args = vars(parser.parse_args())

    metadata_path = args['metadata_path']
    abundance_path = args['abundance_path']
    ofile_path = args['output_path']
    #pd.set_option('display.max_colwidth', -1)

    #ATT # From ['sample_name', 'keywords_matched', 'keywords_extracted', 'biome', 'experimental_factor']
    # useful are: sample name, keywords extracted / keywords matched
    #use_metadata_cols = ['sample_name', 'keywords_extracted'] #F1 flag

    use_metadata_col = ['sample_name']
    treatment_extract_words(metadata_path, ofile_path, use_metadata_col)
    #treatment_extract_clusters(ofile_path, abundance_path, ofile_path)

    ### groups based on string similarity
    #similarity_matrix = compare_strings(m)
    #similarity_matrix.to_csv(ofile_path, sep='\t')

