#!/usr/bin/env python3

import pandas as pd
import numpy as np
import argparse
from math import isnan

def if_nan_then_zero(x):
    if isnan(x):
        return 0
    else:
        return x



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="TODO:merge with ebimg_reg_fil_merge.py.\
                        Input:  merged .tsv file (columns: run ID, OTU ID, counts, taxonomy)\
                                metadata (columns include: run_id, sample_id, biome)\
                                TODO: biome: name of biome to subset by\
                        Output: (default) index: sample IDs; columns: taxonomy\
                        (Should be run ID and OTU ID, which would be the lowest-level\
                        possible of a count matrix, but MemoryError\
                        sample x taxonomy requires < 30 GiB)\
                        ")
    parser.add_argument("-i", "--input_path",
                        help="Merged OTU-TSV v 2.0 (.tsv) (should work with v 1.0 too)",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        help="",
                        required=True)
    parser.add_argument("-m", "--metadata_path",
                        help="",
                        required=True)
    parser.add_argument("-t", "--data_type",
                        help="Currently 'OTU-TSV' and 'GOSlimAnnotations'.",
                        required=True)
    #parser.add_argument("-r", "--rows",
    #                    default="sample_id",
    #                    help='"run ID" (MemErr), "sample_id", "project_id", "biome", "keywords_extracted", "keywords_matched", ...',
    #                    required=False)
    #parser.add_argument("-c", "--columns",
    #                    default="taxonomy",
    #                    help="Either 'taxonomy' or 'OTU ID' (MemErr)",
    #                    required=False)
    parser.add_argument("-s", "--subset_by",
                        default="no",
                        help="Column name and value to subset by separated by '=', e.g. 'biome=Soil'",
                        required=False)
    args = vars(parser.parse_args())

    ifile_path      = args['input_path']
    ofile_path      = args['output_path']
    metadata_path   = args['metadata_path']
    data_type       = args['data_type']
    #column_colname            = args['columns']
    #index_colname            = args['rows']
    subset_by       = args['subset_by']

    if data_type == 'OTU-TSV':
        value_colname   = 'counts'
        index_colname   = 'sample_id'
        columns_colname = 'taxonomy'

    if data_type == 'GOSlimAnnotations' or data_type == 'GOAnnotations':
        value_colname   = 'hits'
        index_colname   = 'sample_id'
        columns_colname = 'go_id'


    merged_tsv = pd.read_csv(ifile_path, sep="\t",
                    usecols=['run ID', columns_colname, value_colname]) #, low_memory=False)
    md = pd.read_csv(metadata_path, sep="\t",
                    usecols=['run_id', 'sample_id', 'project_id', 'biome'])

    # make sure we have the specified col names
    assert(columns_colname in merged_tsv.columns.tolist() + md.columns.tolist())
    assert(index_colname   in merged_tsv.columns.tolist() + md.columns.tolist())

    #print(merged_tsv.shape)
    #print(md.shape)

    if not subset_by == "no":
        subset_by = subset_by.split("=")
        assert(len(subset_by) == 2)
        md = md[md[subset_by[0]] == subset_by[1]]

    merged_tsv = pd.merge(merged_tsv, md[['run_id', index_colname]],
                    right_on = 'run_id', left_on='run ID', how="inner")
    merged_tsv = merged_tsv[[index_colname, columns_colname, value_colname]].drop_duplicates()
    #print(merged_tsv.shape)

    A = merged_tsv.pivot_table(
                        index = index_colname, columns = columns_colname,
                        values = value_colname,
                        aggfunc=np.sum
                        )


    # NaNs are actually zeros (no OTU hits)
    A = A.applymap(lambda x: if_nan_then_zero(x))

    # this doesn't save with csv
    #A.columns.name = cols
    #A.index.name   = rows
    #print(A.columns.name)
    #print(A.index.name)

    #DEB mem testing
    #A = pd.DataFrame(index = merged_tsv['sample_id'], columns = merged_tsv['taxonomy'])

    print("The shape of new table is", A.shape)
    print("Saving...")

    A.to_csv(ofile_path, sep="\t")


