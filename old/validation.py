#!/usr/bin/env python3
'''
'''

import argparse
import pandas as pd
import numpy as np
import sys
from sklearn.metrics.pairwise import pairwise_distances

from toolbox import *

N_JOBS = 8
tax_md_fp = '/D/ebi/taxonomy_metadata.tsv'


# select taxons for manual lookup (think species, families, ?)
# get few top hits, few lowest hits
# select taxons for IRIDESCENT (same)

n = 10
pairwise_fp = 'P/metag/data/pairwise_relationships_species.tsv'


def mean_dist_within_tax_level2(D, mean_dist_D, tax_md, tax_level='family',
                                    permuted=False, min_shape = (4,4)):
    unit_list = list(set(tax_md[tax_level]))
    #mt = mapp(tax_md, tax_level, ['taxonomy'])
    cols = ['n_rows', 'n_cols', 'log(sqrt(n_col*nrow))', 'mean_distance', 'permuted']

    mean_dist = {}
    mean_dist[tax_level] = {cols[0]: D.shape[0],
                            cols[1]: D.shape[1],
                            cols[2]: np.log(np.sqrt(D.shape[0]*D.shape[1])),
                            cols[3]: mean_dist_D,
                            cols[4]: permuted,
                            }

    for unit in unit_list.copy():
        qt = list(set(tax_md['taxonomy'].ix[tax_md[tax_level] == unit])) # query taxonomy
        D_ = delete_empty(D.ix[qt, qt])
        if D_.shape[0] >= min_shape[0] & D_.shape[1] >= min_shape[1]:

            mean_dist[unit] = { cols[0]: D_.shape[0],
                                cols[1]: D_.shape[1],
                                cols[2]: np.log(np.sqrt(D_.shape[0]*D_.shape[1])),
                                cols[3]: D_.mean().mean(),
                                cols[4]: permuted,
                                }

        else:
            unit_list.remove(unit)

    o = pd.DataFrame.from_dict(mean_dist).T
    o.insert(0, 'tax_unit', o.index)
    o.insert(0, 'tax_level', tax_level)
    return o




