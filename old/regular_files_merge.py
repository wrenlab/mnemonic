#!/usr/bin/env python3

'''
Warning: I manually removd an entry with quote chars
Merge many .tsv files of the same format into one file.
(Strips first two lines for OTU-TSV data)
Input, output harcoded atm.
'''

import argparse
import pandas as pd
import numpy as np
import os

def merge_files(idir, ofile_path, tsv_file_list):
    '''
    merging for OTU-TSV data
    '''
    empty = 0
    not_found = 0
    other = 0
    ok_files = 0
    total_nrow = 0
    not_is_header = True
    print("---Merging", len(tsv_file_list), "files.")

    with open(ofile_path, "a") as ofile:
        for tsv_path in tsv_file_list:
            if not_is_header:
                header = "run ID\tOTU ID\tcounts\ttaxonomy\n"
                ofile.write(header)
                not_is_header = False

            try:
                tsv_file = pd.read_csv(idir+"/"+tsv_path, sep="\t", skiprows=2, header=None)

                # if file not found
                if tsv_file.shape[1] == 1:
                    not_found += 1
                else:
                    tsv_file = pd.concat(
                        [
                            pd.Series([ tsv_path.rstrip("_OTU-TSV.tsv") for i in range(tsv_file.shape[0]) ]),
                            tsv_file
                        ],
                        1)

                    total_nrow += tsv_file.shape[0]
                    ok_files += 1
                    tsv_file.to_csv(ofile, sep="\t", header=False, index=False)

            except pd.parser.CParserError:
                not_found +=1
            except ValueError:
                empty += 1
            except:
                other +=1
                pass


    print("---There were", empty, "empty files.")
    print("---There were", not_found, "parser errors (file not found in EBI probably - private?).")
    print("---A total of", other, "files had issues and I don't know why exactly (possibly not in file list).")
    print("---A total of", ok_files, "files were added.")
    print("---There are a total of", total_nrow, "rows.")


def merge_files_goslim(idir, ofile_path, tsv_file_list):
    '''
    merging for GOSlimAnnotations
    '''
    empty = 0
    not_found = 0
    other = 0
    ok_files = 0
    total_nrow = 0
    not_is_header = True
    print("---Merging", len(tsv_file_list), "files.")

    with open(ofile_path, "a") as ofile:
        for tsv_path in tsv_file_list:
            if not_is_header:
                header = "run ID\tgo_id\tgo_name\tgo_namespace\thits\n"
                ofile.write(header)
                not_is_header = False

            try:
                tsv_file = pd.read_csv(idir+"/"+tsv_path, sep=",", header=None)

                # if file not found
                if tsv_file.shape[1] == 1:
                    not_found += 1
                else:
                    tsv_file = pd.concat(
                        [
                            pd.Series([ tsv_path.rstrip("_GOSlimAnnotations.csv") for i in range(tsv_file.shape[0]) ]),
                            tsv_file
                        ],
                        1)

                    total_nrow += tsv_file.shape[0]
                    ok_files += 1
                    #TODO: add runid column
                    tsv_file.to_csv(ofile, sep="\t", header=False, index=False)

            except pd.parser.CParserError:
                not_found +=1
            except ValueError:
                empty += 1
            except:
                other +=1
                pass


    print("---There were", empty, "empty files.")
    print("---There were", not_found, "parser errors (file not found in EBI probably - private?).")
    print("---A total of", other, "files had issues and I don't know why exactly (possibly not in file list).")
    print("---A total of", ok_files, "files were added.")
    print("---There are a total of", total_nrow, "rows.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
            Script merges all .tsv files in the input dir to \
            one huge matrix.\
            ")
    parser.add_argument("-i", "--input_dir_path",
                        help="Path to directory with .tsv files corresponding to separate runs",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        help="Name of the merged output .tsv file",
                        required=True)
    parser.add_argument("-t", "--data_type",
                        help="Type of files. Look up 'mgportal_bulk_download -h'",
                        required=True)
    parser.add_argument("-m", "--metadata_path",
                        help="Optional. Path to a metadata file which can be used for subsetting the runs",
                        required=False)
    args = vars(parser.parse_args())
    idir = args['input_dir_path']
    ofile_path = args['output_path']
    data_type = args['data_type']

    if not args['metadata_path']:
        tsv_file_list = os.listdir(idir)
    else:
        tsv_file_list = [ run_id+'_OTU-TSV.tsv' for run_id in
            pd.read_csv(args['metadata_path'], sep="\t")['run_ids'].tolist() ]

    # erase the previous content of the output file
    open(ofile_path, 'w').close()

    if not os.path.exists(ofile_path):
        os.mknod(ofile_path)

    if data_type == "OTU-TSV":
        merge_files(idir, ofile_path, tsv_file_list)
    if data_type == "GOSlimAnnotations":
        merge_files_goslim(idir, ofile_path, tsv_file_list)
    #TODO add consistency check

