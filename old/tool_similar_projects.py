#!/usr/bin/env python3



import os
import pandas as pd
import seaborn as sns
#! from fastcluster import linkage
import matplotlib.pyplot as plt
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.feature_extraction.text import TfidfTransformer
from scipy.cluster.hierarchy import dendrogram, linkage


from time import time

from pairwise import *
from skrzynka import *
from plots import distance_matrix_heatmap

from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)

def merge_count_data(A, Q):
    '''
    Add query samples to the big count matrix.
    Taxons that are not present are set ot 0. (Alt: delete them? But they're
    useful for distances between other samples)
    '''
    o = pd.merge(A, Q, how='outer', right_index=True, left_index=True)
    o.fillna(0, inplace=True)
    o.drop_duplicates(inplace=True)
    o = delete_empty(o)
    return o

def merge_metadata(md, qmd):
    o = pd.merge(md, qmd, how='outer')#c, right_index=True, left_index=True)
    o.drop_duplicates(inplace=True)
    return o


@memory.cache
def to_dist_c(A):
    return current_raw_to_dist(A)


@memory.cache
def transform_c(A, transf_method):
    if transf_method == 'tfidf':
        tf_transformer = TfidfTransformer(sublinear_tf = True)
        print('this isnt using normalization!')
        A = pd.DataFrame(
                tf_transformer.fit_transform(A).toarray(),
                columns = A.columns, index = A.index
                )
    if transf_method == 'corana':
        A = correspondence_analysis_transform(A)
    return A



    D_ = D.ix[D.index.get_level_values('project_id') == query_project]
    # leave all samples in rows

def plot_samples(D, query_samples, n_D, n_pwl=0):
    pwl = pairwise_to_long(D, delete_self_self=True)
    pwl.index = pwl['sample1']
    pwl = pwl.ix[query_samples, :].sort_values('metric')
    if n_pwl > 0:
        pwl = pwl.ix[:n_pwl, :]
        #pwl = pwl.ix[pwl['metric'] < threshold, :]

    if n_D > 0:
        ss_samples = pwl.ix[:n_D, :]
        ss_samples = list(set(ss_samples.sample1.tolist()+ ss_samples.sample2.tolist()))
        D = D.ix[ss_samples, ss_samples]

    return D, pwl


def distance_and_subset(A, md, query_project):
    '''
    uses current_raw_to_dist
    '''
    n=2000
    D = current_raw_to_dist(A)
    query_samples = set(md['sample_id'].ix[md['project_id'] == query_project])
    D_,pwl = plot_samples(D, query_samples, n_D = 0, n_pwl =0)

    ps = pwl.ix[:n, :]
    ps = list(set(ps.sample1.tolist()+ ps.sample2.tolist()))
    return D, ps

def distance_matrix_dendrogram(A, md, query_project = "ERP009131", n=2000):
    #sns.set(style="white")
    D, ps = distance_and_subset(A, md)

    D_ = D.ix[ps,ps]

    Z = linkage(distance.pdist(D_), 'complete')
    plt.figure(figsize=(16,4))
    den = dendrogram(Z, labels = D_.index)
    return den

def draw_clustermap_for_similar(query_project, A, md):
    query_samples = set(md['sample_id'].ix[md['project_id'] == query_project])
    D, ps = distance_and_subset(A, md, query_project=query_project)
    Dm = current_raw_to_dist(
            add_index_level(
                A.ix[ps,:],
                md, 'sample_id', 'project_id'
                )
            )
    p = distance_matrix_heatmap(Dm, figsize=(30, 30), metric='cosine', method='complete')
    return p


def cluster_samples_linkage():
    '''
    ...to manageable groupings.
    '''
    return 'ni'
    # would have to be per project...
    # or, cluster together those closest yet from the same project
    # dl = dist_to_log(D).sort
    # dict()
    # for pair (row) in dl:
    # check if any of the two is already in dict.values()
        # yes: are all from the same project?
            # yes: put both samples into group, adding one to the group
            # no: don't do anything
        # no:
            # look at pair - if from same prj, cluster them put a new grouping
            # in a ref dict
    #


def linkage_groupings():
    '''
    The point is to provide data for a dendrogram plot. Idea: merge leaves
    that are adjacent and also belong to the same project.
    '''
    return 'ni'
    # start with linkage output Z
    # [D.index[i] for i in Z[:,0]] # there'll be some indices that are higher - these are already clusters
    # [D.index[i] for i in Z[:,0] if i <= D.shape[0] else NaN]
    #

def project_weights1(md):
    pw = md[['project_id', 'number_of_samples']].drop_duplicates()
    pw['weights'] = pw['number_of_samples']
    pw.index = pw.project_id
    pw = pw['weights']
    pw = pw+1
    #pw = np.log(pw)
    pw = 1/pw
    return pw

def project_weights2(pwl, md):
    pwl.index = pwl.sample2
    pwl.index.name = 'sample_id'
    pwl = add_index_level(pwl, md, 'sample_id', 'project_id').reset_index()
    return pwl.groupby('project_id').mean()

#def get_closest_projects(D, query_samples, md, n_prj):
    #_, pwl = plot_samples(D, query_samples, threshold=0)
def get_closest_projects(pwl, md):
    w1 = project_weights1(md) * 0.3
    w2 = project_weights2(pwl, md)
    w = pd.concat([i for i in w1.align(w2, join='inner')], axis=1)
    w = w.weights + w.metric
    return w.sort_values(ascending=False)


def similar_projects(query_project, D, md):
    lookup_prjs = set(D.index.get_level_values('project_id'))
    sel_cols = ['project_id', 'biome', 'project_name', 'study_linkout', 'study_abstract', 'keywords', 'number_of_samples']
    y = md[[i for i in sel_cols if i in md.columns]]\
            .ix[[i in lookup_prjs for i in md.project_id]].drop_duplicates()
    y.set_index('project_id', drop=True, inplace=True)
    return y



if __name__ == "__main__":
    ### INITIAL
    #similarity_matrix = None
    plt_title="test title"
    #transf_method = 'tfidf' # None 'tfidf' 'corana', 'log_transform'
    #similarity_metric='cosine' # 'correlation', 'euclidean', 'mutual_information'
    md_fp = '/D/ebi/DEFAULT_METADATA.tsv'
    output_dir = '/P/metag/data/tool_similar_projects'
    #query_project = "ERP001506"
    query_project = "ERP013524"
    query_project = "ERP013742" # if A = Ai.ix:[100, :100]

    mkdir(output_dir)

    ### LOAD DATA
    A = pd.read_csv('/D/ebi/A_OTU-TSV_v2_intestine.tsv', sep="\t", index_col=0)
    A = delete_empty(A.ix[:1000, :1000])
    md = pd.read_csv(md_fp, sep='\t', index_col=0, low_memory=False)

    A = add_index_level(A, md, 'sample_id', 'project_id')
    query_samples = set(md['sample_id'].ix[md['project_id'] == query_project])
    #A[A.index.get_level_values('project_id') == query_project]

    ### PROCESSING
    t0 = time()
    #if transf_method != None:
        #A = transform_c(A, transf_method)

    D = to_dist_c(A)
    print(D.shape)
    print('Distance matrix calculated in %fs' % (time() - t0))

    ### OUTPUT
    # list of closest samples; projects
    D_ = D
    D_.index = D_.index.droplevel('project_id')
    D_.columns = D_.columns.droplevel('project_id')
    D_ = plot_samples(D_, query_samples, threshold = 300)


    ### clustermap
    clsmap = sns.clustermap(D_)
    #plt.setp(clsmap.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
    #plt.title(plt_title)
    plt.savefig(output_dir+'/clustermap.png')
    Z = clsmap.dendrogram_col.linkage # linkage matrix for columns
    #den = dendrogram(Z, labels=D_.index) # abv_threshold_color='#AAAAAA'_)
    plt.close()
    # return dendrogram, possibly clustermap maybe return linkages instead?



    ### dendrogram throu scipy
    #Z = linkage(D_, method='ward')
    den = dendrogram(Z, labels=D_.index)

    #plt.xticks(rotation=90)
    #plt.figure(figsize=(12,4))
    plt.savefig(output_dir+'/dendrogram.png')
    plt.close()

    # sample-sample + colour by project
    # can't really collapse by project due to different conditions


    #? return some more exploratory plots?





