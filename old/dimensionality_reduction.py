#!/usr/bin/env python3

import argparse
import pandas as pd
import numpy as np
import sys

from sklearn.decomposition import PCA
from skrzynka import add_index_level

# rpy2
from rpy2.robjects import r
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
rvegan = importr('vegan')
pandas2ri.activate()

from sklearn import metrics
from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction.text import TfidfTransformer

from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)

N_JOBS = -2


def notes():
    pass


def cluster_in_project(A_, prj):
    #Z = linkage
    # cluster above a certain percentage of distances, ex. below mean distance for prj
    # if n_samples < x, than don't cluster; just return D_
    #print("Silhouette Coefficient: %0.3f"
      #% metrics.silhouette_score(X, km.labels_, sample_size=1000))
    n_clusters = 3
    affinity   = 'cosine'
    linkage    = 'complete'
    min_n_samples  = 6

    if A_.shape[0] > min_n_samples:

        ac = AgglomerativeClustering(n_clusters=n_clusters, affinity=affinity,
                        linkage=linkage) #, memory='path/to/cache' or joblib Mem
        ac.fit(A_)
        print(ac.labels_[:3])
        o = dict([i for i in zip(
                        A_.index,
                        [prj+str(j+1) for j in ac.labels_]
                                )
                ])

    else:
        print('Not enough samples in project')
        o = dict([i for i in zip(
                        A_.index,
                        [prj+str(1) for j in A_.index]
                                )
                ])
    #return clustered, samples in each cluster = {PRJID1: [s1,s2], ...}
    return o

def cluster_in_project_all(A, md, tfidf_transform=True):
    print('Also apply/relate to treatment_extraction/py')
    if tfidf_transform:
        tf_transformer = TfidfTransformer(sublinear_tf = True)
        A = pd.DataFrame(
                tf_transformer.fit_transform(A).toarray(),
                columns = A.columns, index = A.index
                )
    A = add_index_level(A, md, 'sample_id', 'project_id')
    o = {}
    for prj in set(A.index.get_level_values('project_id')):
        print(prj)
        A_ = A.ix[A.index.get_level_values('project_id') == prj]
        o[prj] = cluster_in_project(A_, prj)
    return o

def count_to_clustered_count(A):
    '''
    Returns an abundance matrix with fake samples made up from samples that are close
    to each other in different projects
    Output: matrix (n_samples < A.shape[0], n_features = A.shape[1]
    '''
    return 'ni'

def subset_k_from_n_studies(A, md, k=False, n=False,
        sample_colname = 'sample_id', project_colname = 'project_id', random_state=3):
    '''
    Returns a matrix with a maximum of k random samples
    for a maximum of n most abundant projects.
    If k or n == None no subsetting takes place.
    '''
    A = add_index_level(A, md, sample_colname, project_colname)
    print('There are', use_prj, 'projects.')
    if n:
        use_prj = A.index.get_level_values(project_colname).value_counts()
        assert n <= len(use_prj), "n is larger than the number of projects."
        A = A.ix[use_prj[:n].index]

    if k:
        A = A.groupby(A.index.get_level_values(project_colname)).\
            apply(lambda x: x.sample(k, random_state=random_state)
                        if x.shape[0] > k else x
                    )
    return A




@memory.cache
def get_pc(A, **kwargs):
    pca = PCA(**kwargs)
    return pca.fit(A)
    #A.groupby('project_id').sample(k)


@memory.cache
def kernel_pca(A, **kwargs):
    kpca_t = KernelPCA(**kwargs)
    #kpca_t = KernelPCA(kernel="rbf", fit_inverse_transform=False, gamma=10, n_components=10)
    return kpca_t.fit(A)

def Rpca(A, n_comps='all'):
    r.assign('A', A)
    r('pca <- prcomp(A)')
    r('var_explained <- (pca$sdev)^2 / sum(pca$sdev^2)')

    if n_comps != 'all':
        r.assign('n_comps', n_comps)
        #r('comps <- pca$rotation[,1:n_comps]')
        r('comps <- pca$x[,1:n_comps]')
        rvar_explained = r('var_explained[1:n_comps]')
    else:
        #r('comps <- pca$rotation') # * var_explained') # weight by var explained
        r('comps <- pca$x') # * var_explained') # weight by var explained
        rvar_explained = r('var_explained')

    rcomps = r('comps')

    colnames = ['PC'+str(n+1) for n in range(rcomps.shape[1])]
    rcomps = pd.DataFrame(rcomps, columns = colnames, index = A.index)

    return rcomps, rvar_explained


def Rdecorana(A):
    r.assign("A", A)
    r('deco <- decorana(A)')
    r('A_ord <- scores(deco)')
    var_explained = "Variance explained is undefined for detrended correspondence analysis."
    rdeco = r('A_ord')
    rdeco = pd.DataFrame(rdeco, index = A.index, columns = ["PC"+str(i+1) for i in range(rdeco.shape[1])])
    return rdeco


def regularise(F):
    F_expected = pd.DataFrame(
        np.outer(F.apply(sum, 1), F.apply(sum, 0)),
        columns = F.columns, index = F.index
        )\
        .applymap(lambda x: x / (F.shape[0]*F.shape[1]) )\

    F_regularised = F * F_expected / F_expected**(1/2)

    return(F_regularised)






