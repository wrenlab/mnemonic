#!/usr/bin/env python3

'''
'''

#from flask import *
import pandas as pd
import numpy as np
from fastcluster import linkage

from shutil import rmtree
from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)

import skrzynka as sk
import pairwise


try:
    from __main__ import ABN_FP
except ImportError as e:
    ABN_FP = '/P/mnemonic-webapp-dev/data/dummy_A.tsv'


GO_MD_FP = '/D/ebi/metadata_GO.tsv'
SAM_MD_FP = '/D/ebi/DEFAULT_METADATA.tsv'
TAX_MD_FP = '/D/ebi/taxonomy_metadata.tsv'
AMGUT_MD_FP = '/D/ebi/American_Gut_metadata_cleaned.txt'

@memory.cache
def get_sam_md(sam_md_fp=SAM_MD_FP, subset_display=True):
    md = pd.read_csv(sam_md_fp, sep='\t', index_col = 'sample_id')
    md.insert(0, 'sample_id', md.index)
    if subset_display:
        allcols = ['project_id', 'project_name', 'number_of_samples',
                    'submitted_date', 'analysis', 'ncbi_project_id',
                    'public_release_date', 'centre_name', 'experimental_factor',
                    'is_public', 'study_linkout', 'study_abstract',
                    'biome', 'sample_id', 'sample_name']
        usecols = [allcols[i] for i in [0,1,2,12,13,14,10]]
        md=md[usecols]
        md.drop_duplicates()
        md['biome'] = md['biome'].str.upper()
    return md


@memory.cache
def get_tax_md(tax_md_fp=TAX_MD_FP):
    md = pd.read_csv(tax_md_fp, index_col='taxon_id', sep='\t')
    md.insert(0, 'taxon_id', md.index)
    return md

@memory.cache
def get_go_md(go_md_fp=GO_MD_FP):
    md = pd.read_csv(go_md_fp, index_col=0, sep='\t')
    return md

def get_amgut_md(amgut_md_fp=AMGUT_MD_FP):
    md = pd.read_csv(amgut_md_fp, index_col=False, sep='\t')
    md = get_sam_md()[['sample_id', 'sample_name']].merge(md, how='inner',
                left_on='sample_name', right_on='#SampleID')
    md.set_index('sample_id', drop=False, inplace=True)

    for na in ['no_data', 'Unspecified', 'Unknown', 'Not sure']:
        md.replace(na, np.nan, inplace=True)
    for tr in ['true', 'TRUE', 'True', 'Yes', 'yes', 'YES']:
        md.replace(tr, True, inplace=True)
    for fa in ['false', 'FALSE', 'False', 'No', 'no', 'NO']:
        md.replace(fa, False, inplace=True)

    return md

@memory.cache
def collM_c(M = ABN_FP, **kwargs):
    return sk.collM(M, **kwargs)
    #return pd.read_csv(M, sep='\t', index_col=0)

@memory.cache
def collM_c_dummy(M = ABN_FP, **kwargs):
    return sk.delete_empty( collM_c(M, **kwargs).sample(10, random_state=3).iloc[:,:300])

@memory.cache
def get_sam_dist(abn = collM_c()):
    return pairwise.current_raw_to_dist(abn)

@memory.cache
def get_tax_dist(abn = collM_c().T):
    return pairwise.current_raw_to_dist(abn)

@memory.cache
def get_tax_n_overlapping(abn = collM_c().T):
    return pairwise.n_overlapping(abn)

@memory.cache
def get_tax_corr(abn = collM_c().T):
    return pairwise.pairwise_distances_pd(abn, metric=pairwise.correlation_)

@memory.cache
def get_tax_fisher(abn = collM_c().T):
    return pairwise.pairwise_distances_pd(abn, metric=pairwise.fisher_exact_)

@memory.cache
def get_tax_chisquare_kernel(abn = collM_c().T):
    return pairwise.pairwise_kernels_pd(abn, metric ='chi2')

@memory.cache
def _current_raw_to_dist(A):
    return pairwise.current_raw_to_dist(A)

@memory.cache
def _linkage(D, **kwargs):
    return linkage(D, **kwargs)


def populate_cache():
    print('sam_md precomputing...')
    _ = get_sam_md()
    print(_.shape)
    _ = get_sam_md(subset_display=False)
    print(_.shape)
    print('tax_md precomputing...')
    _ = get_tax_md()
    print(_.shape)
    print('A precomputing...')
    _ = collM_c()
    print(_.shape)
    print('sam_dist precomputing...')
    _ = get_sam_dist()
    print(_.shape)
    print('tax_dist precomputing...')
    _ = get_tax_dist()
    print(_.shape)
    print('tax_n_overlapping precomputing...')
    _ = get_tax_n_overlapping()
    print(_.shape)
    print('tax_fisher precomputing...')
    _ = get_tax_fisher()
    print(_.shape)
    print('tax_chisquare_kernel precomputing...')
    _ = get_tax_chisquare_kernel()
    print(_.shape)

def clear_cache():
    rmtree('cache')
