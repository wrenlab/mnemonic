import os
import pandas as pd

def summarize(prefix = 'differential_abundance_', suffix = '_subset.tsv',
                contains_not = 'function',
                da_resdir = '/P/mnemonic/differential_analysis_results'):

    lfc_matrix = []
    for f in os.listdir(da_resdir):
        if f.endswith(suffix) and f.startswith(prefix) and f.find(contains_not) == -1:
            contrast = f.strip(prefix).strip(suffix)
            print(contrast)

            da = pd.read_csv(os.path.join(da_resdir,f), sep='\t', index_col=0)
            new = da['log2FoldChange']
            new.name = contrast
            lfc_matrix.append(new)

    #assuming same len:
    lfc_matrix = pd.concat(lfc_matrix, 1)

    return lfc_matrix


