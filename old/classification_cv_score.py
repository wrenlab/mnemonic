#!/usr/bin/env python3

'''
'''
#TODO remove hardcoded directories
#TODO output file name suffix
#TODO addmetrics
#TODO opt to load in the hat data
#TODO (low) posibility to chose the classes: "keywords", "Sample_classification"

# import scripts from the same directory
import plot_classification_report as pcr

import pandas as pd
import numpy as np
import sklearn.base
from scipy import interp
from sklearn.preprocessing import label_binarize
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import cross_val_score, cross_val_predict, StratifiedKFold, train_test_split
from sklearn.metrics import precision_score, recall_score, roc_auc_score, roc_curve, auc, f1_score, classification_report
from sklearn.multiclass import OneVsRestClassifier
from sklearn.feature_selection import VarianceThreshold
import argparse

from metadata_samples import create_dictionary

import matplotlib.pyplot as plt

import os
import sys
import pickle
import random
import gc
import multiprocessing as mp
import warnings
import tempfile
import joblib
from joblib import Memory, Parallel, delayed
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import seaborn as sns
    import matplotlib.pyplot as plt

memory = Memory(cachedir="cache/")


def select_features(X, threshold):
    selector = VarianceThreshold(threshold = threshold)
    selector.fit(X)
    X_new = selector.transform(X)
    return pd.DataFrame(X_new, columns= X.columns[selector.get_support()], index = X.index)


def get_Y(m, use_cv):
    #TODO remove the use_cv subsetting from here
    classes = list(set(m.keywords))
    Y = pd.DataFrame(
        label_binarize(
            list(m.keywords),
            classes = classes
    ))
    Y = Y.ix[:,Y.sum() > max(10, use_cv)]
    classes = [classes[i] for i in Y.columns]
    return Y, classes


def get_better_Y(m, d, use_cv):
    '''
    m is a metedata matrix with the keywords column
    d is a dictionary of classes as keys and the keywords beloning to a class as values
    '''

    d = {k:"\\b"+"\\b|\\b".join([v_ for v_ in v])+"\\b" for k,v in d.items()}
    Y = [m.keywords.str.contains(v, regex = True) for k,v in sorted(d.items())]
    Y = np.vstack(Y).T
    Y = pd.DataFrame(Y, columns = sorted(d.keys()))
    Y = Y.astype(int)
    Y.index = m.sample_id
    return Y


def plot_all_roc_curves(fig_no, fpr, tpr, classes, title_extension, output_metrics_folder, pfile, subset=False):
    '''
    subset should be a list of the keywords to include in the plot
    '''
    # Plot all ROC curves
    plt.figure(fig_no)

    if isinstance(subset, list):
        for i,c in enumerate(classes):
            if (c in subset):
                plt.plot(fpr[i], tpr[i], label='{0} (AUC = {1:0.2f})'
                                            ''.format(c, roc_auc[i]))
        plt.title('ROC curve, all classes, ' + title_extension)

    else:
        plt.plot(fpr["micro"], tpr["micro"],
                label='micro-average ROC curve (area = {0:0.2f})'
                    ''.format(roc_auc["micro"]),
                linewidth=2)

        plt.plot(fpr["macro"], tpr["macro"],
                label='macro-average ROC curve (area = {0:0.2f})'
                    ''.format(roc_auc["macro"]),
                linewidth=2)

        if subset == False:
            for i,c in enumerate(classes):
                plt.plot(fpr[i], tpr[i], label='ROC curve of class {0} (area = {1:0.2f})'
                                            ''.format(c, roc_auc[i]))
            plt.title('ROC curve, all classes, '+title_extension)

    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    plt.legend(loc="lower right")
    plt.savefig(output_metrics_folder+"/"+pfile)
    #plt.show()
    plt.cla()


def hist_micro_auc(fig_no, micro_auc, output_metrics_folder, pfile):
    plt.figure(fig_no)
    sns.distplot(micro_auc, bins=25)
    plt.savefig(output_metrics_folder+"/"+pfile)
    #plt.show()
    plt.cla()


def _predict_cv(model, X, y, cv):
    """
    Perform cross-validation-based prediction on a single annotation (GO term).
    """
    y_hat = np.zeros(y.shape[0])
    kf = StratifiedKFold(y, n_folds=cv, shuffle=True)
    for fold, (tr_ix, te_ix) in enumerate(kf):
        model.fit(X[tr_ix,:], y.iloc[tr_ix])
        ix = list(model.classes_).index(1) #ATT for classification
        y_hat[te_ix] = model.predict_proba(X[te_ix,:])[:,ix]
        #y_hat[te_ix] = model.predict(X[te_ix,:]) # for regression
    return y_hat


#@memory.cache
def predict_cv(model, X, A, cv=10):
    """
    Perform cross-validation-based prediction on all annotations/terms in an
    annotation matrix.
    """
    #X, A = X.align(A, axis=0, join="left")
    #assert(all(A.index == X.index))
    with tempfile.NamedTemporaryFile() as tmp:
        joblib.dump(np.array(X), tmp.name)
        Xm = joblib.load(tmp.name, mmap_mode="r")

        #A = A.ix[:,A.sum() > max(10, cv)]
        Y_hat = np.zeros(A.shape)

        pool = Parallel(n_jobs=use_jobs)
        Y_hat = pool(delayed(_predict_cv)(sklearn.base.clone(model), Xm, A.loc[:,c], cv) for c in A.columns)
        del Xm
        gc.collect()
        return pd.DataFrame(np.vstack(Y_hat).T, index=A.index, columns=A.columns)


def predict_for_query(Q, X, classifier, classes):
    Q,_ = Q.align(X, axis = 1, join = "right")
    Q.fillna(0, inplace = True)

    assert(all(X.columns == Q.columns))
    predictions = classifier.predict_proba(Q)
    predictions = [pred[:,1] for pred in predictions]
    predictions = pd.DataFrame(np.vstack(predictions).T, index=Q.index, columns=classes)

    return predictions


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-m", "--metadata_path",
                        help="",
                        required=True)
    parser.add_argument("-i", "--input_path",
                        help="",
                        required=True)
    parser.add_argument("-k", "--keywords_path",
                        help="",
                        required=True)
    parser.add_argument("-s", "--output_files_name_suffix",
                        help="The suffix that will be added to the output files.",
                        required=True)
    parser.add_argument("-c", "--only_build_classifier",
                        help = "",
                        required = False, action = "store_true")
    parser.add_argument("-p", "--only_predict_for_query",
                        help = "",
                        required = False, action = "store_true")
    parser.add_argument("-q", "--query_path",
                        help="",
                        required=False)
    args = vars(parser.parse_args())

    metadata_path = args['metadata_path']
    ifile_path = args['input_path']
    name_suffix = args['output_files_name_suffix']
    keywords_path = args['keywords_path']
    only_build_classifier = args['only_build_classifier']
    only_predict_for_query = args['only_predict_for_query']
    query_path = args['query_path']

    #potential args
    #auc_plot_subset = False
    auc_plot_subset = ["intestine", "soil", "skin", "mouse",
        "adult", "stroke", "young", "control"]
    output_data_folder = "ML_data"
    output_metrics_folder = "ML_metrics"


    use_jobs = 4 #mp.cpu_count() - 2
    use_cv = 10
    random.seed(3)
    load_from_files_f = False
    select_features_f = False

    ###
    os.makedirs(output_data_folder+"/", exist_ok=True)
    os.makedirs(output_metrics_folder+"/", exist_ok=True)

    print("\n--- Loading data")
    X = pd.read_csv(ifile_path, sep="\t", index_col = 0)
    if select_features_f:
        X = select_features(X, X.var(0).quantile(0.9))
        X.columns.to_series().to_csv(output_data_folder+"/selected_festures_"+name_suffix+".tsv", sep="\t")
    m = pd.read_csv(metadata_path, sep = "\t")

    m.index = m.sample_id
    print("\n---- " + str(sum(m.keywords.str.find("NaN") != -1)) + " samples in metadata did not have keywords and were deleted.")
    m = m.ix[m.keywords.str.find("NaN") == -1, :]
    #m = m.ix[~(m.keywords.str.find("american") != -1) & (m.keywords.str.find("control") != -1), :]

    m, X = m.align(X, axis = 0, join = "inner")
    assert(all(m.index == X.index))

    if load_from_files_f:
        print("\n--- Loading Y")
        Y = pd.read_csv(output_data_folder+"/Y_true_"+name_suffix+".tsv", sep = "\t", index_col = 0)
    else:
        print("\n--- Creating Y")
        #d = pickle.load(open(dictionary_path, 'rb'))
        d = create_dictionary(keywords_path)
        Y = get_better_Y(m,d,use_cv)
        Y.to_csv(output_data_folder+"/Y_true_"+name_suffix+".tsv", sep = "\t")
        Y = Y.ix[Y.sum(1) > 0, Y.sum() > max(10, use_cv)]

    classes = [i for i in Y.columns]

    #sys.exit("Exiting.")
    classifier = KNeighborsClassifier(n_neighbors = 5, weights = "distance")#, metric = "braycurtis")

    #TODO define own dist unction (integrate distance_make.py)
    #metric = sklearn.neighbors.DistanceMetric.get_metric('pyfunc', func=func)
    #classifier = KNeighborsClassifier(n_neighbors = 5, weights = "distance", metric = metric)

    print("\n---- Number of classes:  " + str(len(classes)))
    print("---- Number of samples:  " + str(X.shape[0]))
    print("---- Number of features: " + str(X.shape[1]))
    print("---- Number of studies:  " + str(len(m.project_id.drop_duplicates())))

    ### save the model
    classifier.fit(X, Y)
    with open(output_data_folder+"/trained_classifier"+name_suffix+".pkl", "wb") as o:
        pickle.dump(classifier, o)

    print("\n--- Model pickled")

    if only_build_classifier:
        sys.exit("--- Exiting")

    if only_predict_for_query:
        print("\n--- Clearly there's something wrong with the predicting for query.")
        Q = pd.read_csv(query_path, sep="\t", index_col = 0)
        print("\n--- Predicting for query")
        predictions = predict_for_query(Q, X, classifier, classes)
        predictions.to_csv(output_data_folder+"/predictions_for_query_"+name_suffix+".tsv", sep="\t")
        sys.exit("--- Exiting")

    if load_from_files_f:
        Y_hat = pd.read_csv(output_data_folder+"/Y_hat_predict_cv_"+name_suffix+".tsv", sep="\t", index_col = 0)
    else:
        print("\n--- Calculating Y_hat, method 1")
        Y_hat = predict_cv(classifier, X, Y, cv = 10)
        Y_hat.index = m.sample_id
        Y_hat.to_csv(output_data_folder+"/Y_hat_predict_cv_"+name_suffix+".tsv", sep="\t")

    if load_from_files_f:
        Y_hat_2 = pd.read_csv(output_data_folder+"/Y_hat_cross_val_predict_"+name_suffix+".tsv", sep="\t", index_col = 0)
    else:
        print("\n--- Calculating Y_hat, method 2")
        Y_hat_2 = pd.DataFrame(cross_val_predict(classifier, X, Y, cv = 10, n_jobs = use_jobs))
        Y_hat_2.index = m.sample_id
        Y_hat_2.columns = classes
        Y_hat_2.to_csv(output_data_folder+"/Y_hat_cross_val_predict_"+name_suffix+".tsv", sep = "\t")


    ##############################################################################
    # ROC
    n_classes = Y.shape[1]
    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    ##############################################################################
    ### Y_hat

    print("\n--- Calculating metrics for method 1")
    # Compute ROC curve and ROC area for each class
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(Y.iloc[:, i], Y_hat.iloc[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(np.matrix(Y).ravel().T, np.matrix(Y_hat).ravel().T)
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])


    # Compute macro-average ROC curve and ROC area
    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= n_classes

    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    plot_all_roc_curves(1, fpr, tpr, classes, "probabilities_all", output_metrics_folder, "AUC_scores_probabilities_all_"+name_suffix+".png")
    plot_all_roc_curves(7, fpr, tpr, classes, "probabilities_subset", output_metrics_folder, "AUC_scores_probabilities_subset_"+name_suffix+".png", subset=auc_plot_subset)
    plot_all_roc_curves(5, fpr, tpr, classes, "probabilities_mean", output_metrics_folder, "AUC_scores_probabilities_mean_"+name_suffix+".png", subset=False)
    hist_micro_auc(2, roc_auc_score(Y, Y_hat, average=None), output_metrics_folder, "micro_auc_distribution_probabilities_"+name_suffix+".png")


    ##############################################################################
    ### Compute for the Y_hat_2
    print("\n--- Calculating metrics for method 2")
    # Compute ROC curve and ROC area for each class
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(Y.iloc[:, i], Y_hat_2.iloc[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(np.matrix(Y).ravel().T, np.matrix(Y_hat_2).ravel().T)
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])


    # Compute macro-average ROC curve and ROC area
    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= n_classes

    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    plot_all_roc_curves(3, fpr, tpr, classes, "binary_predictions_all", output_metrics_folder, "AUC_scores_binary_predictions_all_"+name_suffix+".png")
    plot_all_roc_curves(6, fpr, tpr, classes, "binary_predictions_subset", output_metrics_folder, "AUC_scores_binary_predictions_subset_"+name_suffix+".png", subset=auc_plot_subset)
    plot_all_roc_curves(6, fpr, tpr, classes, "binary_predictions_mean", output_metrics_folder, "AUC_scores_binary_predictions_mean_"+name_suffix+".png", subset=False)
    hist_micro_auc(4, roc_auc_score(Y, Y_hat_2, average=None), output_metrics_folder, "micro_auc_distribution_binary_predictions_"+name_suffix+".png")

    print("\n--- Saving some additional data")
    # check that the classes are in the same order as originally!
    # data frame: class no, class name, % positive, roc auc score, f1 score?
    perc_positive = [Y.iloc[:,c].mean()*100 for c in range(Y.shape[1])]

    # NOT POSSIBLE precision_recall_f1_probabilities = classification_report(Y, Y_hat)
    Y.columns = classes
    Y_hat.columns = classes
    Y_hat_2.columns = classes

    cr = classification_report(Y, Y_hat_2)
    cr = pcr.plot_classification_report(classes, cr)
    print("--- NOTE: precision and recall are calculated for binary predictions")
    scores = pd.DataFrame({"class_name": classes,
        "%_positive": [round(i, 2) for i in perc_positive],
        "AUC_scores_for_method_1": [round(i, 2) for i in roc_auc_score(Y, Y_hat, average=None)],
        "AUC_scores_for_method_2": [round(i, 2) for i in roc_auc_score(Y, Y_hat_2, average=None)]
        })
    scores = pd.concat([cr, scores], axis=1)
    scores.to_csv(output_metrics_folder+"/metrics_"+name_suffix+".tsv", sep = "\t")

    plt.savefig(output_metrics_folder+"/heatmap_"+name_suffix+".png", dpi=200, format='png', bbox_inches='tight')
    plt.close()



#    # Plot of a ROC curve for a specific class
#    plt.figure(1)
#    plt.plot(fpr[2], tpr[2], label='ROC curve (area = %0.2f)' % roc_auc[2])
#    plt.plot(fpr[3], tpr[3], label='ROC curve (area = %0.2f)' % roc_auc[3])
#    plt.plot(fpr[4], tpr[4], label='ROC curve (area = %0.2f)' % roc_auc[4])
#    plt.plot([0, 1], [0, 1], 'k--')
#    plt.xlim([0.0, 1.0])
#    plt.ylim([0.0, 1.05])
#    plt.xlabel('False Positive Rate')
#    plt.ylabel('True Positive Rate')
#    plt.title('Receiver operating characteristic example')
#    plt.legend(loc="lower right")
#    plt.show()

    # F1 score
    #f1 = [f1_score(Y[:,i], Y_hat[:,i]) for i in range(Y.shape[1]) ]
    #cv_scores = cross_val_score(classifier, X, Y, cv = 10, n_jobs = 10)
    #print(cv_scores)

    #same results with normalization of X

    #Y_hat = cross_val_predict(knn, X, Y, cv = cv, n_jobs = 10)
    #micro_scores = cross_val_score(knn, X, Y, cv = 10, n_jobs = 10)
    #scores = roc_auc_score(
    #print(micro_scores)
    #print table(labels)


