## MNEMONIC
MNEMONIC (MetageNomic Experiment Mining to create an OTU Network of Inhabitant Correlations) is a project that aims to analyse metagenomics data.

It downloads metagenomic data available on EBI Metagenomics – currently almost 1000 projects and almost 60000 samples (March 2017) –
and aims at providing data-based insight into new metagenomic experiments, as well as providing a framework for the exploration of existing data.

Finding samples and experiments that are similar to query samples allows the potential discovery of new properties of the finding and possibly relevant literature.
A keyword-based approach has been applied to classify the data, as the metadata is very often inconsistent, insufficient, or simply missing across experiments.
This enables query samples to be classified for further analysis. Inference of subgroups within experiments is accomplished with text analysis.
Analysis of differential abundance between those groups enables an analysis of microbe-condition and microbe-microbe relationships.

## Current state, TODOs
Some of the scripts (notably download scripts) should be readily CL-useable.
But the rest not really, need to comment and restructure into a package.

## Usage
### Download metadata from EBI metagenomics
**python2** metadata_download.py -h

### Add keywords from metadata
python3 metadata_keywords.py -h

### Add treatments from metadata
python3 treatment_extract.py -h

### Download EBI Metagenomics data (CL)
**python2** bulk_download_projects.py -o downloaded_dir -v 2.0 -t OTU-TSV

python3 regular_files_extract.py -i downloaded_dir -o extracted_dir

python3 regular_files_merge.py -i extracted_dir -o merged.tsv -t OTU-TSV

python3 abundance_matrix_make.py -i merged.tsv -o abundance_matrix.tsv -m metadata.tsv -t OTU-TSV

### Download metadata from MG-RAST
python3 metadata_download_mgrast.py -o download_dir

### Download metadata for EBI samples from ENA
python3 metadata_download_ena.py -o download_dir

