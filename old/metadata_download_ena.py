#!/usr/bin/env python3

ebi_sample_information_url = "https://www.ebi.ac.uk/metagenomics/samples/doExportTable?searchTerm=&sampleVisibility=ALL_PUBLISHED_SAMPLES&search=Search&startPosition=0"

import argparse
import csv
import pandas as pd
import numpy as np
import os
import sys
import collections

#import urllib
#import xml.etree.ElementTree as etree
import xmltodict
import io
import requests
from lxml import html
#from toolbox import flatten_dict

def flatten_dict(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)



#from metadata_download import get_list_of_samples

def get_list_of_samples(url):
    """
    Returns a list of all unique sample IDs in a .csv file
    specified in 'url'.
    """
    s = requests.get(url).content
    samples = pd.read_csv(io.StringIO(s.decode('utf-8')), sep=",", error_bad_lines = False)

    if "Sample ID" in samples.columns:
        return samples\
            ["Sample ID"]\
            .unique()\
            .tolist()
    else:
        sys.exit("Something's wrong; is the EBI MG project list URL still active and still contains column named 'Study ID'?")





def sample_xml2df(sample_url):
    s = requests.get(sample_url)
    d = flatten_dict(xmltodict.parse(s.content)['ROOT'])

    md1 = {k:v for k,v in d.items() if isinstance(v, str)}
    md1 = pd.DataFrame.from_dict(md1, orient='index').reset_index()

    md2 = {k:v for k,v in d.items() if not isinstance(v, str)}
    md2a = pd.DataFrame.from_dict(md2['SAMPLE_SAMPLE_ATTRIBUTES_SAMPLE_ATTRIBUTE'])


    md2b = pd.DataFrame.from_dict(
                [list(i.values())[0] for i in md2['SAMPLE_SAMPLE_LINKS_SAMPLE_LINK']]
                )

    new_row = pd.DataFrame(np.concatenate([md1, md2a, md2b], axis=0),
                            columns = ['attribute', 'value'])
    new_row.index = new_row['attribute']
    new_row = new_row['value']
    new_row = new_row.to_dict()
    s.close()

    return new_row


def retrieve_sample_information_ena():
    root_url = "https://www.ebi.ac.uk"
    sample_information_url_template = root_url + "/ena/data/view/%s&display=xml"
    sample_list = get_list_of_samples(ebi_sample_information_url)
    #sample_list =  ['fake', 'SRS560249', 'SRS560250', 'SRS560251', 'SRS560252',
                    #'SRS560253', 'SRS560254', 'SRS560255', 'SRS560256', 'SRS560238']
    print('--- Downloading for', len(sample_list), 'samples.')

    erron=[]
    o = {}
    for sid in sample_list:
        try:
            sample_url = sample_information_url_template % (sid)
            print(sample_url)
            o[sid] = sample_xml2df(sample_url)
        except:
            erron.append(sid)

    o = pd.DataFrame.from_dict(o, orient='index')
    o.columns = colnames + o.columns[len(colnames):]
    return o, erron

def clean_sample_information_ena(md_ena):
    colnames = ['sample_id1', 'request', 'center_name', 'sample_alias',
                'sample_accesion',
                'broker_name', 'sample_primary_id', 'sample_title', 'sample_taxon_id',
                'sample_scientific_name', 'sample_description']
    ena.ix[:,ena.columns.str.find('biome') != -1].apply(pd.isnull).sum()
    ena.ix[:,ena.columns.str.find('env') != -1].apply(pd.isnull).sum()
    return 'ni'



def main():
    parser = argparse.ArgumentParser(description="Download EBI Metagenomics\
                        metadata from ENA")
    parser.add_argument("-o", "--output_path",
                        help="Location of the output file.",
                        default = '/D/ebi/metadata_ENA.tsv',
                        required=False)
    args = vars(parser.parse_args())

    ofile = args['output_path']

    o, erron = retrieve_sample_information_ena()
    o.to_csv(ofile, sep='\t')
    print('--- Errors on: ', erron)


if __name__ == "__main__":
    main()
