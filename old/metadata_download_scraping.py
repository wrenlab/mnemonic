#!/usr/bin/env python2

'''
'''

import argparse
import csv
import os
import sys
import urllib
import io
#from urllib2 import URLError
import requests
from lxml import html
import pandas as pd

def integrate_information(project_information_url, sample_information_url, run_information_file):
    s = requests.get(project_information_url).content
    print("--- reading in project info")
    prj = pd.read_csv(io.StringIO(s.decode('utf-8')), na_values = ["(not given)", "none"])
    s = requests.get(sample_information_url).content
    print("--- reading in sample info")
    sam = pd.read_csv(io.StringIO(s.decode('utf-8')), na_values = ["(not given)", "none"])
    print("--- reading in run (scraped) info")
    runi = pd.read_csv(run_information_file, sep="\t")

    runi.replace("(not given)", "NaN", inplace=True)
    runi.replace("none", "NaN", inplace=True)

    # Save the tables for inspection
    prj.to_csv(odir_path + "project_information.tsv", sep="\t")
    sam.to_csv(odir_path + "sample_information.tsv", sep="\t")
    runi.to_csv(odir_path + "run_initial_information_maybe_same.tsv", sep="\t")

    ## read
    #prj = pd.read_csv(odir_path + "project_information.tsv", sep="\t", index_col = 0)
    #sam = pd.read_csv(odir_path + "sample_information.tsv", sep="\t", index_col = 0)
    ##runi = pd.read_csv(odir_path + "run_initial_information_maybe_same.tsv", sep="\t", index_col = 0)
    #runi = pd.read_csv(odir_path + "run_information.tsv", sep="\t", index_col = 0)

    # Integrate information
    prj.columns = [i.replace(" ", "_").lower() for i in prj.columns]
    sam.columns = [i.replace(" ", "_").lower() for i in sam.columns]
    runi.columns = [i.replace(" ", "_").lower() for i in runi.columns]
    #prj.columns = ["Study_id", "Study_name", "Number_of_samples", "Submitted_date",
    #    "Analysis", "NCBI_project_id", "Public_release_date", "Centre_name",
    #    "Experimental_factor", "Is_public", "Study_linkout", "Study_abstract"]
    #sam.columns = ["Sample_biome", "Sample_id", "Sample_name", "Study_name", "UNNAMED"]
    #runi.columns = ["Study_id", "Sample_id", "Run_id", "Sample_classification",
    #    "Experiment_type", "Sample_biome", "Experimental_feature", "Material",
    #    "ENVO", "UBERON", "Sample_description"]

    sam.drop("unnamed:_4", axis = 1, inplace = True)
    runi.drop("biome", axis = 1, inplace = True)
    prj.rename(columns={"study_id":"project_id"}, inplace=True)

    whole = pd.merge(sam, runi, on="sample_id", how="outer")
    whole = pd.merge(prj, whole, on=["project_id", "project_name"], how="outer")

    #col_seq = ['biome', 'sample_id', 'sample_name', 'project_name', 'project_id',
    #'run_id', 'classification', 'experiment_type', 'experimental_feature',
    #'material', 'envo', 'uberon', 'sample_description', 'number_of_samples',
    #'submitted_date', 'analysis', 'ncbi_project_id', 'public_release_date',
    #'centre_name', 'experimental_factor', 'is_public', 'study_linkout',
    #'study_abstract']
    #whole = whole[col_seq]

    return whole


def get_list_of_projects(url):
    """
    Returns a list of all unique project IDs in a .csv file
    specified in 'url'.
    """
    s = requests.get(url).content
    projects = pd.read_csv(io.StringIO(s.decode('utf-8')), error_bad_lines=False)

    if "Study ID" in projects.columns:
        return projects\
            ["Study ID"]\
            .unique()\
            .tolist()
    else:
        sys.exit("Something's wrong; is the EBI MG project list URL still active and still contains column named 'Study ID'?")


def get_list_of_samples(url):
    """
    Returns a list of all unique sample IDs from the .csv file
    specified in 'url'.
    """
    s = requests.get(url).content
    projects = pd.read_csv(io.StringIO(s.decode('utf-8')), error_bad_lines=False)

    if "Sample ID" in projects.columns:
        return projects\
            ["Sample ID"]\
            .unique()\
            .tolist()
    else:
        sys.exit("Something's wrong; is the EBI MG sample list URL still active and still contains column named 'Sample ID'?")


def _get_file_stream_handler(url_template, project_id):
    """
    Returns a file stream handler for the given URL.
    Get a url to retrieve the list of runs for a study from
    """
    print("---Getting the list of project runs for project " + project_id + "...")
    url_get_project_runs = url_template % (project_id)
    try:
        return urllib.urlopen(url_get_project_runs)
    #except URLError as url_error:
    #    print(url_error)
    #    raise
    except  IOError as io_error:
        print(io_error)
        raise
    except ValueError as e:
        print(e)
        print("---Could not retrieve any runs. Open the retrieval URL further down in your browser and see if you get any results back. Program will exit now.")
        print(url_get_project_runs)
        raise


def get_information(run_information_path, ofile_path, odir_path, version, study_url_template, project_information_url, sample_information_url):

    # get the projects list
    list_of_projects_all = get_list_of_projects(project_information_url)
    proj_to_remove = []# ["ERP012803"]
    list_of_projects = [p for p in list_of_projects_all if p not in proj_to_remove]
    list_of_projects = sorted(list_of_projects)
    #list_of_projects = ["ERP000118", "ERP000339"]# : encoding errors
    #list_of_projects = ['ERP001227', 'ERP001227']# NoneType no attr  "find"
    # ERP001506 ERP001568 still not
    print(list_of_projects)

    print("---Downloading information for "+str(len(list_of_projects))+" projects")


    # manage dirs and erase the previous content of the output file
    if not os.path.exists(odir_path):
        os.mkdir(odir_path)
    if not os.path.exists(ofile_path):
        os.mknod(ofile_path)
    if not os.path.exists(run_information_path):
        os.mknod(run_information_path)
    open(run_information_path, 'w').close()
    open(ofile_path, 'w').close()

    errors = 0
    errors_on_projects = []
    total_n_runs = 0

    with open(run_information_path, 'a') as ofile:
        header = "project_id\tsample_id\trun_id\tclassification\texperiment_type\tbiome\texperimental_feature\tmaterial\tenvo\tuberon\tsample_description\n"
        ofile.write(header)

        for project_id in list_of_projects:
            # Retrieve a file stream handler from the given URL and iterate over each line (each run) and build the download link using the variables from above
            file_stream_handler = _get_file_stream_handler(study_url_template, project_id)
            #py3: reader = unicodecsv.reader(file_stream_handler, delimiter=',', encoding = 'utf-8')
            reader = csv.reader(file_stream_handler, delimiter=',')


            try:
                for project_id, sample_id, run_id in reader:
                    total_n_runs +=1
                    #print(project_id, sample_id, run_id)

                    try:

                        # get run information
                        run_information_url = run_information_url_template % (project_id, sample_id, run_id, version)
                        rpage = requests.get(run_information_url)
                        rtree = html.fromstring(rpage.content)

                        data_fields = [i.text for i in rtree.findall('.//div[@class="result_row_data"]')]
                        data_fields = ["NaN" if i is None else i.strip() for i in data_fields]

                        label_fields = [i.text for i in rtree.findall('.//div[@class="result_row_label"]')]
                        label_fields = ["NaN" if i is None else i.lower().rstrip(":").replace(" ", "_") for i in label_fields]

                        rfields = {l:d for l,d in zip(label_fields, data_fields)}

                        for key in ['classification', 'sample_name', 'project_name', 'experiment_type', 'pipeline_version', 'analysis_date', 'biome', 'experimental_feature', 'material']:
                            if key not in rfields.keys():
                                rfields[key] = "NaN"

                        classification, sample_name, project_name, experiment_type, pipeline_version, analysis_date, biome, experimental_feature, material\
                            = rfields['classification'],\
                            rfields['sample_name'],\
                            rfields['project_name'],\
                            rfields['experiment_type'],\
                            rfields['pipeline_version'],\
                            rfields['analysis_date'],\
                            rfields['biome'],\
                            rfields['experimental_feature'],\
                            rfields['material']

                    except ValueError as e:
                        print(rfields)
                        classification, experiment_type, biome, experimental_feature, material = "NaN", "NaN", "NaN", "NaN", "NaN"
                        errors =+ 1
                        errors_on_projects.append(project_id)
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except NameError as e:
                        classification, experiment_type, biome, experimental_feature, material = "NaN", "NaN", "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except TypeError as e:
                        classification, experiment_type, biome, experimental_feature, material = "NaN", "NaN", "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except IndexError as e:
                        classification, experiment_type, biome, experimental_feature, material = "NaN", "NaN", "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except AttributeError as e:
                        classification, experiment_type, biome, experimental_feature, material = "NaN", "NaN", "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except KeyError as e:
                        print(rfields)
                        classification, experiment_type, biome, experimental_feature, material = "NaN", "NaN", "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except KeyboardInterrupt as e:
                        print(errors_on_projects)
                        sys.exit()
                    except:
                        print(rfields)
                        classification, experiment_type, biome, experimental_feature, material = "NaN", "NaN", "NaN", "NaN", "NaN"
                        print("---Some other error", sys.exc_info())
                        print(run_information_url)
                        print(sample_information_url)
                        pass


                    try:
                        # get sample information
                        sample_information_url = sample_information_url_template % (sample_id)
                        spage = requests.get(sample_information_url)
                        stree = html.fromstring(spage.content)
                        sfields = [i.text for i in stree.findall('.//value')]
                        sfields = ["NaN" if i is None else i for i in sfields]

                        envo = "|".join([i for i in sfields if i.find("ENVO") != -1])
                        if not type(envo) == str:
                            envo = "NaN"
                        uberon = "|".join([i for i in sfields if i.find("UBERON") != -1])
                        if not type(uberon) == str:
                            uberon = "NaN"

                        if len(stree.findall('.//description')) > 0:
                            description = stree.findall('.//description')[0].text
                        else:
                            description = "NaN"
                        if not type(description) == str:
                            description = "NaN"

                    except ValueError as e:
                        envo, uberon, description = "NaN", "NaN", "NaN"
                        errors =+ 1
                        errors_on_projects.append(project_id)
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except NameError as e:
                        envo, uberon, description = "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except TypeError as e:
                        envo, uberon, description = "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except IndexError as e:
                        envo, uberon, description = "NaN", "NaN", "NaN"
                        print(e)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except AttributeError as e:
                        envo, uberon, description = "NaN", "NaN", "NaN"
                        print(e)
                        print(sfields)
                        print(run_information_url)
                        print(sample_information_url)
                        pass
                    except KeyboardInterrupt as e:
                        print(errors_on_projects)
                        sys.exit()
                    except:
                        envo, uberon, description = "NaN", "NaN", "NaN"
                        print("---Some other error", sys.exc_info()[0])
                        print(run_information_url)
                        print(sample_information_url)
                        pass

                    # Write line to output file
                    new_line = "\t".join([project_id, sample_id, run_id, classification,
                        experiment_type, biome, experimental_feature, material,
                        envo, uberon, description]) + "\n"
                    new_line = new_line.encode("utf-8")
                    ofile.write(new_line)

            except ValueError as e:
                print(e)
                print("Bo jest pare runs na jeden sample:")
                print([i for i in reader])
                errors =+ 1
                errors_on_projects.append(project_id)
                pass

            #except ValueError as e:
            #    errors =+ 1
            #    errors_on_projects.append(project_id)
            #    print(e)
            #    print(run_information_url)
            #    print(sample_information_url)
            #    pass
            #except NameError as e:
            #    print(e)
            #    print(run_information_url)
            #    print(sample_information_url)
            #    pass
            #except TypeError as e:
            #    print(e)
            #    print(run_information_url)
            #    print(sample_information_url)
            #    pass
            #except IndexError as e:
            #    print(e)
            #    print(run_information_url)
            #    print(sample_information_url)
            #    pass
            #except AttributeError as e:
            #    print(e)
            #    print(run_information_url)
            #    print(sample_information_url)
            #    pass
            #except:
            #    print("---Some other error", sys.exc_info()[0])
            #    print(run_information_url)
            #    print(sample_information_url)
            #    pass
            #except KeyboardInterrupt as e:
            #    print(errors_on_projects)
            #    sys.exit()
    return total_n_runs, errors, errors_on_projects


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A tool to retrieve project information, sample information and\
                        run IDs for all EBI metagenomics projects. Takes forever.")
    parser.add_argument("-o", "--output_path",
                        help="Location of the output directory (folder!), where the files get stored.**MANDATORY**",
                        required=True)
    parser.add_argument("-v", "--version", help="Version of the pipeline used to generate the results.**MANDATORY**",
                        required=True)
    args = vars(parser.parse_args())

    version = args['version']
    odir_path = args['output_path'] + "/"
    ofile_path = odir_path + "full_information_v" + version + ".tsv"
    run_information_path = odir_path + "run_information.tsv"
    print(run_information_path)

    root_url = "https://www.ebi.ac.uk"
    study_url_template = root_url + "/metagenomics/projects/%s/runs"
    run_information_url_template = root_url + "/metagenomics/projects/%s/samples/%s/runs/%s/results/overview/versions/%s"
    sample_information_url_template = root_url + "/ena/data/view/%s&display=xml"

    sample_information_url = "https://www.ebi.ac.uk/metagenomics/samples/doExportTable?searchTerm=&sampleVisibility=ALL_PUBLISHED_SAMPLES&search=Search&startPosition=0"
    project_information_url = "https://www.ebi.ac.uk/metagenomics/projects/doExportDetails?search=Search&studyVisibility=ALL_PUBLISHED_PROJECTS"

    urllib.URLopener().retrieve(sample_information_url, odir_path + "all_samples.tsv")
    urllib.URLopener().retrieve(project_information_url, odir_path + "all_projects.tsv")

    total_n_runs, errors, errors_on_projects = get_information(run_information_path, ofile_path, odir_path, version, study_url_template, project_information_url, sample_information_url)

    print("--- There were "+str(total_n_runs)+" total runs across all projects.")
    print("--- There were "+str(errors)+" ValueErrors during processing these projects:")
    print(errors_on_projects)

    print("--- Integrating information")
    whole_table = integrate_information(project_information_url, sample_information_url, run_information_path)
    whole_table.to_csv(ofile_path, sep="\t")

    print("--- Program finished.")
