#!/usr/bin/env python3

'''
summarize:
    - no of samples, projects, runs (current - get from website)
    - no of actually downloaded
        - listr prjs that have any data downloaded for different -t types and -v version
    - sumarize metadata

    - OTU frequency (sorted)
        - global?
        - by biome?
'''

import subprocess
import pandas as pd
import argparse
import os
#was: from math import nan, isnan, inf, isinf
from math import isnan, isinf

from metadata_download import get_list_of_projects
from metadata_download import get_list_of_samples

def merge_by_run_id(data, metadata):
    #TODO extend to tolarate varying col names
    metadata.index = metadata['run_id']
    data.index = data['run ID']
    #metadata_, data_ = metadata.align(data, axis=0, join='right')
    #data_.insert(0, "biome", metadata_['biome'])
    data_ = pd.merge(data, metadata[["biome", "project_id"]], left_index=True, right_index=True, how="left")
    return data_

def tax_correlations(A):
    tax_dist = pd.DataFrame(pairwise_distances(A.T),
        columns = A.columns, index = A.columns)
    return tax_dist.stack().reset_index()


def n_new_prj(list_of_projects_all, md):
    n_new_prj = len(set(list_of_projects_all)) - len(set(md.project_id.tolist()))
    print("--- There are", n_new_prj, "new projects since last download.")

def ebi_website_info():
    ### information as listed on the website
    project_information_url = "https://www.ebi.ac.uk/metagenomics/projects/doExportDetails?search=Search&studyVisibility=ALL_PUBLISHED_PROJECTS"
    sample_information_url = "https://www.ebi.ac.uk/metagenomics/samples/doExportTable?searchTerm=&sampleVisibility=ALL_PUBLISHED_SAMPLES&search=Search&startPosition=0"


    list_of_projects_all = get_list_of_projects(project_information_url)
    list_of_samples_all = get_list_of_samples(sample_information_url)

    #print("\n\n")
    #print("--- Information on the website:")
    #print("------ There are ", len(list_of_projects_all), " projects listed on the EBI website.")
    #print("------ There are ", len(list_of_samples_all), " samples listed on the EBI website.")
    return list_of_projects_all, list_of_samples_all

def ebi_summarize_data_main(metadata_fp, huge_matrix_with_all_runs_fp,
                            list_of_projects_all, list_of_samples_all):

    ### information as in metadata
    md = pd.read_csv(metadata_fp, sep="\t")
    n_new_prj(list_of_projects_all, md)

    biome_freq = md[['biome', 'sample_id']].drop_duplicates()\
        ['biome'].value_counts()

    # how many samples per project?
    sam_per_prj = md[["sample_id", "project_id"]].drop_duplicates()['project_id'].value_counts()

    #print("\n\n")
    #print("--- Samples in project as per metadata:")
    #print(sam_per_prj.describe())


    ### information as gathered from the MERGED files (rather than downloaded; get only those that actually are data)
    #TODO capture the prints of regular_files_merge.py?
    huge_matrix_with_all_runs = pd.read_csv(huge_matrix_with_all_runs_fp, sep="\t")
    tax_freq = huge_matrix_with_all_runs[['taxonomy', 'counts']].groupby('taxonomy').sum()\
        .sort_values('counts', ascending=False)

    #print("\n\n")
    #print("--- Taxonomy counts as per merged .tsv files:")
    #print(tax_freq[:49])


    # by biome
    # OTU / tax by biome
#    tax_freq_by_biome = merge_by_run_id(huge_matrix_with_all_runs, md)
# MemError:
#    tax_freq_by_biome = tax_freq_by_biome[['taxonomy', 'biome', 'counts']].groupby('biome')\
#        .apply(lambda x: x.groupby('taxonomy').sum()\
#        .sort_values('counts', ascending=False))


    #print("\n")

    return sam_per_prj, biome_freq, tax_freq#, tax_freq_by_biome

def tax_freq_for_biome():
    #TODO
    pass

def check_df(M):
    try:
        # also inf
        print("--- NaN count:",
                M.applymap(isnan).sum().sum()
                )
    except TypeError:
        pass
    print("--- Number of row sums equal to 0:",
            sum(M.sum(0) == 0)
            )
    print("--- Number of col sums equal to 0:",
            sum(M.sum(1) == 0)
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
                        ")
    parser.add_argument("-m", "--metadata_path",
                        help="",
                        required=False)
    parser.add_argument("-t", "--tsv_matrix_path",
                        help="",
                        required=False)
    parser.add_argument("-v", "--mapping_dir",
                        help="",
                        required=False)
    args = vars(parser.parse_args())

    print("---Arguments not implemented! Hardcoded paths.\n")

    #huge_matrix_with_all_runs = args['tsv_matrix_path']

    #DEB
    metadata_fp = "/D/ebi/DEFAULT_METADATA.tsv"
    huge_matrix_with_all_runs_fp = "/D/ebi/A_OTU-TSV_v2.tsv"
    #DEB

    ebi_summarize_data_main(metadata_fp, huge_matrix_with_all_runs_fp)
