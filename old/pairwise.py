#!/usr/bin/env python3

'''
Anything that has to do with distances, correlations, (dis)similarities etc.
between observations, as well as some higher-level means for comparing the
similarity matrices.
'''

import argparse
import pandas as pd
import numpy as np
import sys
from scipy.stats import pearsonr, fisher_exact, spearmanr
from scipy.spatial import distance_matrix # euclidean
from scipy.cluster.hierarchy import dendrogram, linkage
from skbio.stats.distance import mantel
import fisher

from sklearn.preprocessing.data import scale
from sklearn.preprocessing import normalize, binarize
from sklearn.metrics.pairwise import pairwise_distances, pairwise_kernels
from sklearn.metrics import mutual_info_score
from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction.text import TfidfTransformer

from preprocess import correspondence_analysis_transform
from skrzynka import delete_empty, add_index_level

N_JOBS = 8


def notes_pairwise():
    print("NOTES FOR pairwise.py:\n\
        from sklearn.neighbors import DistanceMetric\n\
        ")

# ==============================================================================
# (DIS)SIMILARITY MEASURE CALCULATION


def current_raw_to_dist(A, n_jobs = N_JOBS):
    tf_transformer = TfidfTransformer(sublinear_tf = True)
    A = delete_empty(A)
    A = pd.DataFrame(tf_transformer.fit_transform(A).toarray(),
                            index=A.index, columns=A.columns
                            )
    D = pairwise_distances_pd(A, metric='cosine', n_jobs = n_jobs)
    #D.index.names = A.columns.names
    #D.columns.names = A.columns.names
    return D

def current_transformation(A):
    tf_transformer = TfidfTransformer(sublinear_tf = True)
    A = pd.DataFrame(tf_transformer.fit_transform(A).toarray(),
            index=A.index, columns=A.columns)
    #D.index.names = A.columns.names
    #D.columns.names = A.columns.names
    return A

def correlation_(v,w):
    return pearsonr(v,w)[0]
    # return np.corrcoef(v,w)[0,1] # also valid


def contingency_table_(v,w):
    v = [bool(i) for i in v]
    w = [bool(i) for i in w]
    ct = np.array([
            [   np.sum([not i and not j for i,j in zip(v,w)]),
                np.sum([not i and j for i,j in zip(v,w)])],
            [   np.sum([i and not j for i,j in zip(v,w)]),
                np.sum([i and j for i,j in zip(v,w)])]]
        )
    return ct


def contingency_table_pandas_(v,w):
    v, w = [bool(i) for i in v], [bool(i) for i in w]
    x = pd.DataFrame([v,w]).T
    ct = pd.crosstab(x[0], x[1])
    return ct

def fisher_exact_(v,w):
    ct = contingency_table_(v,w)
    # old
    #oddsratio, pvalue = fisher_exact(ct)
    #return pvalue
    #
    ct = contingency_table_(v,w)
    pvalue = fisher.pvalue(*ct.ravel())
    #left_tail, right_tail, two_tail
    return pvalue.two_tail

def pairwise_distances_pd(A, n_jobs=N_JOBS, **kwargs):
    '''
    MNEMONIC adds fisher_exact_, correlation_
    '''
    return pd.DataFrame(
                pairwise_distances(A, n_jobs=n_jobs, **kwargs),
            columns = A.index, index = A.index)

def pairwise_kernels_pd(A, **kwargs):
    '''
    The data is assumed to be non-negative, and is often normalized to have an
    L1-norm of one. The normalization is rationalized with the connection to
    the chi squared distance, which is a distance between discrete probability
    distributions.

    The chi squared kernel is most commonly used on histograms (bags) of visual words.
    '''
    return pd.DataFrame(
                pairwise_kernels(A, **kwargs),
            columns = A.index, index = A.index)


def correspondence_analysis(A, axis = 0, n_jobs=N_JOBS):
    '''
    The input matrixmust have no zero col sums nor row sums! (else F_expected has zeros)
    '''
    if axis == 1:
        A = A.T
    r = delete_empty(A)
    A = correspondence_analysis_transform(A)

    o = pd.DataFrame(
        # old: distance_matrix(A, A),
        pairwise_distances(A, metric = "euclidean", n_jobs=n_jobs),
        columns = A.index, index = A.index
        )

    if axis == 1:
        o = o.T

    return o



# ==============================================================================
# CONVERSION OF (DIS)SIMILARITY MEASURES

#def n_overlapping_(A, c1):
    #c_out = A.apply(lambda c2: n_overlapping_(c1, c2))
    #return c_out


def n_overlapping(A, n_jobs=N_JOBS):
    # change as is fisher = binarize vectors and use pairwise_distances_pd
    #A_out = pairwise_distances(A, metric=n_overlapping_, labels = A.columns)
    A = pd.DataFrame(binarize(A), columns = A.columns, index = A.index)
    #TODO binarize with a cutoff e.g. below 10 -> 0
    A_out = pairwise_distances_pd(A, metric=n_overlapping2_, n_jobs=n_jobs)#, labels = A.columns)
    return A_out

def n_overlapping_(v1, v2, labels):
    v1 = set(labels[v1 != 0])
    v2 = set(labels[v2 != 0])
    return len(v1.intersection(v2))

def n_overlapping2_(v1,v2):
    return sum([i and j for i, j in zip(v1, v2)])


def dist_to_kern(D):
    print('This function is notes.')
    gamma = 1/ D.shape[1]
    S = np.exp(-D * gamma) # where one heuristic for choosing gamma is 1 / num_features
    #S = 1. / (D / np.max(D))
    return S

def simi_to_dist(S):
    print('This function is notes. Also good for MI')
    #D = S-S.max()
    gamma = 1/ S.shape[1]
    D = -(np.log(S) / gamma)
    return D


def mi_to_simi(S):
    print('This function is experimental notes.')
    maxvals = S.max()
    n = S.shape[0]
    inames = S.index

    S = np.array(S)
    D = np.zeros_like(S)
    for c in range(n):
        for r in range(n):
            D[r,c] = S[r,c] - maxvals[r]/2 - maxvals[c]/2

    D = pd.DataFrame(D, index=inames, columns=inames).applymap(lambda x: round(x,6))
    D = normalise_my(abs(D))
    #D = normalize(abs(D))
    #D = D.applymap(abs)
    #D = D.applymap(lambda x: D.max().max() - x)
    return D

# ==============================================================================
# COLLAPSING

def collapse_pairwise(D, md, by, index_name=None, fun='mean'):
    return 'use skrzynka.skrzynka.collM'


# ==============================================================================
# SUMMARY-LIKE TOOLS

def pairwise_to_long(D, colnames=['sample1', 'sample2', 'metric'], delete_self_self=False):
    D = D.stack()
    D.index.names = colnames[:2]
    o = D.reset_index()\
        .rename(columns={0:colnames[2]})

    if delete_self_self:
        o = o[ o[colnames[0]] != o[colnames[1]] ]

    return o


def summarize_pairwise(A_or_list_of_pairwise, colnames=None):
    A = A_or_list_of_pairwise


    if isinstance(A_or_list_of_pairwise, list):
        #A = [pairwise_to_long(A_, colnames=colnames) for A_ in A]
        #o = pd.concat(A, axis=1)

        o = pairwise_to_long(A[0], colnames=colnames[:3])
        for i,A_ in enumerate(A):
            c = colnames[:2]
            c.append(colnames[i+2])
            o = pd.merge(
                o,
                pairwise_to_long(A_, c)
                )

    else:
        o = pd.merge(
                pairwise_to_long(n_overlapping(A),
                                colnames=['sample1', 'sample2', 'n_overlapping']),
                pairwise_to_long(pairwise_distances_pd(A, metric=correlation_),
                                colnames=['sample1', 'sample2', 'correlation']),
                #pairwise_to_long(correspondence_analysis(A)),
                #pairwise_to_long(mutual_information(A)),
                on = ['sample1', 'sample2']
                )
    return o


def get_extremal(pw):
    '''
    Intended for reference purposes mostly

    Get entities with highest and lowest (dis)similarity scores.
    Input:  Pairwise matrix
    '''
    top = {}
    bot = {}
    for c in pw.columns[2:]:
        print(c)
        top[c] = pw[c].sort_values(ascending=False).index.tolist()
        bot[c] = pw[c].sort_values(ascending=True).index.tolist()

    return top, bot

def get_validation_entities_list(pw, n):
    '''
    Intended for reference purposes mostly

    Get a list of interesting entities (taxons, samples, etc) for
    manual validation purposes.
    Input:  Pairwise matrix
    '''
    print('convert dist to 1/dist')
    top, bot = get_extremal(pw)
    top = list_intersection(
            [i[:n] for i in top.values()]
            )
    bot = list_intersection(
            [i[:n] for i in bot.values()]
            )

    return top, bot



def hclust(D):
    #TODO
    return 'ni'
    ward = AgglomerativeClustering(n_clusters=6, linkage='ward').fit(X)
    ward = AgglomerativeClustering(n_clusters=6, connectivity=connectivity,
                                   linkage='ward').fit(X)
    label = ward.labels_

def R_glm_pairwise():
    '''
    Output: a matrix of p-values?
    '''
    #TODO
# https://www.r-bloggers.com/do-not-log-transform-count-data-bitches/
#    library(multcomp)
#### set up all pair-wise comparisons for count data
#    data(Titanic)
#    mod <- glm(Survived ~ Class, data = as.data.frame(Titanic), weights = Freq, family = binomial)
#
#    ### specify all pair-wise comparisons among levels of variable "Class"
#    ### Note, Tukey means the type of contrast matrix.  See ?contrMat
#    glht.mod <- glht(mod, mcp(Class = "Tukey"))
#
#    ###summaryize information
#    ###applying the false discovery rate adjustment
#    ###you know, if that's how you roll
#    summary(glht.mod, test=adjusted("fdr"))
    return 'ni'


####
# COMPARISON
####

def compare_similarity_matrices(dict_of_matrices,
                method='pearson', permutations=999, alternative='two-sided'):
    #TODO: Input:  Two or more data frames that can be compared in  pairwise manner by row or col
    '''
    Input:  Two or more similarity matrices comparing the same entities
    Output: Similarity between each pair

    http://scikit-bio.org/docs/0.1.3/generated/skbio.math.stats.distance.mantel.html

    The Mantel test compares two distance matrices by computing the correlation
    between the distances in the lower (or upper) triangular portions of the
    symmetric distance matrices. Correlation can be computed using Pearson’s
    product-moment correlation coefficient or Spearman’s rank correlation coefficient.
    '''
    return 'ni'
    o = {}
    for S1,S2 in combinations(dict_of_matrices.keys(), 2):
        o[S1] = mantel( dict_of_matrices[S1],
                        dict_of_matrices[S2],
                        method=method,
                        permutations=permutations, alternative=alternative)




def temp_fun(Ai, name=""):

    tf_transformer = TfidfTransformer(sublinear_tf = True)
    At = tf_transformer.fit_transform(Ai.T).toarray().T
    tax_dist_bc = pd.DataFrame(pairwise_distances(At.T, metric='braycurtis'),
                        columns = Ai.columns, index = Ai.columns)
    tax_dist_cos = pd.DataFrame(pairwise_distances(At.T, metric='cosine'),
                        columns = Ai.columns, index = Ai.columns)

    tf_transformer = TfidfTransformer(sublinear_tf = True)
    As = tf_transformer.fit_transform(Ai).toarray()
    sam_dist_bc = pd.DataFrame(pairwise_distances(As, metric='braycurtis'),
                        columns = Ai.index, index = Ai.index)
    sam_dist_cos = pd.DataFrame(pairwise_distances(As, metric='cosine'),
                        columns = Ai.index, index = Ai.index)

    tax_dist_bc.to_csv( '/P/metag/data/'+name+'_tfidf_feature_dist_bc.tsv', sep='\t')
    tax_dist_cos.to_csv('/P/metag/data/'+name+'_tfidf_feature_dist_cos.tsv', sep='\t')
    sam_dist_bc.to_csv( '/P/metag/data/'+name+'_tfidf_sample_dist_bc.tsv', sep='\t')
    sam_dist_cos.to_csv('/P/metag/data/'+name+'_tfidf_sample_dist_cos.tsv', sep='\t')

    return tax_dist_bc, tax_dist_cos, sam_dist_bc, sam_dist_cos




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
                        TODO: Replace with sklearn.metrics.pairwise.pairwise_distances: parallelisation. | \
                        Calculates the specified pairwise distances between either rows or columns.\
                        Note: Don't use prior normalisation for the Bray-Curtis distance.\
                        ")
    parser.add_argument("-a", "--axis",
                        help='Whether distances between rows ("0") or columns ("1").',
                        required=True)
    parser.add_argument("-m", "--distance_metric",
                        help='One of the metric available in sklearn.metrics.pairwise_distances, e.g. "euclidean", "braycurtis"',
                        required=True)
    parser.add_argument("-i", "--input_path",
                        help="Matrix of counts / frequencies / etc.",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        help="Matrix of distances.",
                        required=True)
#    parser.add_argument("-q", "--query_path",
#                        help="An optional path to a query data.",
#                        required=False)
    parser.add_argument("-j", "--n_jobs",
                        help="Number of jobs to use.",
                        required=True)
    args = vars(parser.parse_args())

    axis       = args['axis']
    ifile_path = args['input_path']
    ofile_path = args['output_path']
    #query_path = args['query_path']
    metric     = args['distance_metric']
    N_JOBS     = int(args['n_jobs'])


