README
================================================================================

MetageNomic Experiment Mining to create an OTU Network of Inhabitant Correlations

Installation with virtualenv
--------------------------------------------------------------------------------

.. code:: bash

    pip install --user virtualenv
    mkdir workspace
    cd workspace
    git clone http://gitlab.com/wrenlab/mnemonic.git
    virtualenv venv
    . venv/bin/activate
    cd mnemonic
    pip install .
    cd ..
    #TODO: Download data
    python -m mnemonic.report

