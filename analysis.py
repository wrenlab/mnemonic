import mnemonic.literature
import mnemonic.data.AGP

def fn():
    DS = mnemonic.data.AGP.AGPDataset()
    fit = DS.fit()
    L = mnemonic.literature.IMI()


#def HFD():
#    MI = M.MI_contrast_frequency()
#    print(MI["O"].loc[:,"high-fat diet"].sort_values())

@memoize(ignore=["DS"])
def AGP_contrast(DS, key):
    return DS.fit(key)

@memoize
def AGP_contrast_species(key):
    DS = AGPDataset()
    fit = AGP_contrast(DS, key)
    fit = fit.loc[[ix for ix in fit.index if ix in DS.taxonomy.common_name],:]
    fit.index = [DS.taxonomy.common_name[ix] for ix in fit.index]
    return fit

@memoize
def AGP_contrasts():
    DS = AGPDataset()
    o = {}
    for k in DS.A.columns:
        try:
            print(k)
            fit = AGP_contrast(DS, k)
            if fit is not None:
                o[k] = fit
        except:
            continue
    return o

def contrasts(DS):
    o = pd.read_csv("data/l2fcXtaxa.tsv", sep="\t", index_col=0)
    o = o.loc[[ix for ix in o.index if (not ix.startswith("GO:")) and (ix in DS.taxonomy.M)],:]
    o.index = [DS.taxonomy.M[k] for k in o.index]
    o = o.loc[[ix for ix in o.index if ix in DS.taxonomy.common_name],:]
    o.index = [DS.taxonomy.common_name[ix] for ix in o.index]
    return o

@memoize
def MI_contrast():
    DS = AGPDataset()
    X = contrasts(DS)
    Y = MI()
    o = {}
    for k,YY in Y.items():
        XX, YY = X.align(YY, join="inner", axis=0)
        o[k] = corr(XX,YY)
    return o

def MI_contrast_frequency():
    M = MI_contrast()
    o = {}
    for k,v in M.items():
        v = v.loc[[ix for ix in v.index if "FREQUENCY" in ix and not any([q in ix for q in REMOVE])],:]
        v.index = list(map(fix_frequency_term, v.index))
        o[k] = v
    return o

@memoize
def MI_combined_contrast():
    X = combined_frequency_contrasts().SLPV.drop("Intercept", axis=1)
    Y = IMI()
    o = {}
    for k,YY in Y.items():
        XX, YY = X.align(YY, join="inner", axis=0)
        o[k] = corr(XX,YY)
    return o

REMOVE = ["POOL", "TEETHBRUSHING", "FLOSSING", "COSMETICS", "SUPPLEMENT", "SMOKING"]
def fix_frequency_term(ix):
    o = ix.rsplit("_", 1)[0].replace("_", " ").lower()
    if o.startswith("vitamin"):
        o = o.split(" ")
        o[1] = o[1].upper()
        o = " ".join(o)
    #if o == "milk cheese":
    #    o = "milk cheese"
    #if o == "meat eggs":
    #    o = "meat eggs"
    return o.replace(" ", "_")

def fix_frequency_index(X):
    o = X.copy()
    o = o.loc[[ix for ix in o.index if "FREQUENCY" in ix and not any([q in ix for q in REMOVE])],:]
    o.index = list(map(fix_frequency_term, o.index))
    return o

@memoize
def frequency_contrasts():
    DS = AGPDataset()
    o = contrasts(DS)
    o = o.loc[:,[ix for ix in o.columns if "FREQUENCY" in ix and not any([q in ix for q in REMOVE])]]
    o.columns = list(map(fix_frequency_term, o.columns))
    return o

def age_MI():
    fit = AGP_contrast_species("AGE_YEARS")
    Y = MI()
    o = {}
    for k,v in Y.items():
        o[k] = corr(fit, v).T
    return o

@memoize
def combined_frequency_contrasts(include_age=False):
    """
    """
    DS = AGPDataset()
    fit = DS.lm_fit(include_age=include_age)
    return fit

@memoize
def species_data():
    DS = AGPDataset()
    return DS.data()

def spearman(x):
    nc = x.shape[1]
    o = np.zeros((nc,nc))
    for i in range(nc):
        for j in range(nc):
            if j > i:
                continue
            if j == i:
                o[i,j] = 0
                continue
            v1 = x.iloc[:,i]
            v2 = x.iloc[:,j]
            r = scipy.stats.spearmanr(*wrenlab.align_series(v1,v2))[0]
            distance = 2 * (1 - abs(r))
            o[i,j] = distance
            o[j,i] = distance
    o = pd.DataFrame(o, index=x.columns, columns=x.columns)
    o = o.loc[:,~(o == 0).all(axis=0)]
    o = o.loc[~(o == 0).all(axis=1),:]
    return o

def validate1():
    # Probably using a better distance metric than Pearson will push several of these over the edge
    import skbio.stats.distance
    X = (1 + species_data()).apply(np.log).corr()
    #CT = frequency_contrasts().dropna().T.corr()
    #CCT = combined_frequency_contrasts(include_age=False).SLPV.T.corr()
    #CCTA = combined_frequency_contrasts(include_age=True).SLPV.T.corr()

    CT = spearman(frequency_contrasts().dropna().T)
    CCT = spearman(combined_frequency_contrasts(include_age=False).SLPV.T)
    CCTA = spearman(combined_frequency_contrasts(include_age=True).SLPV.T)

    def L(key):
        o = IMI()[key]
        o = wrenlab.util.pandas.trim_missing(o, column_threshold=0.25)
        return wrenlab.util.pandas.trim_missing(o, row_threshold=0.25).T.corr()
    def cdist(X):
        #return 2 * (1 - X.abs())
        return spearman(X)
    def fn(X,Y):
        X,Y = X.align(Y, axis=None, join="inner")
        return skbio.stats.distance.mantel(cdist(X),cdist(Y))

    ix,o = [],[]
    for k1 in ["CT", "CCT", "CCTA"]:
        C = locals()[k1]
        for k,v in IMI().items():
            if (not k.startswith("G")) and (v.shape[1] > 100):
                try:
                    o.append(fn(C, L(k)))
                    ix.append((k1,k))
                except Exception as e:
                    print(e)
                    continue

    """
    ix,o = [],[]
    for v1 in vars:
        x = locals()[v1]
        for v2 in vars:
            if v1 <= v2:
                continue
            y = locals()[v2]
            ix.append(v1 + "-" + v2)
            o.append(fn(x,y))
    """

    ix = pd.MultiIndex.from_tuples(ix, names=["Contrast", "Literature"])
    o = pd.DataFrame.from_records(o)
    o.columns = ["coef", "p", "N"]
    o.index = ix
    return o.sort_values("p")
