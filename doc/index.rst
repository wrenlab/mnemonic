.. MNEMONIC documentation master file, created by
   sphinx-quickstart on Thu Jun  7 13:06:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MNEMONIC documentation
================================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. include:: ../README.rst


Report
================================================================================

.. automodule:: mnemonic.report
    :members:

.. automodule:: mnemonic.paper_figures
    :members:

Data
================================================================================

.. automodule:: mnemonic.taxonomy
    :members:

.. automodule:: mnemonic.data.AGP
    :members:

.. automodule:: mnemonic.data.AGP_variables
    :members:

.. automodule:: mnemonic.data.ProTraits
    :members:

.. automodule:: mnemonic.data.ProTraits_variables
    :members:

.. automodule:: mnemonic.data.EBI
    :members:

Terms
================================================================================
.. automodule:: mnemonic.contrast
    :members:

Indices and tables
================================================================================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
