import mnemonic.util
import mnemonic.data.AGP

def test_configuration():
    cfg = mnemonic.util.get_configuration()
    assert "data" in cfg
    assert "root" in cfg["data"]
