import mnemonic.data.AGP

def test_AGP_dataset():
    DS = mnemonic.data.AGP.dataset()
    assert DS.X.shape[0] > 0

def test_AGP_dendrogram():
    ct = mnemonic.data.AGP.contrasts()
    ct.dendrogram("img/frequency_solo_dendrogram.pdf")

def test_AGP_combined_dendrogram():
    pass
