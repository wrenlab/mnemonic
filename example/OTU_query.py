import sys

import mnemonic.data.AGP as AGP

if __name__ == "__main__":
    species = "Streptomyces mirabilis"
    DS = AGP.dataset()
    q = DS.OTU(species)
    df = q.profile_similarity()
    df.head(10).round(2).to_csv(sys.stdout, sep="\t")

