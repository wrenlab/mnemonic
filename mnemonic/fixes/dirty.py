from mnemonic.data.AGP_variables import AGPVarCats
from mnemonic.data import AGP
from mnemonic.data.ProTraits_variables import PTVarCats
from mnemonic.literature import IMI
from mnemonic.taxonomy import TaxonomyLevel
import mnemonic.data.case_study as CS
import seaborn as sns
import matplotlib.pyplot as plt
import os
from mnemonic.util import memoize

output_dir = 'all_plots'
os.makedirs(output_dir, exist_ok=True)


@memoize
def agp_food():
    return AGP.contrasts('food', level=TaxonomyLevel.SPECIES)


def mass_plots_AGP(odir = output_dir):
    odir = os.path.join(odir, 'clustermap')
    os.makedirs(odir, exist_ok=True)

    AGP_categories = AGPVarCats().ls_major_categories()
    literature_categories = IMI().keys()
    taxonomy_levels = [i for i in TaxonomyLevel]
    #AGP_categories = ['food']#, 'mental disease']
    #literature_categories = ['D']#, 'Dr']
    #taxonomy_levels = [TaxonomyLevel.FAMILY]
    #taxonomy_levels = [TaxonomyLevel.SPECIES]

    print("--- AGP:", AGP_categories)
    print("--- lit:", literature_categories)
    print("--- tax:", taxonomy_levels)

    issuesw = []

    for level in taxonomy_levels:
        print('--- Calculating taxonomy level:', level.name)
        for agp_cat in AGP_categories:
            print('--- Calculating AGP category:', agp_cat)

            try:
                ct = AGP.contrasts(agp_cat, level=level)

                plt.clf()
                fname = os.path.join(odir, 'agp-' + agp_cat + 'Xtax_' + level.name.lower() + '.png')
                #return ct
                ct.clustermap().savefig(fname, dpi=300, transparent=True, papertype='a4')

                #for lit_cat in literature_categories:
                #    print('--- literature category:', lit_cat)
                #    try:
                #        lct = ct.literature_associations(lit_cat)
                #        plt.clf()
                #        fname = os.path.join(odir, 'agp-' + agp_cat + 'Xlit-' + lit_cat + '_' + level.name.lower() + '.png')

                #        lct.clustermap(standard_scale=0, param='r').savefig(fname)
                #    except Exception as e:
                #        issuesw.append(str(agp_cat) + " " + str(lit_cat))
                #        print('--- Problems:', agp_cat, lit_cat)
                #        print(e)

            except Exception as e:
                issuesw.append(str(agp_cat))
                print('--- Problems:', agp_cat)
                print(e)

    print(issuesw)

def mass_plots_DA(odir = output_dir):
    odir = os.path.join(odir, 'DA')
    os.makedirs(odir, exist_ok=True)

    #AGP_categories = ['food', 'physical disease', 'mental disease', 'allergy']
    #AGP_categories = ['food']
    #AGP_categories = ['mental disease']
    #taxonomy_levels = [TaxonomyLevel.SPECIES]

    taxonomy_levels = [i for i in TaxonomyLevel]
    AGP_categories = AGPVarCats().ls_major_categories()

    print("--- AGP:", AGP_categories)
    print("--- tax:", taxonomy_levels)

    issuesw = []

    for level in taxonomy_levels:
        print('--- Calculating taxonomy level:', level.name)
        for agp_cat in AGP_categories:
            print('--- Calculating AGP category:', agp_cat)
            try:
                ct = AGP.contrasts(agp_cat, level=level)

                for term in ct.keys():
                    print('--- Calculating term:', term)
                    os.makedirs(os.path.join(odir, term), exist_ok=True)

                    fname = os.path.join(odir, term, 'agp-' + agp_cat + '-' +\
                        term.replace(' ', '_') + '_' + level.name.lower() + '_{0}' + '.png')

                    try:
                        plt.clf()
                        ct[term].plot.bar()
                        plt.savefig(fname.format('bar'), dpi=300, transparent=True, papertype='a4')

                        plt.clf()
                        ct[term].plot.globalFC()
                        plt.savefig(fname.format('globalFC'), dpi=300, transparent=True, papertype='a4')

                        plt.clf()
                        ct[term].plot.MA()
                        plt.savefig(fname.format('MA'), dpi=300, transparent=True, papertype='a4')

                        plt.clf()
                        ct[term].plot.volcano()
                        plt.savefig(fname.format('volcano'), dpi=300, transparent=True, papertype='a4')

                    except Exception as e:
                        issuesw.append(str(agp_cat) + " " + str(term))
                        print('--- Problems:', agp_cat, term)
                        print(e)

            except Exception as e:
                issuesw.append(str(agp_cat))
                print('--- Problems:', agp_cat)
                print(e)

    print(issuesw)

def mass_plots_metadata(odir = output_dir, only_counts=False, plot_AGP = True, plot_PT = True):
    odir = os.path.join(odir, 'metadata')
    os.makedirs(odir, exist_ok=True)
    os.makedirs(os.path.join(odir, "AGP"), exist_ok=True)
    os.makedirs(os.path.join(odir, "PT"), exist_ok=True)

    fname = os.path.join(odir, '{0}-{1}_categories.png')

    a = AGPVarCats()
    p = PTVarCats()

    plt.figure(figsize=(6,6))

    plt.clf()
    a.plot_var_counts(major=False)
    plt.savefig(fname.format("AGP", "detailed"), dpi=300, transparent=True, papertype='a4')
    plt.close()

    plt.clf()
    a.plot_var_counts(major=True)
    plt.savefig(fname.format("AGP", "major-VioScreen"), dpi=300, transparent=True, papertype='a4')
    plt.close()

    plt.clf()
    a.plot_var_counts(major=True, vioscreen=False)
    plt.savefig(fname.format("AGP", "major"), dpi=300, transparent=True, papertype='a4')
    plt.close()

    plt.clf()
    p.plot_var_counts(major=False)
    plt.savefig(fname.format("PT", "detailed"), dpi=300, transparent=True, papertype='a4')
    plt.close()

    plt.clf()
    p.plot_var_counts(major=True)
    plt.savefig(fname.format("PT", "major"), dpi=300, transparent=True, papertype='a4')
    plt.close()

    if only_counts:
        return

    if plot_AGP:
        successes = []
        #for term in list(a.ls_variables()):
        for term in ['SUGARY_SWEETS_FREQUENCY', 'AGE_YEARS']:
            fname = os.path.join(odir, 'AGP', 'AGP-{0}.png')

            try:
                plt.clf()
                a.plot_single_var(term)
                plt.savefig(fname.format(term), dpi=300, transparent=True, papertype='a4')
                successes.append(term)
            except Exception as e:
                print(term)
                print(e)
        print("Success on:", successes)

    if plot_PT:
        successes = []
        for term in list(p.ls_variables()):#[:20]:
            fname = os.path.join(odir, 'PT', 'PT-{0}.png')

            try:
                plt.clf()
                p.plot_single_var(term)
                plt.savefig(fname.format(term), dpi=300, transparent=True, papertype='a4')
                successes.append(term)
            except Exception as e:
                print(term)
                print(e)
        print("Success on:", successes)





