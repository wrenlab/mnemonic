"""
.. module: ProTraits

ProTraits
--------------------------------------------------------------------------------

ProTraits data.

"""
import pandas as pd
import numpy as np

import mnemonic.util

def table():
    '''
    Fetch the ProTraits data in a table format.
    '''
    URL = "http://protraits.irb.hr/data/ProTraits_binaryIntegratedPr0.90.txt"
    path = mnemonic.util.download(URL)
    # NB: dropped TaxonID (first column)
    o = pd.read_csv(path, sep="\t", index_col=0, na_values=["?"]).iloc[:,:-1].iloc[:,1:]
    o.columns = ["_".join(k.split("_")).replace("_", " ") for k in o.columns]
    return o

def predictions():
    URL = "http://protraits.irb.hr/data/ProTraits_precisionScores.txt"
    path = mnemonic.util.download(URL)
    # NB: dropped TaxonID (first column)
    o = pd.read_csv(path, sep=";", na_values=["?"])
    #o = o.loc[:,["Organism_name", "Phenotype", "Integrated_score_+"]]
    o["Score"] = (o["Integrated_score_+"] + (1 - o["Integrated_score_-"])) / 2
    o = o.pivot("Organism_name", "Phenotype", "Score")
    o.columns = ["_".join(k.split("_")).replace("_", " ") for k in o.columns]
    o = o.drop("growth in groups", axis=1)
    return o
