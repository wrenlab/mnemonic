"""
.. module: ProTraits_variables

ProTraits_variables
--------------------------------------------------------------------------------

Functionality to fetch and browse the variables (terms) in ProTraits (http://protraits.irb.hr/).

"""

import pandas as pd
import numpy as np
import mnemonic.util




class PTVarCats():
    """
    Stores the variable category information
    as well as edited variable names.
    """
    def __init__(self):
        pt_url = "http://protraits.irb.hr/data/ProTraits_binaryIntegratedPr0.90.txt"
        path = mnemonic.util.download(pt_url)
        # NB: dropped TaxonID (first column)

        original_data = pd.read_csv(path, sep='\t')\
                    .drop('Unnamed: 426', axis=1, errors='ignore')
        vs = original_data.columns

        # ==============================================================================
        # VARIABLE NAMES
        # ==============================================================================

        category = {
        'bioticrelationship':"Biotic relationship",
        'cellarrangement':"Cell arrangement",
        'ecosystem':"Ecosystem",
        'ecosystemcategory':"Ecosystem",
        'ecosystemsubtype':"Ecosystem",
        'ecosystemtype':"Ecosystem",
        'energysource':"Energy source",
        'gram_stain':"Gram strain",
        'habitat':"Habitat",
        'host':"Host",
        'knownhabitats':"Habitat",
        'mammalian_pathogen':"Mammalian pathogen",
        'metabolism':"Metabolism",
        'oxygenreq':"Oxygen requirement",
        'phenotypes':"Phenotype",
        'shape':"Shape",
        'specificecosystem':"Ecosystem",
        'temperaturerange':"Temperature range",
        }

        assignment = {
        "foodproduction" : "food production",
        "strictaero" : "sctrict aerobe",
        "gastrointestinaltract" : "gastro-intestinal tract",
        "plantsexceptlegumes" : "plants (except legumes)",
        "carbondioxidefixation" : "carbon dioxide fixation",
        "intestinaltract" : "intestinal tract",
        "biomassdegrader" : "biomass degrader",
        "reproductivesystem" : "reproductive system",
        "humanintestinalmicroflora" : "intestinal",
        "plantslegumes" : "plants (legumes)",
        "strictanaero" : "strict anaerobe",
        "ironreducer" : "iron reducer",
        "cellulosedegrader" : "cellulose degrader",
        "urogenital_reproductive" : "urogenital or reproductive",
        "largeintestine" : "large intestine",
        "digestivesystem" : "digestive system",
        "ironoxidizer" : "iron oxidizer",
        "hostassociated" : "host-associated",
        "humanoralcavity" : "oral cavity",
        "urogenitaltract" : "urogenital tract",
        "urogenital_urinary" : "urogenital or urinary",
        "oralcavity" : "oral cavity",
        "mammalsman" : "human",
        "humanfecal" : "fecal",
        "mammalspig" : "pig",
        #"storespolyhydroxybutyrate" : "",
        #"hydrothermalvent" : "",
        #"aquatic_salinewater" : "",
        #"birdsintestinalmicroflora" : "",
        #"insectstermites" : "",
        #"nitrogenproducer" : "",
        #"cerebrospinalfluid" : "",
        #"ammoniaoxidizer" : "",
        #"sulfurrespiration" : "",
        #"skin_softtissues" : "",
        #"gingivalcrevices" : "",
        #"oilfields" : "",
        #"mammalscarnivora" : "",
        #"pahdegrading" : "",
        #"softtissues" : "",
        #"bovinerumen" : "",
        #"hydrogensulfidegasrelease" : "",
        #"hydrothermalvents" : "",
        #"cardiovascular_blood" : "",
        #"industrialwastewater" : "",
        #"nonmarinesalineandalkaline" : "",
        #"mammalsherbivores" : "",
        #"agriculturalfield" : "",
        #"plantroot" : "",
        #"circulatorysystem" : "",
        #"plantsymbiont" : "",
        #"respiratory_throatnoseeyes" : "",
        #"humanvaginalmicroflora" : "",
        #"dairyproducts" : "",
        #"humanairways" : "",
        #"nervous_system" : "",
        #"nitrogenfixation" : "",
        #"cardiovascular_heart_bloodvessels" : "",
        #"saltcrystallizerponds" : "",
        #"marineinvertebrates" : "",
        #"insectendosymbiont" : "",
        #"intertidalzone" : "",
        #"oportunisticnosocomial" : "",
        #"surfacewater" : "",
        #"respiratorysystem" : "",
        #"sulfuroxidizer" : "",
        #"respiratory_lungdisease" : "",
        #"deepsea" : "",
        #"insectsgeneral" : "",
        #"rootnodule" : "",
        #"sulfatereducer" : "",
        #"freeliving" : "",
        #"animalgastrointestinaltract" : "",
        #"oral_cavity" : "",
        #"creosotecontaminatedsoil" : "",
        #"solidwaste" : "",
        #"ethanolproduction" : "",
        #"chemoorganotroph" : "",
        #"thermalsprings" : "",
        #"hotspring" : "",
        #"humanskin" : "",
        }



        # Category types:
        # 1. "name_name" type
        # 2. "(...|...)" type
        # 3. "Category=assignment" type

        # ==============================================================================
        # 1. "name_name" type
        mapp1 = {i:i.replace("_", " ").replace('  ', ' ') for i in
                    vs[ [not(j or k) for j,k in zip(vs.str.contains('='), vs.str.contains('\(')) ] ]
                    }

        # ==============================================================================
        # 2. "(...|...)" type

        # Just leave those as are
        mapp2 = {i:i for i in vs[vs.str.contains('\(')]}


        # ==============================================================================
        # 3. "Category=assignment" type

        #set([i.split('=')[0] for i in vs[vs.str.contains('=')]])

        def assign_type3(s):
            p0 = s.split("=")[0]
            p1 = s.split("=")[1]
            if p0 in category.keys():
                p0 = category[p0]
            if p1 in assignment.keys():
                p1 = assignment[p1]
            return str(p0)+": "+str(p1)

        mapp3 = {i:assign_type3(i) for i in vs[vs.str.contains('=')]}


        # ==============================================================================
        # CATEGORIES
        # ==============================================================================

        # Minor

        p0 = vs[vs.str.contains("=")]
        cats = {k:v for k,v in zip(p0, [category[i.split('=')[0]] for i in p0])}

        p1 = vs[vs.str.contains("\(")]
        cats.update({k:'Metabolism' for k in p1})

        p2 = {
            'Organism_name' : None,
            'Tax_ID' : None,
            'flagellarpresence' : 'Phenotype',
            'growth_in_groups' : 'Phenotype',
            'mobility' : 'Phenotype',
            'motility' : 'Phenotype',
            'pathogenic_in_mammals' : 'Ecosystem',
            'pathogenic_in_plants' : 'Ecosystem',
            'radioresistance' : 'Phenotype',
            'sporulation' : 'Phenotype',
            'pathogenic_in_fish' : 'Ecosystem',
            }
        cats.update(p2)

        p3 = [i for i in vs if i not in set(p0.tolist()+p1.tolist()+list(p2.keys()))]
        cats.update({k:"Metabolism" for k in p3})


        # Major
        major_categories = {'Metabolism', 'Ecosystem', 'Phenotype'}

        major_categories_mapp = {
        'bioticrelationship':"Ecosystem",
        'cellarrangement':"Phenotype",
        'ecosystem':"Ecosystem",
        'ecosystemcategory':"Ecosystem",
        'ecosystemsubtype':"Ecosystem",
        'ecosystemtype':"Ecosystem",
        'energysource':"Metabolism",
        'gram_stain':"Phenotype",
        'habitat':"Ecosystem",
        'host':"Ecosystem",
        'knownhabitats':"Ecosystem",
        'mammalian_pathogen':"Ecosystem",
        'metabolism':"Metabolism",
        'oxygenreq':"Metabolism",
        'phenotypes':"Phenotype",
        'shape':"Phenotype",
        'specificecosystem':"Ecosystem",
        'temperaturerange':"Ecosystem",
        }

        mcats = {k:v for k,v in cats.items()}
        for k,v in mcats.items():
            if k.find('=') > -1:
                mcats[k] = major_categories_mapp[k.split('=')[0]]


        # ==============================================================================
        # INTEGRATE
        # ==============================================================================

        mapp = mapp1
        mapp.update(mapp2)
        mapp.update(mapp3)

        mapp = {k:[v, cats.get(k), mcats.get(k)] for k,v in mapp.items()}

        '''
        self._all_variables = vs
        self._categories = categories
        self._human_readable_names = human_readable_names
        self._major_categories = major_categories
        '''
        self._mappings = mapp
        self.URL = pt_url
        self.original_data = original_data

    def process_data(self):
        #Warning: circular import
        import mnemonic.data.ProTraits
        p = mnemonic.data.ProTraits.table()
        self.processed_data = p
        return p

    def ls_variables(self):
        """
        List all variables in the AGP dataset.
        """
        #return vs
        return self.get_table()['variable_name'].drop_duplicates().tolist()

    def ls_categories(self):
        """
        List all available categories.
        """
        return self.get_table()['detailed_category'].value_counts().index.tolist()

    def ls_major_categories(self):
        """
        List the major categories.
        """
        return self.get_table()['category'].value_counts().index.tolist()

    def nicename(self, var_name):
        """
        Get the edited, human-readable variable name.
        """
        #if not var_name in self.ls_variables():
        #    raise KeyError("Variable name not found.")
        tb = self.get_table()
        return tb[tb['variable_name'] == var_name]['edited_name'].iloc[0]

    '''
    def ls_variables_with_categories(self):
        """
        List all variables that have a category annotation.
        """
        t = self.get_table()
        return t[['variable_name','category']].dropna()['variable_name'].tolist()

    def category_members(self, cat_name):
        """
        List all members for a given category name.
        """
        if not cat_name in self.ls_categories():
            raise KeyError("Category name not found.")
        return self._categories[cat_name]


    def categories_for(self, var_name):
        """
        List all categories that the variable is a member of.
        """
        if not var_name in vs:
            raise KeyError("Variable name not found.")
        else:
            res = []
            for c in self.ls_categories():
                if var_name in self.category_members(c):
                    res.append(c)
            return res

    def main_category_for(self, var_name):
        """
        Get the main category that the variable belongs to.
        """
        if not var_name in vs:
            raise KeyError("Variable name not found.")
        else:
            for c in self.ls_main_categories():
                if var_name in self.category_members(c):
                    return c
    '''


    def get_table(self):
        """
        Returns a table of all the data stored in the object.
        """

        table = pd.DataFrame(self._mappings).T.reset_index()
        table.columns = ['variable_name', 'edited_name', 'detailed_category', 'category']

        return table

    def plot_var_counts(self, major=True):
        import matplotlib.pyplot as plt

        def make_autopct(values):
            def my_autopct(pct):
                total = sum(values)
                val = int(round(pct*total/100.0))
                return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
            return my_autopct

        plt.clf()
        tb = self.get_table()
        if major:
            x = tb.category.value_counts()
        else:
            x = tb.detailed_category.value_counts()
        x.plot.pie(figsize=(6,6), autopct=make_autopct(x.values),
                title = 'ProTraits categories and counts per category')
        #plt.savefig('PT_detailed_category_pie.png', transparent=True)

    def plot_single_var(self, var_name):
        import matplotlib.pyplot as plt
        import seaborn as sns

        if not hasattr(self, 'processed_data'):
            self.process_data()

        md = self.processed_data

        #md.columns = md.columns.get_level_values('Variable')

        if md[var_name].dtypes == np.float64 or md[var_name].dtypes == 'object':
            n_bins = 10
            s = md[var_name]

            if n_bins < len(s.value_counts()):
                s = pd.cut(s, n_bins)
            s = s.value_counts().sort_index()

            plt.clf()
            sns.set_style('whitegrid')


            fontsize=10
            pos = [i for i in range(len(s))]
            bars = plt.bar(pos, s.values, width=0.98)# facecolor='black', edgecolor='black')
            for bar in bars:
                plt.gca().text(
                            bar.get_x()+bar.get_width()/2,
                            max(s)/100,
                            #max(s),
                            str(int(bar.get_height())),
                            va='bottom', ha='center', color='k', fontsize=fontsize,
                        )

            #labels
            if set(s.index) == {0,1}:
                s.rename(index = {0:"false", 1:"true"}, inplace=True)

            plt.xticks(pos, s.index, rotation='horizontal', fontsize=fontsize)

if __name__ == "__main__":
    PTVarCats().get_table().to_csv("ProTraits_variables.tsv", sep='\t', index=None)
