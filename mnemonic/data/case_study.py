import collections

import pandas as pd
import numpy as np
import scipy.stats

import mnemonic.util
from mnemonic.taxonomy import TaxonomyLevel, Taxonomy
from mnemonic.contrast import ContrastSet
import mnemonic.data.case_study_data

from mnemonic.data.case_study_data import download_cs1
download_cs1()


def ERP002469_clinical():
    def fn(k):
        k = k.strip()
        fields = k.split(" ")
        if (len(fields) == 2):
            try:
                int(fields[1])
                k = "-".join(fields)
            except:
                pass
        return k

    items = {"data_Spearman": "rho", "pFDR": "FDR"}
    for k,v in items.items():
        path = "data/ERP002469/ERP002469_correlation_with_clinical_{}.tsv".format(k)
        df = pd.read_csv(path, index_col=0, sep="\t")
        df.columns = list(map(fn, df.columns))
        M = dict(zip(df.index, [" ".join(ix.split(" ")[:2]) for ix in df.index]))
        if v == "rho":
            rho = df.groupby(M).mean()
        else:
            p = df.groupby(M).min()
    o = {}
    for c in rho.columns:
        o[c] = pd.DataFrame({"rho": rho[c], "p": p[c]})
    return ContrastSet(o, level=TaxonomyLevel.SPECIES, default_parameter="rho")

def ERP002469():
    items = {"metformin_vs_none": "metformin", "NGT_vs_T2D": "T2D"}
    o = {}
    for k,v in items.items():
        path = "data/ERP002469/ERP002469_DA_{}.tsv".format(k)
        df = pd.read_csv(path, index_col=0, sep="\t")
        df["p"] = df["P.adj."]
        ix = [c for c in df.columns if "log10 FC" in c][0]
        df["logFC"] = df[ix]
        df["SLPV"] = - df["p"].apply(np.log10) * df["logFC"].apply(np.sign)
        df = df.loc[:,["logFC", "p", "SLPV"]]
        M = dict(zip(df.index, [" ".join(ix.split(" ")[:2]) for ix in df.index]))
        o[v] = df.groupby(M).apply(lambda xs: xs.sort_values("p").iloc[0,:])
    return ContrastSet(o, level=TaxonomyLevel.SPECIES)

def SRP022224():
    url = "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957428/bin/NIHMS536070-supplement-3.xlsx"
    path = mnemonic.util.download(url)
    df = pd.read_excel(path, sheet_name='Table S22', skiprows=1)[:31]\
            .set_index(["Genus & species"])\
            .loc[:,["Plant-based diet", "Animal-based diet", "p-value"]]
    df.columns = ["PlantDiet", "AnimalDiet", "p"]
    df["logFC"] = np.log2(df.AnimalDiet / (df.PlantDiet + df.PlantDiet.min() * 0.01))
    df["SLPV"] = -1 * df.p.apply(np.log10) * df.SLPV.apply(np.sign)
    return df

def most_extreme_value(df):
    def fn(x):
        x = np.array(x)
        ix = np.argsort(np.abs(x))
        mask = ~np.isnan(x)
        for i in reversed(range(len(x))):
            if mask[i]:
                return x[ix[i]]
        return np.nan
    o = []
    for i in range(df.shape[0]):
        o.append(fn(df.iloc[i,:]))
    o = pd.Series(o, index=df.index)
    o.name = df.name
    return o

def Duvallet(collapse=False):
    '''
    CASE STUDY 4:
    A metaanalysis: couple of papers, couple of diseaes: Obesity, IBD, T1D (sadly no T2D)
    '''
    # source of the data:
    url = "https://raw.githubusercontent.com/cduvallet/microbiomeHD/master/final/supp-files/file-S1.qvalues.txt"
    path = mnemonic.util.download(url)

    # q-values across different studies (column names are disease_author)
    # ART arthritis, ASD autism spectrum disorder, CD Crohn’s disease, CDI Clostridium difficile infection, CIRR liver cirrhosis, CRC colorectal cancer, EDD enteric diarrheal disease, H healthy, HIV human immunodeficiency virus, LIV liver diseases, MHE minimal hepatic encephalopathy, NASH non-alcoholic steatohepatitis, OB obesity, PAR Parkinson’s disease, PSA psoriatic arthritis, RA rheumatoid arthritis, T1D type I diabetes, UC ulcerative colitis
    df = pd.read_csv(path, sep='\t')
    df.rename(columns={'Unnamed: 0': 'taxon'}, inplace=True)
    df = df.set_index(["taxon"])
    df = Taxonomy.reindex(df.T, reducer=lambda df: df.iloc[0,:], level=TaxonomyLevel.GENUS).T

    counts = collections.defaultdict(int)
    ix = []
    for c in df.columns:
        c = c.split("_")[0].upper()
        ix.append("{}{}".format(c, counts[c] + 1))
        counts[c] += 1
    df.columns = ix

    if collapse is True:
        groups = [c[:-1] for c in df.columns]
        df = df.groupby(groups, axis=1).apply(most_extreme_value)

    def fn(c):
        oo = df[c].to_frame()
        oo.columns = ["p"]
        # actually Z-score
        p = oo.p
        oo["SLPV"] = - np.log10(p.abs()) * np.sign(p)
        oo.SLPV = oo.SLPV.fillna(0)
        oo.p = oo.p.abs()
        oo.name = c
        return oo
    o = {k:fn(k) for k in df.columns}

    return ContrastSet(o, level=TaxonomyLevel.GENUS)
