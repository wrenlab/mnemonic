"""
.. module: AGP_variables

AGP_variables
--------------------------------------------------------------------------------

Functionality to fetch and browse the variables (terms) in the American Gut Project (http://humanfoodproject.com/americangut/).

"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import mnemonic.util

# ==============================================================================
# Variables
# ==============================================================================

def palette():
    pal = {
        'red'   : sns.color_palette('muted')[2],
        'black' : sns.light_palette('black')[-2],
        'gray'  : sns.light_palette('black')[-4],
        'blue'  : sns.color_palette('muted')[0],
        }
    return pal


# VioScreen variable descriptions (let me know if needed in the metadata):
# ftp://ftp.microbio.me/AmericanGut/manuscript-package/vioscreen_data_dictionary.pdf

# ==============================================================================
# Ontology

class AGPVarCats():
    """
    Stores the American Gut Project variable category information
    as well as edited variable names.
    """
    def __init__(self):
        url = "https://www.dl.dropboxusercontent.com/s/e0x1luu79tldgti/metadata_American_Gut_cleaned.txt"
        #agp_metadata_fp = "data/metadata_American_Gut_cleaned.txt"
        agp_metadata_fp = mnemonic.util.download(url)

        # ==============================================================================
        # Variable categories

        major_categories = {'VioScreen', 'food', 'allergy', 'physical disease', 'mental disease', 'demographic'}
        categories = {
            "food" : {'ARTIFICIAL_SWEETENERS','SALTED_SNACKS_FREQUENCY','FERMENTED_PLANT_FREQUENCY','SUGARY_SWEETS_FREQUENCY',
            'PREPARED_MEALS_FREQUENCY', 'ONE_LITER_OF_WATER_A_DAY_FREQUENCY', 'POULTRY_FREQUENCY',
            'HOMECOOKED_MEALS_FREQUENCY','FROZEN_DESSERT_FREQUENCY','VITAMIN_B_SUPPLEMENT_FREQUENCY',
            'VITAMIN_D_SUPPLEMENT_FREQUENCY','READY_TO_EAT_MEALS_FREQUENCY','MILK_SUBSTITUTE_FREQUENCY',
            'LOWGRAIN_DIET_TYPE','OLIVE_OIL','FRUIT_FREQUENCY','SUGAR_SWEETENED_DRINK_FREQUENCY',
            'WHOLE_GRAIN_FREQUENCY','WHOLE_EGGS','CONSUME_ANIMAL_PRODUCTS_ABX',
            'SEAFOOD_FREQUENCY','MILK_CHEESE_FREQUENCY', 'HIGH_FAT_RED_MEAT_FREQUENCY','RED_MEAT_FREQUENCY',
            'MEAT_EGGS_FREQUENCY', 'VEGETABLE_FREQUENCY', 'PROBIOTIC_FREQUENCY',
            'OTHER_SUPPLEMENT_FREQUENCY',
            'LACTOSE', 'TYPES_OF_PLANTS', 'GLUTEN', 'DRINKING_WATER_SOURCE', 'BREASTMILK_FORMULA_ENSURE',
            'MULTIVITAMIN', 'DIET_TYPE',

            'ALCOHOL_TYPES_SPIRITSHARD_ALCOHOL','ALCOHOL_FREQUENCY','ALCOHOL_TYPES_SOUR_BEERS',
            'ALCOHOL_TYPES_RED_WINE', 'ALCOHOL_TYPES_WHITE_WINE','ALCOHOL_TYPES_BEERCIDER', 'ALCOHOL_CONSUMPTION',
            'ALCOHOL_TYPES_WHITE_WINE',
            'ALCOHOL_CONSUMPTION',
            'ALCOHOL_TYPES_BEERCIDER',
            'ALCOHOL_TYPES_SOUR_BEERS',
            'ALCOHOL_FREQUENCY',
            'ALCOHOL_TYPES_SPIRITSHARD_ALCOHOL',
            'ALCOHOL_TYPES_UNSPECIFIED',
            },

            "salt" : {
            'SALTED_SNACKS_FREQUENCY',
            },

            "sugar" : {
            'SUGARY_SWEETS_FREQUENCY',
            'FROZEN_DESSERT_FREQUENCY',
            'SUGAR_SWEETENED_DRINK_FREQUENCY',
            },

            "plant product" : {
            'FERMENTED_PLANT_FREQUENCY',
            'VEGETABLE_FREQUENCY',
            'WHOLE_GRAIN_FREQUENCY',
            'LOWGRAIN_DIET_TYPE',
            'OLIVE_OIL',
            'FRUIT_FREQUENCY',
            },

            "fat" : {
            'OLIVE_OIL',
            'HIGH_FAT_RED_MEAT_FREQUENCY',
            },


            "animal product" : {
            'SEAFOOD_FREQUENCY',
            'HIGH_FAT_RED_MEAT_FREQUENCY',
            'RED_MEAT_FREQUENCY',
            'POULTRY_FREQUENCY',
            'CONSUME_ANIMAL_PRODUCTS_ABX',
            'MEAT_EGGS_FREQUENCY',
            'MILK_CHEESE_FREQUENCY',
            'WHOLE_EGGS',
            },

            "dairy" : {
            'MEAT_EGGS_FREQUENCY',
            'MILK_CHEESE_FREQUENCY',
            'WHOLE_EGGS',
            },

            "meat" : {
            'MEAT_EGGS_FREQUENCY',
            'HIGH_FAT_RED_MEAT_FREQUENCY',
            'RED_MEAT_FREQUENCY',
            'POULTRY_FREQUENCY',
            },

            "supplements" : {
            'VITAMIN_D_SUPPLEMENT_FREQUENCY',
            'PROBIOTIC_FREQUENCY',
            'VITAMIN_B_SUPPLEMENT_FREQUENCY',
            'OTHER_SUPPLEMENT_FREQUENCY',
            'MULTIVITAMIN',
            },

            "alcohol" : {
            'ALCOHOL_TYPES_SPIRITSHARD_ALCOHOL','ALCOHOL_FREQUENCY','ALCOHOL_TYPES_SOUR_BEERS',
            'ALCOHOL_TYPES_RED_WINE', 'ALCOHOL_TYPES_WHITE_WINE','ALCOHOL_TYPES_BEERCIDER', 'ALCOHOL_CONSUMPTION',
            'ALCOHOL_TYPES_WHITE_WINE',
            'ALCOHOL_CONSUMPTION',
            'ALCOHOL_TYPES_BEERCIDER',
            'ALCOHOL_TYPES_SOUR_BEERS',
            'ALCOHOL_FREQUENCY',
            'ALCOHOL_TYPES_SPIRITSHARD_ALCOHOL',
            'ALCOHOL_TYPES_UNSPECIFIED',
            },


            "allergy" : {'NON_FOOD_ALLERGIES_BEESTINGS','ALLERGIC_TO_UNSPECIFIED','ALLERGIC_TO_I_HAVE_NO_FOOD_ALLERGIES_THAT_I_KNOW_OF',
            'ALLERGIC_TO_OTHER','NON_FOOD_ALLERGIES_SUN','NON_FOOD_ALLERGIES_UNSPECIFIED','ALLERGIC_TO_TREE_NUTS','ALLERGIC_TO_PEANUTS',
            'NON_FOOD_ALLERGIES_POISON_IVYOAK','ALLERGIC_TO_SHELLFISH','NON_FOOD_ALLERGIES_PET_DANDER','SEASONAL_ALLERGIES',
            'NON_FOOD_ALLERGIES_DRUG_EG_PENICILLIN',
            },

            "disease" : {
            'SUBSET_HEALTHY',
            'LIVER_DISEASE','SIBO','ADD_ADHD','IBS','FUNGAL_OVERGROWTH','CLINICAL_CONDITION',
            'MIGRAINE','IBD','DIABETES','CHICKENPOX','LUNG_DISEASE','SKIN_CONDITION','PKU',
            'CANCER','ASD','MENTAL_ILLNESS','MENTAL_ILLNESS_TYPE_BULIMIA_NERVOSA',
            'CDIFF', 'EPILEPSY_OR_SEIZURE_DISORDER',
            'MENTAL_ILLNESS_TYPE_BIPOLAR_DISORDER','CARDIOVASCULAR_DISEASE','MENTAL_ILLNESS_TYPE_DEPRESSION','MENTAL_ILLNESS_TYPE_PTSD_POSTTRAUMATIC_STRESS_DISORDER',
            'KIDNEY_DISEASE', 'MENTAL_ILLNESS_TYPE_SCHIZOPHRENIA',
            'ACID_REFLUX','ALZHEIMERS', 'DIABETES_TYPE',
            'MENTAL_ILLNESS_TYPE_ANOREXIA_NERVOSA','AUTOIMMUNE','MENTAL_ILLNESS_TYPE_UNSPECIFIED',
            'DEPRESSION_BIPOLAR_SCHIZOPHRENIA','MENTAL_ILLNESS_TYPE_SUBSTANCE_ABUSE',
            'THYROID',
            },

            "GI disease" : {
            'SIBO',
            'IBS',
            'FUNGAL_OVERGROWTH',
            'IBD',
            'DIABETES',
            'DIABETES_TYPE',
            'CDIFF',
            'ACID_REFLUX',
            },

            "other disease" : {
            'LIVER_DISEASE',
            'CANCER',
            'SKIN_CONDITION',
            'PKU',
            'CARDIOVASCULAR_DISEASE',
            'KIDNEY_DISEASE',
            'AUTOIMMUNE',
            'CHICKENPOX',
            'LUNG_DISEASE',
            'THYROID',
            },

            "mental disease" : {
            'ALZHEIMERS',
            'MIGRAINE',
            'EPILEPSY_OR_SEIZURE_DISORDER',
            'ADD_ADHD',
            'ASD',
            'DEPRESSION_BIPOLAR_SCHIZOPHRENIA',
            'MENTAL_ILLNESS',
            'MENTAL_ILLNESS_TYPE_BULIMIA_NERVOSA',
            'MENTAL_ILLNESS_TYPE_ANOREXIA_NERVOSA',
            'MENTAL_ILLNESS_TYPE_UNSPECIFIED',
            'MENTAL_ILLNESS_TYPE_SCHIZOPHRENIA',
            'MENTAL_ILLNESS_TYPE_DEPRESSION',
            'MENTAL_ILLNESS_TYPE_BIPOLAR_DISORDER',
            'MENTAL_ILLNESS_TYPE_PTSD_POSTTRAUMATIC_STRESS_DISORDER',
            'MENTAL_ILLNESS_TYPE_SUBSTANCE_ABUSE',
            },

            "demographic" : {'AGE_YEARS','AGE_CORRECTED',
            'SUBSET_HEALTHY',
            'COUNTRY_RESIDENCE',
            'ALTITUDE',
            'DEPTH', 'LONGITUDE', 'ELEVATION',
            'BIRTH_YEAR', 'LATITUDE',
            'COLLECTION_DATE',
            'LAST_MOVE',
            'STATE',
            'LAST_TRAVEL',
            'CSECTION',
            'LEVEL_OF_EDUCATION',
            'PETS_OTHER_FREETEXT',
            'SEX',
            'COLLECTION_TIME',
            'COLLECTION_MONTH',
            'COLLECTION_SEASON',
            'FED_AS_INFANT',
            'FLU_VACCINE_DATE',

            'SUBSET_ANTIBIOTIC_HISTORY','SMOKING_FREQUENCY',
            'ROOMMATES',
            'POOL_FREQUENCY',
            'CONTRACEPTIVE',
            'APPENDIX_REMOVED',
            'PETS_OTHER','CANCER_TREATMENT','VIVID_DREAMS','WEIGHT_KG',
            'BMI_CORRECTED',
            'BOWEL_MOVEMENT_FREQUENCY',
            'EXERCISE_FREQUENCY','DOG','NAIL_BITER','CAT', 'PREGNANT','BMI','TEETHBRUSHING_FREQUENCY','LIVINGWITH',
            'FLOSSING_FREQUENCY',
            'WEIGHT_CHANGE',
            'ANTIBIOTIC_HISTORY',
            'BOWEL_MOVEMENT_QUALITY',
            'EXERCISE_LOCATION',
            'SLEEP_DURATION',
            'DEODORANT_USE',
            },
        #BMI_CAT
        #AGE_CAT


            "hygiene" :{
            'SUBSET_ANTIBIOTIC_HISTORY','SMOKING_FREQUENCY',
            'ROOMMATES',
            'POOL_FREQUENCY',
            'CONTRACEPTIVE',
            'APPENDIX_REMOVED',
            'PETS_OTHER','CANCER_TREATMENT','VIVID_DREAMS','WEIGHT_KG',
            'BMI_CORRECTED',
            'BOWEL_MOVEMENT_FREQUENCY',
            'EXERCISE_FREQUENCY','DOG','NAIL_BITER','CAT', 'PREGNANT','BMI','TEETHBRUSHING_FREQUENCY','LIVINGWITH',
            'FLOSSING_FREQUENCY',
            'WEIGHT_CHANGE',
            'ANTIBIOTIC_HISTORY',
            'BOWEL_MOVEMENT_QUALITY',
            'EXERCISE_LOCATION',
            'SLEEP_DURATION',
            'DEODORANT_USE',
            },

            "remove" : {'LinkerPrimerSequence', 'ENA-BASE-COUNT', 'SUBSET_BMI', 'HOST_TAXID', 'HEIGHT_CM',
            'COSMETICS_FREQUENCY', 'ASSIGNED_FROM_GEO', 'NON_FOOD_ALLERGIES_SUN', 'DNA_EXTRACTED',
            'BarcodeSequence', 'SUBSET_AGE', 'WEIGHT_KG', 'ENA-SPOT-COUNT', 'CSECTION',
            'ACNE_MEDICATION','SOFTENER', 'PUBLIC',
            'POOL_FREQUENCY', "ACNE_MEDICATION_OTC"
            },
            }

        original_data = pd.read_csv(agp_metadata_fp, sep='\t')
        vs = original_data.columns.tolist()

        categories.update({
            "physical disease": categories['disease'].difference(categories['mental disease']),
            "VioScreen": set([i for i in vs if i.find('VIOSCREEN') > -1]),
            })

        # Not sure what those mean:
        # check=[ 'LACTOSE',] 'CONSUME_ANIMAL_PRODUCTS_ABX',


        # ==============================================================================
        # Human-readable variable names


        human_readable_names = {k:v for k,v in zip(vs,
                [i.replace('_FREQUENCY', '').replace("_SUBSET", '')\
                .replace("_", ' ').capitalize()\
                .replace("Vioscreen", "VioScreen")\
                .replace('Allergic to', 'Allergy:')\
                .replace('Non food allergies', 'Non-food allergy:')\
                .replace("Alcohol types", "Alcohol:")
                for i in vs]
                )}

        human_readable_names.update({
            'IBD':"Inflammatory bowel disease",
            'ADD_ADHD':"Attention-deficit (hyperactivity) disorder",
            'AUTOIMMUNE':"Autoimmune disease",
            'CDIFF':"C. difficile infection",
            'SKIN_CONDITION':"Skin disease",
            'SIBO':"Small intestinal bacterial overgrowth",
            'PKU':"Phenylketonuria",
            'THYROID':"Thyroid disease", #??
            'IBS':"Irritable bowel syndrome",
            'ALZHEIMERS':"Alzheimer's",
            'EPILEPSY_OR_SEIZURE_DISORDER':"Epilepsy/seisure disorder",
            'ASD':"Autism spectrum disorder",
            'MENTAL_ILLNESS_TYPE_BULIMIA_NERVOSA': "Bulimia nervosa",
            'MENTAL_ILLNESS_TYPE_BIPOLAR_DISORDER': "Bipolar disorder",
            'MENTAL_ILLNESS_TYPE_DEPRESSION': "Depression",
            'MENTAL_ILLNESS_TYPE_PTSD_POSTTRAUMATIC_STRESS_DISORDER': "Posttraumatic stress disorder",
            'MENTAL_ILLNESS_TYPE_SCHIZOPHRENIA': "Schizophrenia",
            'MENTAL_ILLNESS_TYPE_ANOREXIA_NERVOSA': "Anorexia nervosa",
            'MENTAL_ILLNESS_TYPE_UNSPECIFIED': "Unspecified mental illness",
            'DEPRESSION_BIPOLAR_SCHIZOPHRENIA': "Depression or Bipolar disorder or Schizophrenia",
            'MENTAL_ILLNESS_TYPE_SUBSTANCE_ABUSE':"Substance abuse",
            'VITAMIN_B_SUPPLEMENT_FREQUENCY': "Vitamin B supplement",
            'VITAMIN_D_SUPPLEMENT_FREQUENCY': "Vitamin D supplement",
            'SUGAR_SWEETENED_DRINK_FREQUENCY': "Sugar-sweetened drink",
            #'CONSUME_ANIMAL_PRODUCTS_ABX' ??
            'MILK_CHEESE_FREQUENCY': "Milk or cheese",
            'HIGH_FAT_RED_MEAT_FREQUENCY': "High-fat red meat",
            'MEAT_EGGS_FREQUENCY': "Meat or eggs",
            'ALLERGIC_TO_I_HAVE_NO_FOOD_ALLERGIES_THAT_I_KNOW_OF': 'No food allergies',
            'NON_FOOD_ALLERGIES_POISON_IVYOAK' : 'Non-food allergy: ivyoak',
            'NON_FOOD_ALLERGIES_DRUG_EG_PENICILLIN': 'Non-food allergy: drug (e.g. penicillin)',
            'AGE_YEARS': "Age (years)",
            'AGE_CORRECTED': "Age (corrected)",
            'BMI': "Body mass index",
            'BMI_CORRECTED': "Body mass index (corrected)",
            'LIVINGWITH': "Living with",
            'PETS_OTHER': "Other pets",
            'WEIGHT_KG': "Weight (KG)",
            'COUNTRY_RESIDENCE': "Country of residence",
            'APPENDIX_REMOVED': "Removed appendix",
            }
            )

        self._filepath = agp_metadata_fp
        self._all_variables = vs
        self._categories = categories
        self._human_readable_names = human_readable_names
        self._major_categories = major_categories

        self.original_data = original_data

    def process_data(self):
        #Warning: circular import
        import mnemonic.data.AGP
        p = mnemonic.data.AGP.metadata()
        self.processed_data = p
        return p

    def ls_variables(self, data='processed'):
        """
        List all variables in the AGP dataset.
        data : "processed" or "raw"
        """
        if data == 'raw':
            return set(self._all_variables)
        elif data == 'processed':
            if not hasattr(self, 'processed_data'):
                self.process_data()
            return set(self.processed_data.columns.get_level_values('Variable'))

    def ls_variables_with_categories(self):
        """
        List all variables that have a category annotation.
        """
        t = self.get_table()
        return t[['variable_name','category']].dropna()['variable_name'].tolist()

    def ls_categories(self):
        """
        List all available categories.
        """
        return set(self._categories.keys())

    def ls_major_categories(self):
        """
        List the major categories.
        """
        return self._major_categories

    def category_members(self, cat_name):
        """
        List all members for a given category name.
        """
        if not cat_name in self.ls_categories():
            raise KeyError("Category name not found.")
        return self._categories[cat_name]

    def nicename(self, var_name):
        """
        Get the edited, human-readable variable name.
        """
        if not var_name in self._all_variables:
            raise KeyError("Variable name not found.")
        return self._human_readable_names[var_name]

    def categories_for(self, var_name):
        """
        List all categories that the variable is a member of.
        """
        if not var_name in self._all_variables:
            raise KeyError("Variable name not found.")
        else:
            res = []
            for c in self.ls_categories():
                if var_name in self.category_members(c):
                    res.append(c)
            return res

    def major_category_for(self, var_name):
        """
        Get the major category that the variable belongs to.
        """
        if not var_name in self._all_variables:
            raise KeyError("Variable name not found.")
        else:
            for c in self.ls_major_categories():
                if var_name in self.category_members(c):
                    return c


    def get_table(self):
        """
        Returns a table of all the data stored in the object.
        """
        res = []

        for v in self.ls_variables('raw'):
            res.append(pd.Series(
                [v, "|".join(self.categories_for(v)), self.major_category_for(v), self.nicename(v)]
                ))

        table = pd.concat(res, axis=1).T.rename(
            columns={0:'variable_name', 1:'all_categories', 2:'category', 3:'edited_name'})

        return table

    def plot_var_counts(self, major=True, vioscreen=True):
        import matplotlib.pyplot as plt

        def make_autopct(values):
            def my_autopct(pct):
                total = sum(values)
                val = int(round(pct*total/100.0))
                #return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
                return '{v:d}'.format(p=pct,v=val)
            return my_autopct

        plt.clf()
        tb = self.get_table()
        #plt.figure(figsize=(3,3))

        if major:
            x = tb.category.value_counts()
            if not vioscreen:
                x.drop("VioScreen", inplace=True, axis=0)
        else:
            names = []
            cats = []

            for r in tb.iterrows():
                for i in r[1].all_categories.split('|'):
                    names.append(r[1].variable_name)
                    cats.append(i)

            x = pd.Series(cats, index=names).value_counts()

            for major_cat in self.ls_major_categories().union({''}):
                x.drop(major_cat, inplace=True, axis=0)


        x.name = ''
        x.plot.pie(figsize=(6,6), autopct=make_autopct(x.values),
                title = 'AGP categories and counts per category')
        #plt.savefig('AGP_category_pie.png', transparent=True)

    def plot_single_var(self, var_name):
        n_bins = 7

    #bowel_movement_frequency : {
    #        "Less than one": 0,
    #        "One": 1,
    #        "Two": 2,
    #        "Three": 3,
    #        "Four": 4,
    #        "Five or more": 5,
    #    },
        frequencies = {
            "Daily": 1,
            "Regularly": 4/7,
            "Occasionally": 1.5/7,
            "Rarely": 1/14,
            "Never": 0
        }
        frequencies = {round(v,2):k for k,v in frequencies.items()}

        antibiotic_history = {
                "I have not taken antibiotics in the past year.": 0,
                "Year": 1,
                "6 months": 2,
                "Month": 3,
                "Week": 4,
        }
        antibiotic_history = {v:k for k,v in antibiotic_history.items()}

        plt.clf()
        sns.set_style('whitegrid')
        if not hasattr(self, 'processed_data'):
            self.process_data()

        md = self.processed_data


        md.columns = md.columns.get_level_values('Variable')

        if md[var_name].dtypes == np.float64 or md[var_name].dtypes == 'object':
            s = md[var_name].dropna()
            s = s.round(2)
            #return s

            #if md[var_name].dtypes == np.float64:

            if n_bins < len(s.value_counts()):
                s = pd.cut(s, n_bins)
                xlab = 'Bins'
            else:
                xlab = "Values"
            s = s.value_counts().sort_index()

            fontsize=10
            pos = [i for i in range(len(s))]
            bars = plt.bar(pos, s.values, width=0.98, facecolor=palette()['gray'])#, edgecolor='black')
            for bar in bars:
                plt.gca().text(
                            bar.get_x()+bar.get_width()/2,
                            max(s)/100,
                            #max(s),
                            str(int(bar.get_height())),
                            va='bottom', ha='center', color='k', fontsize=fontsize,
                        )

            #labels
            if set(s.index) == {0,1}:
                s.rename(index = {0:"false", 1:"true"}, inplace=True)

            #if all([i in {0, 1/14, 1.5/7, 4/7, 1} for i in set(s.index)]):
            if 0.07 in set(s.index):
                s.rename(index = frequencies, inplace=True)

            if set(s.index) == {0, 1, 2, 3, 4}:
                s.rename(index = antibiotic_history, inplace=True)

            plt.xticks(pos, s.index, rotation='vertical', fontsize=fontsize)
            plt.title("Distribution of " + self.nicename(var_name))
            plt.ylabel('Number of samples')
            plt.xlabel(xlab)

            #FIXME
            #plt.tight_layout()
            plt.subplots_adjust(bottom=0.4)
            #plt.subplots_adjust(left=  0.2)
            #plt.subplots_adjust(top=   1)
            #plt.subplots_adjust(right= 1)


if __name__ == "__main__":
    A = AGPVarCats().get_table()
    A.to_csv('AGP_variables.tsv', sep='\t', index=False)

