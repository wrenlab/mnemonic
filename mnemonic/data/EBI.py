"""
.. module: EBI

EBI
--------------------------------------------------------------------------------

European Bioinformatics Institute metagenomics data.

"""
import pandas as pd

from mnemonic.util import memoize, download

def metadata():
    '''
    Get the EBI data in a table format.
    '''
    url = "https://dl.dropboxusercontent.com/s/22yveu1odg3r5hk/metadata_EBI.tsv"
    path = download(url)
    o = pd.read_csv(path, sep="\t")
    o = o.groupby("sample_id").first()
    o.index.name = "SampleID"
    o = o.loc[:,["project_id", "project_name"]]
    o.columns = ["ProjectID", "ProjectName"]
    return o

@memoize
def counts():
    url = "https://www.dl.dropboxusercontent.com/s/l83pp5744yjludl/counts_OTUs.tsv"
    path = download(url)
    o = pd.read_csv(path, index_col=0, sep="\t").astype(int)
    o.index.name = "SampleID"
    return o
