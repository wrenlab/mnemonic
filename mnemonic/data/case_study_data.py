import os
import io
import pandas as pd
from mnemonic.util import memoize, LOG
import mnemonic.util


cfg = mnemonic.util.get_configuration()
data_root = os.path.join(cfg["data"]["root"], "download")
download_dir = os.path.join(os.path.expanduser(data_root))
os.makedirs(download_dir, exist_ok=True)

data_dir = 'data'
os.makedirs(data_dir, exist_ok=True)

def download_all():

    download_cs1()
    download_cs2()
    download_cs3()
    download_cs4()

# ==============================================================================
# AGING
# The comparison of gut microbiota obtained from centenarian, elderly,
# and adults living in southwestern longevity belt in Korea
# EBI - ERP007244
# CAVEATS: no DA data

#TODO: get the table
# https://link.springer.com/article/10.1007%2Fs00018-017-2674-yo
# Table 2: Comparison of the changes to GM species composition between Italian, Chinese
# and Japanese centenarians according to different papers


#TODO: get the table
# http://www.jmb.or.kr/journal/view.html?doi=10.4014/jmb.1410.10014
# http://www.jmb.or.kr/submission/Journal/025/JMB025-08-02_FSUP_1.pdf
# Table S4. Phylogeny of the thirty-three key OTUs (97% similarity level) and
# p value of Mann-Whitney test between centenarians (RC) and younger elderly (RE and CE).

# ==============================================================================
# get case study datasets
# ==============================================================================


def download_cs1():
    '''
    CASE STUDY 1:
    Gut metagenome in European women with normal, impaired and diabetic glucose
    control
    EBI: ERP002469
    '''

    # source of the data:
    id1 = "ERP002469"
    cs1_url = "https://media.nature.com/original/nature-assets/nature/journal/v498/n7452/extref/nature12198-s2.xlsx"
    cs1_file = mnemonic.util.download(cs1_url)
    #cs1_file = os.path.join(downloads_dir, cs1_url.split("/")[-1])
    #if os.path.exists(cs1_file):
    #    os.remove(cs1_file)
    #wget.download(cs1_url, cs1_file)

    # List of species differentially abundant in NGT
    # (normal glucose tolerance) and T2D samples
    t11 = pd.read_excel(cs1_file,
            sheet_name= 'Supplementary Table 6', skiprows=1)

    # List of species significantly correlating with clinical
    # data.

    # a, Spearman's rank correlation coefficient
    t12 = pd.read_excel(cs1_file, sheet_name= 'Supplementary Table 7', skiprows=2)[:74]

    # b, P value of correlation adjusted for multiple testing.
    t13 = pd.read_excel(cs1_file, sheet_name= 'Supplementary Table 7', skiprows=79)

    # List of species differentially abundant in T2D samples from women with
    # and without metformin.
    t14 = pd.read_excel(cs1_file, sheet_name= 'Supplementary Table 13', skiprows=1)

    os.makedirs(os.path.join(data_dir, id1), exist_ok=True)
    t11.to_csv(os.path.join(data_dir, id1, id1+"_DA_NGT_vs_T2D.tsv"), sep='\t', index=False)
    t12.to_csv(os.path.join(data_dir, id1, id1+"_correlation_with_clinical_data_Spearman.tsv"), sep='\t', index=False)
    t13.to_csv(os.path.join(data_dir, id1, id1+"_correlation_with_clinical_pFDR.tsv"), sep='\t', index=False)
    t14.to_csv(os.path.join(data_dir, id1, id1+"_DA_metformin_vs_none.tsv"), sep='\t', index=False)
    return t11,t12,t13,t14

def download_cs2():
    '''
    CASE STUDY 2:
    Diet rapidly and reproducibly alters the human gut microbiome
    https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957428/
    GEO - GSE46761, ENA - SRP022224
    '''
    id2 = "SRP022224"

    # source of the data:
    cs2_url = "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3957428/bin/NIHMS536070-supplement-3.xlsx"
    #cs2_file = os.path.join(downloads_dir, cs2_url.split("/")[-1])
    cs2_file = mnemonic.util.download(cs2_url)

    # Taxa with significant transcriptional changes during diet arms: Animal-based
    # diet vs. Plant-based diet
    t21 = pd.read_excel(cs2_file, sheet_name='Table S22', skiprows=1)[:31]

    t21.to_csv(os.path.join(data_dir, id2+"_DA_plantdiet_vs_animaldiet.tsv"), sep='\t', index=False)
    return t21


def download_cs3():
    '''
    CASE STUDY 3:
    Diet, Environments, and Gut Microbiota. A Preliminary Investigation
    in Children Living in Rural and Urban Burkina Faso and Italy
    EBI: ERP021966
    https://www.ebi.ac.uk/metagenomics/projects/ERP021966
    http://europepmc.org/articles/PMC5645538
    '''
    id3 = "ERP021966"

    # source of the data:
    # http://europepmc.org/articles/PMC5645538/bin/Presentation_1.pdf
    # Table 5: DA for phylum, family, genus levels
    # (using family-level becase the table is in pdf and the genus table is screwed)

    #t31 = pd.read_csv(os.path.join(downloads_dir, "case_study3_family.txt"), sep=' ')
    t31 = pd.read_csv(io.StringIO(case_study3_data), sep=' ')

    t31.to_csv(os.path.join(data_dir, id3+"_DA_plantdiet_vs_animaldiet.tsv"), sep='\t', index=False)
    return t31

def download_cs4():
    '''
    CASE STUDY 4:
    A metaanalysis: couple of papers, couple of diseaes: Obesity, IBD, T1D (sadly no T2D)
    '''
    id4 = "Duvallet"

    # source of the data:
    cs4_url = "https://raw.githubusercontent.com/cduvallet/microbiomeHD/master/final/supp-files/file-S1.qvalues.txt"
    cs4_file = mnemonic.util.download(cs4_url)

    # q-values across different studies (column names are disease_author)
    # ART arthritis, ASD autism spectrum disorder, CD Crohn’s disease, CDI Clostridium difficile infection, CIRR liver cirrhosis, CRC colorectal cancer, EDD enteric diarrheal disease, H healthy, HIV human immunodeficiency virus, LIV liver diseases, MHE minimal hepatic encephalopathy, NASH non-alcoholic steatohepatitis, OB obesity, PAR Parkinson’s disease, PSA psoriatic arthritis, RA rheumatoid arthritis, T1D type I diabetes, UC ulcerative colitis
    t41 = pd.read_csv(cs4_file, sep='\t')
    t41.rename(columns={'Unnamed: 0': 'taxon'}, inplace=True)
    t41.to_csv(os.path.join(data_dir, id4+"_metaanalysis_qvalues.tsv"), sep='\t', index=False)
    return t41

# ==============================================================================
# load case study data
# ==============================================================================

def ERP002469():
    id1 = "ERP002469"

    paths = [
        os.path.join(data_dir, id1+"_DA_NGT_vs_T2D.tsv"),
        os.path.join(data_dir, id1+"_correlation_with_clinical_data_Spearman.tsv"),
        os.path.join(data_dir, id1+"_correlation_with_clinical_pFDR.tsv"),
        os.path.join(data_dir, id1+"_DA_metformin_vs_none.tsv"),
        ]

    tables = {
            os.path.split(p)[1]: pd.read_csv(p, sep='\t', index_col=0)
            for p in paths
            }
    return tables

def SRP022224():
    id2 = "SRP022224"
    paths = [
        os.path.join(data_dir, id2+"_DA_plantdiet_vs_animaldiet.tsv")
        ]

    tables = {
            os.path.split(p)[1]: pd.read_csv(p, sep='\t', index_col=0)
            for p in paths
            }
    return tables



def ERP021966():
    id3 = "ERP021966"
    paths = [
        os.path.join(data_dir,id3+"_DA_plantdiet_vs_animaldiet.tsv")
        ]

    tables = {
            os.path.split(p)[1]: pd.read_csv(p, sep='\t', index_col=0)
            for p in paths
            }
    return tables


def Duvallet():
    id4 = "Duvallet"
    paths = [
        os.path.join(data_dir, id4+"_metaanalysis_qvalues.tsv")
        ]

    tables = {
            os.path.split(p)[1]: pd.read_csv(p, sep='\t', index_col=0)
            for p in paths
            }
    return tables

# ==============================================================================
# misc data
# ==============================================================================

case_study3_data = """
"Family" "BR vs BT p" "BR vs BT p.adj (FDR)" "BR vs BC p" "BR vs BC p.adj (FDR)" "BR vs EU p" "BR vs EU p.adj (FDR)" "BT vs BC p" "BT vs BC p.adj (FDR)" "BT vs EU p" "BT vs EU p.adj (FDR)" "BC vs EU p" "BC vs EU p.adj (FDR)"
Bacteroidetes_Prevotellaceae 0.033 0.154 0.002 0.014 0.000 0.000 0.047 0.258 0.001 0.005 0.833 0.952
Firmicutes_Ruminococcaceae 0.026 0.143 0.009 0.021 0.000 0.000 0.622 0.864 0.089 0.173 0.035 0.136
Firmicutes_Lachnospiraceae 0.238 0.490 0.005 0.016 0.018 0.038 0.093 0.284 0.140 0.257 0.173 0.370
Bacteroidetes_Bacteroidaceae 0.117 0.370 0.000 0.005 0.000 0.000 0.019 0.205 0.004 0.016 0.924 0.986
Unknown_Unknown 0.351 0.576 0.955 0.986 0.008 0.018 0.558 0.864 0.089 0.173 0.038 0.136
Firmicutes_Unknown 0.043 0.158 0.052 0.110 0.042 0.074 0.213 0.530 0.612 0.703 0.443 0.626
Actinobacteria_Bifidobacteriaceae 0.038 0.156 0.004 0.015 0.000 0.000 0.060 0.282 0.006 0.019 0.443 0.626
Bacteroidetes_Porphyromonadaceae 0.005 0.055 0.027 0.062 0.000 0.000 0.724 0.885 0.011 0.026 0.059 0.190
Bacteroidetes_Rikenellaceae 0.132 0.370 0.000 0.005 0.000 0.000 0.029 0.239 0.001 0.006 0.016 0.113
Proteobacteria_Enterobacteriaceae 0.614 0.751 0.774 0.854 0.159 0.219 1.000 1.000 0.560 0.703 0.457 0.626
Firmicutes_Unknown.1 0.172 0.437 0.570 0.689 1.000 1.000 0.379 0.736 0.310 0.487 0.489 0.626
Bacteroidetes_Unknown 0.310 0.558 0.001 0.007 0.000 0.000 0.011 0.179 0.000 0.005 0.285 0.480
Proteobacteria_Enterobacteriaceae.1 0.901 0.934 0.063 0.125 0.000 0.000 0.233 0.530 0.002 0.009 0.137 0.312
Firmicutes_Veillonellaceae 0.321 0.558 0.733 0.838 0.353 0.432 0.833 0.982 0.689 0.758 0.961 0.992
Firmicutes_Streptococcaceae 0.934 0.934 0.377 0.575 0.139 0.200 0.557 0.864 0.346 0.519 0.730 0.898
Spirochaetes_Spirochaetaceae 0.588 0.751 0.004 0.015 0.000 0.000 0.089 0.284 0.007 0.019 NA NA
Firmicutes_Clostridiaceae.1 0.383 0.576 0.570 0.689 0.171 0.226 0.941 1.000 0.942 0.942 1.000 1.000
Proteobacteria_Sutterellaceae 0.004 0.055 0.000 0.005 0.000 0.000 0.089 0.284 0.000 0.005 0.004 0.067
Proteobacteria_Unknown 0.185 0.437 0.003 0.015 0.036 0.066 0.095 0.284 0.769 0.818 0.025 0.113
Bacteroidetes_Unknown.1 0.384 0.576 0.005 0.016 0.000 0.000 0.278 0.573 0.007 0.019 0.137 0.312
Proteobacteria_Unknown.1 0.218 0.479 1.000 1.000 0.047 0.078 0.524 0.864 0.618 0.703 0.240 0.452
Actinobacteria_Coriobacteriaceae 0.023 0.143 0.099 0.187 0.839 0.865 0.714 0.885 0.011 0.026 0.012 0.113
Firmicutes_Peptostreptococcaceae 0.934 0.934 0.252 0.424 0.430 0.489 0.137 0.376 0.579 0.703 0.359 0.575
Firmicutes_Erysipelotrichaceae 0.932 0.934 0.004 0.015 0.392 0.462 0.010 0.179 0.442 0.608 0.001 0.037
Proteobacteria_Desulfovibrionaceae 0.001 0.037 0.008 0.021 0.001 0.003 0.881 1.000 0.152 0.265 0.269 0.478
Firmicutes_Leuconostocaceae 0.485 0.667 0.304 0.486 0.010 0.023 0.633 0.864 0.033 0.072 0.129 0.312
Proteobacteria_Unknown.2 0.134 0.370 0.515 0.686 0.001 0.003 0.047 0.258 0.000 0.001 0.024 0.113
Firmicutes_Unknown.2 0.464 0.665 0.476 0.681 0.627 0.690 1.000 1.000 0.793 0.818 0.760 0.900
Firmicutes_Lactobacillaceae 0.841 0.934 0.882 0.940 0.828 0.865 0.655 0.864 0.542 0.703 0.900 0.986
Fusobacteria_Fusobacteriaceae 0.286 0.556 NA NA 0.055 0.086 0.527 0.864 0.383 0.549 0.198 0.396
Tenericutes_Unknown 0.012 0.101 0.157 0.279 0.316 0.401 0.589 0.864 0.002 0.009 0.024 0.113
Proteobacteria_Campylobacteraceae 0.726 0.856 0.582 0.689 0.073 0.109 0.921 1.000 0.259 0.427 0.470 0.626
Firmicutes_Unknown.3 0.612 0.751 0.489 0.681 0.023 0.044 0.241 0.530 0.007 0.019 0.137 0.312
"""

if __name__ == "__main__":
    download_cs1()
    download_cs2()
    download_cs3()
    download_cs4()
