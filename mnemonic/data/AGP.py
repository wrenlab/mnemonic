"""
.. module: AGP

AGP
--------------------------------------------------------------------------------

American Gut Project data.

"""

import urllib.request

import patsy
import numpy as np
import pandas as pd
import sklearn.decomposition

from mnemonic.util import memoize, LOG, download
from mnemonic.dataset import Dataset
from mnemonic.model import MFitSM
import mnemonic.data.EBI as EBI
from mnemonic.taxonomy import TaxonomyLevel
from mnemonic.R import MFitEdgeR
import mnemonic.query.OTU
from mnemonic.data.AGP_variables import AGPVarCats

##########
# Metadata
##########

REMOVE = ["POOL", "TEETHBRUSHING", "FLOSSING", "COSMETICS", "SUPPLEMENT", "SMOKING", "HIGH_FAT_RED_MEAT"]

@memoize
def AGP_variables():
    return AGPVarCats()


def fix_metadata(A):

    # Partial string match
    frequencies = {
        "Daily": 1,
        "Regularly": 4/7,
        "Occasionally": 1.5/7,
        "Rarely": 1/14,
        "Never": 0
    }

    bowel_movement_quality = {
        "normal formed stool": 1,
        "diarrhea": 0,
        "constipated": 0,
        "don't know": np.nan,
    }

    # Exact match

    exact_match_levels = {
        "disease_states" : {
            'I do not have this condition': 0,
            'Self-diagnosed': 1,
            'Diagnosed by a medical professional (doctor, physician assistant)': 1,
            'Diagnosed by an alternative medicine practitioner': 1,
        },

        "bowel_movement_frequency" : {
            "Less than one": 0,
            "One": 1,
            "Two": 2,
            "Three": 3,
            "Four": 4,
            "Five or more": 5,
        },

        "weight_change" : {
            "Remained stable" : 0,
            "Decreased more than 10 pounds" : -1,
            "Increased more than 10 pound" : 1,
        },

        "antibiotic_history" : {
            "I have not taken antibiotics in the past year.": 0,
            "Year": 1,
            "6 months": 2,
            "Month": 3,
            "Week": 4,
        },

        "collection_month" : {
            "January": 1,
            "February": 2,
            "March": 3,
            "April": 4,
            "May": 5,
            "June": 6,
            "July": 7,
            "August": 8,
            "September": 9,
            "October": 10,
            "November": 11,
            "December": 12,
        },
    }


    '''
    collection_season = {}
    sleep_duration = {}
    sex = {}
    fed_as_infant = {}
    types_pf_plants = {}
    level_of_education = {}
    '''

    def fn(v):
        # Continuous: keep as is
        try:
            return v.astype(float)
        except:
            pass

        # Frequencies
        # has Never: never = 0, everything else = 1
        if any(k in list(v) for k in frequencies):
            o = []
            for e in v:
                if isinstance(e, str):
                    o.append(frequencies[e.split(" ")[0]])
                else:
                    o.append(e)
            return o

        for level_dict in exact_match_levels.values():
            if any(k in list(v) for k in level_dict):
                o = []
                for e in v:
                    if e in level_dict:
                        o.append(level_dict[e])
                    else:
                        o.append(e)
                return o
                continue

        if v.name == 'BOWEL_MOVEMENT_QUALITY':
            o = []
            for e in v:
                if isinstance(e, str):
                    for k in bowel_movement_quality.keys():
                        if e.find(k) > -1:
                            o.append(bowel_movement_quality[k])
                            continue
                else:
                    o.append(np.nan)
            return o

        '''
        # No such entries, removing ~Ola
        # Healthy/diseased
        # has Healthy: healthy = 0, else 1
        if "Healthy" in list(v):
            v[(v != "Healthy") & ~v.isnull()] = 1
            v[(v == "Healthy") & ~v.isnull()] = 0
            return v
        '''

        # Boolean
        if True in list(v):
            # "SUBSET" variables are reverse
            if v.name.startswith('SUBSET'):
                return [not i for i in v]
            return v

    o = []
    for c in A.columns:
        v = A[c]
        nv = fn(v.copy())
        if nv is not None:
            nv = pd.Series(nv, index=A.index)
            nv.name = c
            o.append(nv)
    return pd.concat(o, axis=1)

'''
def fix_frequency_term(ix):
    o = ix.rsplit("_", 1)[0].replace("_", " ").lower()
    if o.startswith("vitamin"):
        o = o.split(" ")
        o[1] = o[1].upper()
        o = " ".join(o)
    #if o == "milk cheese":
    #    o = "milk cheese"
    #if o == "meat eggs":
    #    o = "meat eggs"
    return o.replace(" ", "_")
'''

@memoize
def sample_map():
    uri = "https://www.ebi.ac.uk/metagenomics/projects/ERP012803/overview/doExport"
    with urllib.request.urlopen(uri) as h:
        o = pd.read_csv(h, usecols=[0,2])
        o.columns = ["SampleName", "SampleID"]
        return o

def read_AGP_metadata(agp_md_fp):
    md = pd.read_csv(agp_md_fp, index_col=0, sep='\t', low_memory=False)
#    md = get_sam_md()[['sample_id', 'sample_name']].merge(md, how='inner',
#                left_on='sample_name', right_on='#SampleID')
#    md.set_index('sample_id', drop=False, inplace=True)

    for na in ['no_data', 'Unspecified', 'Unknown', 'Not sure']:
        md.replace(na, np.nan, inplace=True)
    for tr in ['true', 'TRUE', 'True', 'Yes', 'yes', 'YES']:
        md.replace(tr, True, inplace=True)
    for fa in ['false', 'FALSE', 'False', 'No', 'no', 'NO']:
        md.replace(fa, False, inplace=True)
    return md

@memoize
def metadata():
    url = "https://www.dl.dropboxusercontent.com/s/e0x1luu79tldgti/metadata_American_Gut_cleaned.txt"
    path = download(url)
    #o = pd.read_csv(path, index_col=0, sep="\t", na_values="Unknown", low_memory=False)
    o = read_AGP_metadata(path)

    M = dict(sample_map().to_records(index=False))
    o = o.loc[o.index.isin(M),:]
    o.index = [M[ix] for ix in o.index]
    o.index.name = "SampleID"
    o = o.loc[o["SIMPLE_BODY_SITE"] == "FECAL",:]

    o = fix_metadata(o)

#    path = "data/AGP_variables.tsv"
#    M = pd.read_csv(path, sep="\t", skiprows=(1,))\
#            .dropna(subset=["category"])

    M = AGPVarCats().get_table()

    C = dict(zip(M["variable_name"], M["category"]))
    #M = dict(zip(M["variable_name"], M["edited_name"]))
    #M["AGE_YEARS"] = "Age"
    o = o.loc[:,o.columns.isin(M["variable_name"])]
    #ix = [(C[c],M[c]) for c in o.columns]
    ix = [(C[c], c) for c in o.columns]
    ix = pd.MultiIndex.from_tuples(ix, names=["Category", "Variable"])
    o.columns = ix

    # ============
    # Add variables
    mi = o['mental disease']
    x = mi.drop('DEPRESSION_BIPOLAR_SCHIZOPHRENIA', 1).sum(1).apply(lambda x: int(bool(x)))

    # depression, excluding other mental conditions
    depression = (mi['DEPRESSION_BIPOLAR_SCHIZOPHRENIA']-x).apply(lambda x: int(x==1))
    depression[mi['DEPRESSION_BIPOLAR_SCHIZOPHRENIA'].isna()] = np.nan

    o['mental disease','DEPRESSION_INFERRED'] = depression
    # ============

    return o.sort_index(axis=1)


#########
# Dataset
#########

class Decomposition(object):
    def __init__(self, components):
        self.components = components

    def plot(self, target=None):
        max_points = 250
        import seaborn as sns
        import matplotlib.pyplot as plt

        assert target is not None
        target = target.dropna()
        components, target = self.components.align(target, axis=0, join="inner")
        r = components.corrwith(target)
        df = components.loc[:,r.abs().sort_values(ascending=False).index[:2]]
        #df = components.iloc[:,:2]

        n_bins = min(10, len(set(target)))
        _, bins = np.histogram(target, bins=n_bins)
        bins = bins.round(2)
        ix = np.searchsorted(bins, target)
        #bin_names = np.array(["{}-{}".format(bins[i], bins[i+1]) for i in range(len(bins)-1)])
        if (bins.max() - bins.min()) > (len(bins) * 2):
            bins = bins.astype(int)
        bin_names = bins
        df["Category"] = bin_names[ix]
        if df.shape[0] > max_points:
            df = df.loc[np.random.choice(df.index, max_points, replace=False)]
        return sns.lmplot(x=df.columns[0], y=df.columns[1],
                hue=df.columns[2], palette="RdBu_r",
                hue_order=bin_names,
                data=df, fit_reg=False)

    """
    def plot_joint_kde(self, target):
        import seaborn as sns
        components, target = self.components.align(target, axis=0, join="inner")
        components = components.iloc[:,:2]

        center = target.median()
        low = components.loc[target > center,:]
        high = components.loc[target >= center,:]

        ax = sns.kdeplot(low.iloc[:,0], low.iloc[:,1],
                cmap="Reds", shade=True, shade_lowest=False)
        ax = sns.kdeplot(high.iloc[:,0], high.iloc[:,1],
                cmap="Blues", shade=True, shade_lowest=False)

        #red = sns.color_palette("Reds")[-2]
        #blue = sns.color_palette("Blues")[-2]
        #ax.text(2.5, 8.2, "virginica", size=16, color=blue)
        #ax.text(3.8, 4.5, "setosa", size=16, color=red)
    """

class AGPDataset(Dataset):
    def __init__(self, A="AGP", X="AGP", filter=True):
        if isinstance(A, str):
            A = metadata()
        if isinstance(X, str):
            X = EBI.counts()

        if filter:
            sigma = X.sum(axis=1)
            X = X.loc[sigma > 10000,:]
            mu = X.mean()
            X = X.loc[:,mu > 1]

        if filter:
            super().__init__(A,X)
        else:
            super().__init__(A,X, remove_low=3)

    def design(self, category=None):
        o = super().design()
        if category is not None:
            o = o.xs(category, axis=1)
        return o

    def fit(self, formula=None, category=None, level=TaxonomyLevel.SPECIES):
        A = self.design()
        if category is not None:
            A = A.xs(category, axis=1)
        else:
            A.columns = A.columns.droplevel()
        if formula is None:
            A = A.loc[:,A.dtypes == np.float64]
            A = A.loc[:,(~A.isnull()).sum() > 50]
            formula = " + ".join(A.columns)
        X = self.data(level=level)
        D = patsy.dmatrix(formula, data=A, return_type="dataframe").dropna()
        X,D = X.align(D, axis=0, join="inner")
        # Filter low expressed OTUs
        mu = X.mean()
        return MFitEdgeR.fit(X, D)

    def OTU(self, key, level=TaxonomyLevel.SPECIES):
        return mnemonic.query.OTU.OTU(key, self, level=level)

    def fit2(self, formula=None, include_age=False, level=TaxonomyLevel.SPECIES):
        import statsmodels.api as sm
        if formula is None:
            A = self.frequency_design()
            if include_age:
                A = pd.concat([self.A.loc[:,["AGE_YEARS"]], self.frequency_design()],
                        axis=1)
            formula = " + ".join(A.columns)
            X = self.data(level=level)
            X = X.loc[X.sum(axis=1) > 10000,:]
            X = X.loc[:,X.mean() > 10]
            if True:
                X = (X.T / X.sum(axis=1)).T
                family = sm.families.Gaussian()
            else:
                family = None
            return MFitSM.fit(formula, X, A, family=family)
        else:
            return super().fit(formula)

    def PCA(self, n_components=10, level=TaxonomyLevel.SPECIES):
        X = self.data(normalize=True, level=level)
        model = sklearn.decomposition.PCA(n_components=n_components)
        o = pd.DataFrame(model.fit_transform(np.array(X)),
                index=X.index, columns=["C{}".format(i+1) for i in range(n_components)])
        return Decomposition(o)

    def ICA(self, variable, n_components=10, level=TaxonomyLevel.SPECIES):
        X = self.data(normalize=True, level=level)
        y = self.design().xs(variable, axis=1, level=1).dropna()
        X,y = X.align(y, join="inner", axis=0)
        model = sklearn.decomposition.FastICA(n_components=n_components, max_iter=1000)
        o = pd.DataFrame(model.fit_transform(X,y),
                index=X.index, columns=["C{}".format(i+1) for i in range(n_components)])
        return Decomposition(o)


#############################
# Precomputed data / analyses
#############################

@memoize
def dataset():
    """
    Get the AGP dataset in a table format.
    """
    print("forcing refresh")
    return AGPDataset()

@memoize
def _combined_contrasts(category, level=TaxonomyLevel.SPECIES):
    DS = dataset()
    o = {k:v for (k,v) in DS.fit(category=category, level=level).contrasts().items()
            if k != "Intercept"}
    return level, o

def combined_contrasts(category, level=TaxonomyLevel.SPECIES):
    from mnemonic.contrast import ContrastSet
    level, o = _combined_contrasts(category, level=level)
    return ContrastSet(o, level=level)

@memoize
def contrast(key, level=TaxonomyLevel.SPECIES):
    LOG.info("Computing contrast results for: '{}'".format(key))
    DS = dataset()
    fit = DS.fit(key, level=level)
    return fit.contrasts()[key]

def contrasts(category, level=TaxonomyLevel.SPECIES):
    any_ok = False
    from mnemonic.contrast import ContrastSet
    DS = dataset()
    o = {}

    D = DS.design(category=category)
    ix = D.columns[
            (D.dtypes == np.float64) &
            ((~D.isnull()).sum() > 50) &
            np.array([len(D[c].unique()) >= 2 for c in D.columns])
    ]
    for k in ix:
        try:
            o[k] = contrast(k, level=level)
            any_ok = True
        except:
            pass
    if not any_ok:
        raise Exception("All contrasts failed for key={}, level={}".format(key, level))
    return ContrastSet(o, level=level)

@memoize
def diversity():
    DS = dataset()
    return DS.alpha_diversity()

@memoize
def diversity_combined_contrasts(category, level=TaxonomyLevel.SPECIES):
    import statsmodels.api as sm
    DS = dataset()
    A = DS.design(category=category).dropna(how="all")
    X = diversity()
    A,X = A.align(X, axis=0, join="inner")
    formula = " + ".join(A.columns)
    family = sm.families.Gaussian()
    return MFitSM.fit(formula, X, A, family=family)

def refresh():
    """
    Run and memoize all prepared analyses.
    """
    DS = dataset()
    D = DS.design()
    categories = D.columns.levels[0]
    for c in categories:
        for level in TaxonomyLevel.__members__.values():
            if level.name == "KINGDOM":
                continue
            try:
                contrasts(c, level=level)
            except:
                pass
            try:
                combined_contrasts(c, level=level)
            except:
                pass
            try:
                diversity_combined_contrasts(c, level=level)
            except:
                pass

