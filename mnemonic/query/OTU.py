import numpy as np

from mnemonic.taxonomy import TaxonomyLevel
import mnemonic.distance

class OTU(object):
    """
    Class for querying core datasets for similarities with
    a given OTU.
    """
    def __init__(self, key, dataset, level=TaxonomyLevel.SPECIES):
        # FIXME: allow various types of input like taxon ID, for now just species (or whatever) name
        self.key = key
        self.level = level
        self.dataset = dataset

    def profile_similarity(self):
        """
        Return table of other OTUs sorted by similar abundance profiles.
        """
        X = self.dataset.data(level=self.level)
        q = X[self.key]
        def fn(x):
            return mnemonic.distance.CC(x, q)
        o = X.apply(fn).T
        o["MeanLogAbundance"] = X.mean().apply(np.log2)
        return o.sort_values("r", ascending=False)
