from abc import ABC, abstractproperty

import patsy
import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.sandbox.stats.multicomp

class MFit(object):
    @abstractproperty
    def coef(self):
        pass

    @abstractproperty
    def p(self):
        pass

    @property
    def SLPV(self):
        p = self.p
        coef = self.coef
        return -1 * coef.apply(np.sign) * p.apply(np.log)

    def summary(self, coef):
        p = self.p[coef]
        p_adjusted = pd.Series(statsmodels.sandbox.stats.multicomp.multipletests(p)[1], 
                index=p.index)
        o = pd.concat([
            self.coef[coef],
            p,
            p_adjusted,
            self.SLPV[coef]
        ], axis=1)
        o.columns = ["coef", "p", "p_adjusted", "SLPV"]
        return o.sort_values("p")

class MFitSM(MFit):
    def __init__(self, fits):
        self.M = fits

    @staticmethod
    def fit(formula, Y, A, family=None):
        if family is None:
            family = sm.families.NegativeBinomial()

        o = {}
        D = patsy.dmatrix(formula, data=A, return_type="dataframe")
        for j in range(Y.shape[1]):
            try:
                y = Y.iloc[:,j]
                DD,y = D.align(y, axis=0, join="inner")
                model = sm.GLM(y,DD,family=family)
                o[Y.columns[j]] = model.fit()
            except:
                continue
        return MFitSM(o)

    @property
    def coef(self):
        o = {}
        for k,fit in self.M.items():
            o[k] = fit.params
        return pd.DataFrame(o).T

    @property
    def t(self):
        o = {}
        for k,fit in self.M.items():
            o[k] = fit.tvalues
        return pd.DataFrame(o).T

    @property
    def p(self):
        o = {}
        for k,fit in self.M.items():
            o[k] = fit.pvalues
        return pd.DataFrame(o).T

    
