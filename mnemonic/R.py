import bidict
import hashlib
import numpy as np
import patsy
import pandas as pd
import tqdm
from statsmodels.sandbox.stats.multicomp import multipletests
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri, r

from mnemonic.util import LOG

pandas2ri.activate()

def replace_index_names(X, names_from, names_to, axis=1):
    assert len(names_from) == len(names_to)
    assert axis in (0,1)
    if axis == 0:
        X = X.T

    columns = list(X.columns)
    for nf,nt in zip(names_from, names_to):
        for i,c in enumerate(columns):
            if c == nf:
                columns[i] = nt
                break

    X.columns = columns
    if axis == 0:
        X = X.T
    return X

def make_mapping(ix):
    return bidict.bidict({k:"T"+hashlib.md5(k.encode("utf-8")).hexdigest() for k in ix})

class MFitEdgeR(object):
    def __init__(self, fit, N, maps):
        self.pkg = importr("edgeR")
        self.N = N
        self.maps = maps
        self.fit = fit

    def contrasts(self):
        coefs = list(self.fit[self.fit.names.index("coefficients")].colnames)
        LOG.info("Extracting coefficient parameters...".format(len(coefs)))
        results = {}
        for ix in coefs:
            key = self.maps["D_columns"].inv[ix]
            encoded_key = self.maps["D_columns"][key]
            ct = self.pkg.glmLRT(self.fit, coef=encoded_key)
            table = r["as.data.frame"](self.pkg.topTags(ct, n=self.N))
            o = pandas2ri.ri2py(table)
            o.index = list(r["rownames"](table))
            o.index = [self.maps["X_columns"].inv[ix] for ix in o.index]
            o = replace_index_names(o, ["PValue"], ["p"])
            # TODO: add p_adjusted?
            o["SLPV"] = o["p"].apply(np.log10) * np.sign(o["logFC"])
            results[key] = o
        return results

    @staticmethod
    def fit(X, D):
        LOG.info("Fitting model with shapes: X={} D={}".format(X.shape, D.shape))
        # FIXME: ensure align
        assert X.shape[0] == D.shape[0]
        nr = X.shape[0]

        M_index = make_mapping(X.index)
        M_X_columns = make_mapping(X.columns)
        M_D_columns = make_mapping(D.columns)

        D = D.copy()
        X = X.copy()
        X.index = [M_index[ix] for ix in X.index]
        X.columns = [M_X_columns[ix] for ix in X.columns]
        D.index = [M_index[ix] for ix in D.index]
        D.columns = [M_D_columns[ix] for ix in D.columns]

        Xr = pandas2ri.py2ri(X.T)
        Dr = pandas2ri.py2ri(D)

        pkg = importr("edgeR")
        y = pkg.DGEList(counts=Xr)
        dispersion = pkg.estimateDisp(y, Dr)
        fit = pkg.glmFit(dispersion, Dr)
        return MFitEdgeR(fit, nr, {"X_columns": M_X_columns, "D_columns": M_D_columns})

def normalize_counts(X):
    # Samples X OTU
    pkg = importr("EDASeq") # Not updated?
    mu = X.mean()
    X = X.loc[:,mu >= mu.quantile(0.75)]
    DS = pkg.newSeqExpressionSet(counts=np.array(X.T))
    n = pkg.betweenLaneNormalization(DS, round=False, offset=True)
    return pd.DataFrame(np.array(pkg.normCounts(n)).T,
            index=X.index, columns=X.columns)

def test(DS):
    formula = "AGE_YEARS + FRUIT_FREQUENCY"
    D = patsy.dmatrix(formula, data=DS.design(), return_type="dataframe").dropna()
    X = DS.data()
    X = X.loc[:,X.sum() > X.sum().quantile(0.75)]
    D,X = D.align(X, axis=0, join="inner")
    return MFitEdgeR.fit(X, D)
