import tqdm
import numpy as np
import pandas as pd
import skbio.diversity.alpha as A
import scipy.stats

ALPHA_FUNCTIONS = {
    "ACE": A.ace,
    "BergerParkerDominance": A.berger_parker_d,
    "BrillouinIndex": A.brillouin_d,
    "Chao1": A.chao1,
    "Dominance": A.dominance,
    "ENS_pie": A.enspie,
    #"FaithPD": A.faith_pd,
    "FisherAlpha": A.fisher_alpha,
    "Gini": A.gini_index,
    "GoodsCoverage": A.goods_coverage,
    "HeipEvenness": A.heip_e,
    "KemptonTaylorQ": A.kempton_taylor_q,
    "Lladser": A.lladser_pe,
    "Margalef": A.margalef,
    "McintoshD": A.mcintosh_d,
    "McintoshE": A.mcintosh_e,
    "MenhinickRichness": A.menhinick,
    #"MichaelisMentenFit": A.michaelis_menten_fit,
    "ObservedOTUs": A.observed_otus,
    #"OSD": A.osd,
    "PielouJ": A.pielou_e,
    "Robbins": A.robbins,
    "ShannonEntropy": A.shannon,
    "SimpsonIndex": A.simpson,
    "SimpsonEvenness": A.simpson_e,
    "Singles": A.singles,
    "StrongDominance": A.strong
}

def alpha(x):
    o = {}
    for k,fn in ALPHA_FUNCTIONS.items():
        try:
            o[k] = fn(x)
        except:
            o[k] = np.nan
    o = pd.Series(o)
    o.name = x.name
    return o

"""
def permutation_test(X, covariate, fn, n=1000):
    # Determine p-value for the correlation between diversity statistic and a covariate
    # using a permutation test.

    # FIXME: allow LM?
    # FIXME: this is useless with Pearson
    covariate = covariate.dropna()
    ix = list(set(X.index) & set(covariate.index))
    X = X.loc[ix,:]
    covariate = covariate.loc[ix]

    if isinstance(fn, str):
        fn = ALPHA_FUNCTIONS[fn]

    metric = X.apply(fn, axis=1)
    r, p = scipy.stats.pearsonr(metric, covariate)

    r_p = []
    for i in tqdm.trange(n):
        rs = scipy.stats.pearsonr(metric, np.random.permutation(covariate))
        r_p.append(rs[0])
    r_p = np.array(r_p)

    gt = (r_p > r).mean()
    lt = (r_p < r).mean()
    return p, gt, lt, gt + lt
"""
