import seaborn as sns
import pylatexenc

import mnemonic.statistics

def fix_index(ix):
    # for LaTeX output
    return [pylatexenc.latexencode.utf8tolatex(k) for k in ix]

def clustermap(X, color_xticks=None, color_yticks=None, transpose=False, cmap="RdBu_r", **kwargs):
    """
    color_xticks, color_yticks: dict
        color -> list<label>
        e.g. {"red": ["C1","C3","C5"]}
    """
    X = mnemonic.statistics.impute(X.copy())
    X.index = fix_index(X.index)
    X.columns = fix_index(X.columns)
    if transpose:
        X = X.T
    cg = sns.clustermap(X, cmap=cmap, **kwargs)

    # Set colors
    def color_ticks(axis, M):
        if M is None:
            return
        ix = [t.get_text() for t in axis.get_ticklabels()]
        for color, labels in M.items():
            for label in labels:
                # FIXME:
                try:
                    axis.get_ticklabels()[ix.index(label)].set_color(color)
                except:
                    pass
    color_ticks(cg.ax_heatmap.xaxis, color_xticks)
    color_ticks(cg.ax_heatmap.yaxis, color_yticks)
    return cg
