"""
.. module: paper_figures

paper_figures
--------------------------------------------------------------------------------

Generate figures that appear in the paper.
"""

import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import mnemonic.data.AGP as AGP
import mnemonic.data.case_study as CS
import mnemonic.contrast
from mnemonic.contrast import ContrastSet, Contrast

from mnemonic.util import make
from mnemonic.taxonomy import TaxonomyLevel

from mnemonic.data.AGP_variables import AGPVarCats
from mnemonic.data.ProTraits_variables import PTVarCats

# TODO missing:
# Figure 1: Metagenomics in literature / year
# Figure 2: Metagenomic data / year
# Figure 5: Validation
# Figure S2: does not appear in the paper

palette = {
    'red'   : sns.color_palette('muted')[2],
    'black' : sns.light_palette('black')[-2],
    'gray'  : sns.light_palette('black')[-4],
    'blue'  : sns.color_palette('muted')[0],
    }


def CPM():
    DS = AGP.dataset()
    X = DS.data(level=TaxonomyLevel.PHYLUM)
    logCPM = (1000000 * X.T / X.sum(axis=1)).T.mean().apply(np.log2)
    df = logCPM.sort_values(ascending=False).head(10).to_frame().reset_index()
    df.columns = ["Phylum", "logCPM"]
    sns.barplot(x="logCPM", y="Phylum", data=df, color=palette["gray"])

def fix(x):
    fields = x.split("_")
    if fields[-1] == "FREQUENCY":
        fields = fields[:-1]
    fields[0] = fields[0].title()
    for i in range(1, len(fields)):
        fields[i] = fields[i].lower()
        if fields[0] == "Vitamin":
            fields[1] = fields[1].upper()
    return " ".join(fields)

def f1(key, groups=2, level=TaxonomyLevel.GENUS):
    ct = AGP.contrasts(key, level=level)
    if key == "food":
        ct = ContrastSet({fix(k):v.data for k,v in ct.items() if not k.startswith("ALCOHOL")},
                            level=level)
    elif key == "physical disease":
        ct = ContrastSet({k:v.data for k,v in ct.items() if not k in
                        ["CLINICAL_CONDITION", "SKIN_CONDITION", "CARDIOVASCULAR_DISEASE"]},
                        level=level)
    ct.clustermap(groups=groups, transpose=True, z_score=1)

def contrast(key, level=TaxonomyLevel.SPECIES):
    ct = AGP.contrast(key, level=level)
    return Contrast(key, ct, level=level)

def f2(key):
    ct = contrast(key, level=TaxonomyLevel.SPECIES).ProTraits()
    ct.index = [ix.replace("α", "alpha") for ix in ct.index]
    ct.index.name = "Key"
    ct = ct.reset_index()
    ct = ct.query("p <= 0.05").sort_values("r", ascending=False)
    sns.barplot(x="r", y="Key", data=ct, orient="h", color=palette["gray"])
    plt.ylabel("")
    plt.xlabel("Pearson correlation coefficient")

def f5(category):
    DS = AGP.dataset()
    D = DS.design().xs(category, axis=1)
    D = D.loc[:,D.dtypes == np.float64]
    da = AGP.diversity()
    r = mnemonic.statistics.corr(D, da).ACE
    r = r.loc[[c for c in r.index if not "ALCOHOL" in c]]
    r.index = [fix(ix) for ix in r.index]
    r = r.to_frame().reset_index()
    r.columns = ["Contrast", "rho"]
    r = r.dropna().sort_values("rho")
    sns.barplot(x="rho", y="Contrast", data=r, color=palette["gray"], orient="h")


#@make.figure
#def Figure3_AGP_term_categories():
#    """
#    Figure 3
#    """
#    a = AGPVarCats()
#    return a.plot_var_counts(major=True)

@make.figure
def Figure4a_AGP_term_Age():
    a = AGPVarCats()
    return a.plot_single_var('AGE_YEARS')

@make.figure
def Figure4b_AGP_term_Sugary_sweets():
    a = AGPVarCats()
    return a.plot_single_var('SUGARY_SWEETS_FREQUENCY')

#@make.figure
#def Figure6_AGP_most_abundant_phyla():
#    return CPM()
#
#@make.figure
#def Figure7_AGP_food_vs_genera():
#    return f1('food')
#
#
#@make.figure
#def Figure8_AGP_food_vs_diversity():
#    return f5('food')
#
#@make.figure
#def Figure9_ProTraits_terms_vs_diabetes():
#    return f2('DIABETES')
#
#@make.figure
#def Figure10_ProTraits_terms_vs_IBD():
#    return f2('IBD')
#
#@make.figure
#def Figure11_AGP_lFC_vs_autism():
#    ct = AGP.contrasts('mental disease')
#    return ct['ASD'].plot.bar()
#
#@make.figure
#def FigureS1():
#    return f2("FRUIT_FREQUENCY")
#

######
# Main
######

def main():
    make.generate()

if __name__ == "__main__":
    main()

