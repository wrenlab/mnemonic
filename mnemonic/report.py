"""
.. module: report

report
--------------------------------------------------------------------------------

Generate various figures & tables demonstrating usage of the package descriptives on EBI/AGP.
"""

"""
NOTES
DS.alpha_diversity()

"""

import seaborn as sns
import mnemonic.data.AGP
import mnemonic.data.AGP as AGP
import mnemonic.data.case_study as CS
#import mnemonic.statistics
import mnemonic.contrast
from mnemonic.util import make
from mnemonic.taxonomy import TaxonomyLevel

from mnemonic.data.AGP_variables import AGPVarCats
from mnemonic.data.ProTraits_variables import PTVarCats

##########
# Bar plot
##########

@make.figure
def AGP_bar_species_fruit():
    ct = mnemonic.data.AGP.contrasts("food", level=TaxonomyLevel.GENUS)
    return ct["FRUIT_FREQUENCY"].plot.bar()

@make.figure
def AGP_bar_class_age():
    ct = mnemonic.data.AGP.contrasts("demographic", level=TaxonomyLevel.CLASS)
    return ct["AGE_YEARS"].plot.bar()

@make.figure
def AGP_bar_species_age():
    ct = mnemonic.data.AGP.contrasts("demographic")
    return ct["AGE_YEARS"].plot.bar()

@make.figure
def AGP_bar_class_whole_grain():
    ct = AGP.contrasts("food", level=TaxonomyLevel.CLASS)
    return ct['WHOLE_GRAIN_FREQUENCY'].plot.bar()

@make.figure
def AGP_bar_genus_whole_grain():
    ct = AGP.contrasts("food", level=TaxonomyLevel.GENUS)
    return ct['WHOLE_GRAIN_FREQUENCY'].plot.bar()

@make.figure
def AGP_bar_species_whole_grain():
    ct = AGP.contrasts("food", level=TaxonomyLevel.SPECIES)
    return ct['WHOLE_GRAIN_FREQUENCY'].plot.bar()

############
# Dendrogram
############

@make.figure
def AGP_dendrogram():
    ct = AGP.contrasts("food", level=TaxonomyLevel.GENUS)
    return ct.dendrogram()

############
# Clustermap
############

@make.figure
def AGP_food_clustermap():
    ct = AGP.contrasts("food")
    return ct.clustermap(standard_scale=0)

##############
# MA & volcano
##############

@make.figure
def AGP_fruit_MA():
    ct = AGP.contrasts("food")
    return ct["FRUIT_FREQUENCY"].plot.MA()

@make.figure
def AGP_fruit_volcano():
    ct = AGP.contrasts("food")
    return ct["FRUIT_FREQUENCY"].plot.volcano()

############
# Literature
############

@make.figure
def AGP_literature_clustermap_Dr():
    ct = AGP.contrasts("food")
    lct = ct.literature_associations("Dr")
    return lct.clustermap(standard_scale=0, param="r")

@make.figure
def AGP_literature_clustermap_D():
    ct = AGP.contrasts("food")
    lct = ct.literature_associations("D")
    return lct.clustermap(standard_scale=0, param="r")

@make.figure
def AGP_literature_clustermap_CP():
    ct = AGP.contrasts("food")
    lct = ct.literature_associations("CP")
    return lct.clustermap(standard_scale=0, param="r")

###########
# ProTraits
###########

@make.figure
def AGP_ProTraits():
    ct = AGP.contrasts("food")
    pt = ct.ProTraits()
    return pt.clustermap("t")

####################
# Profile similarity
####################

@make.table
def S_mirabilis_profile_similarity():
    species = "Streptomyces mirabilis"
    DS = AGP.dataset()
    q = DS.OTU(species)
    df = q.profile_similarity()
    return df.head(10).round(2)

####################
# Combined contrasts
####################

"""
@make.figure
def AGP_CC_ProTraits():
    ct = AGP.combined_contrasts("food")
    pt = ct.ProTraits()
    return pt.clustermap("t")

@make.figure
def CS_CC_Duvallet_vs_disease_clustermap():
    ct = AGP.combined_contrasts("physical disease", level=TaxonomyLevel.GENUS)
    q = CS.Duvallet(collapse=True)
    ct.update(q)
    return ct.clustermap(param="SLPV", color_xticks={"red": list(q.keys())})

@make.figure
def AGP_CC_dendrogram():
    ct = AGP.combined_contrasts("food")
    return ct.dendrogram()

@make.figure
def AGP_CC_bar_class_fruit():
    ct = AGP.combined_contrasts("food", level=TaxonomyLevel.CLASS)
    return ct.bar("FRUIT_FREQUENCY")
"""

##############
# Case studies
##############

# FIXME: rho vs logFC
"""
@make.figure
def CS_ERP002469_clinical():
    ct = AGP.contrasts("food")
    q = CS.ERP002469_clinical()
    ct.update(q)
    return ct.clustermap(color_xticks={"red": list(q.keys())})

@make.figure
def CS_ERP002469():
    ct = AGP.contrasts("physical disease")
    q = CS.ERP002469_clinical()
    return q.compare(ct)
"""

@make.figure
def CS_Duvallet_vs_disease_clustermap():
    ct = AGP.contrasts("physical disease", level=TaxonomyLevel.GENUS)
    q = CS.Duvallet(collapse=True)
    ct.update(q)
    return ct.clustermap(param="SLPV", color_xticks={"red": list(q.keys())})

##########
# Datasets
##########

@make.figure
def AGP_term_categories_minor():
    a = AGPVarCats()
    return a.plot_var_counts(major=False)

@make.figure
def AGP_term_categories_major():
    a = AGPVarCats()
    return a.plot_var_counts(major=True)

@make.figure
def AGP_term_count_Whole_grain():
    a = AGPVarCats()
    return a.plot_single_var('WHOLE_GRAIN_FREQUENCY')

@make.figure
def ProTraits_term_categories():
    p = PTVarCats()
    return p.plot_var_counts()

######
# Main
######

def main():
    make.generate()

if __name__ == "__main__":
    main()
