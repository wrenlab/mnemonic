"""
.. module: contrast

contrast
--------------------------------------------------------------------------------

N.B. Throughout this project, "contrast" is used relatively interchangeably
with "linear model term/coefficient".
"""

import scipy.stats
import sklearn.metrics
import sklearn.cluster
import scipy.cluster.hierarchy
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import pylatexenc.latexencode

from mnemonic.taxonomy import TaxonomyLevel
import mnemonic.literature
import mnemonic.data.AGP
import mnemonic.statistics
import mnemonic.distance
import mnemonic.plot

def palette():
    pal = {
        # is green now
        # 'red'   : sns.color_palette('muted')[2],
        'red'   : sns.color_palette('muted')[3],
        'black' : sns.light_palette('black')[-2],
        'gray'  : sns.light_palette('black')[-4],
        'blue'  : sns.color_palette('muted')[0],
        }
    return pal

def only_printable(df):
    def fn(index):
        o = []
        for ix in index:
            try:
                ix.encode("ascii")
                o.append(True)
            except:
                o.append(False)
        return o
    rix, cix = fn(df.index), fn(df.columns)
    return df.loc[rix,:].loc[:,cix]

def fix_index(ix):
    # for LaTeX output
    return [pylatexenc.latexencode.utf8tolatex(k) for k in ix]

class Contrast(object):
    """
    Contains modeling results for a single contrast.
    """
    class Plot(object):
        def __init__(self, ct):
            self.ct = ct

        def MA(self):
            plt.clf()
            df = self.ct.data.loc[:,["logFC","logCPM","FDR"]]

            sns.set_style('whitegrid')
            alpha = 0.05
            pal = palette()

            colors = [pal['red'] if i else pal['black']
                        for i in (df['FDR'] < alpha).tolist()]

            plt.scatter(df['logCPM'], df['logFC'],
                    c = colors, alpha = 0.5, s = 15)

            plt.tick_params(top=False, bottom=False, left=False, right=False,
                            labelleft=True, labelbottom=True)

            for spine in plt.gca().spines.values():
                spine.set_visible(False)

            plt.tight_layout()
            plt.ylabel("$log_{2}$(fold change)")
            plt.xlabel("$log_{2}$(base mean)")


        def volcano(self):
            plt.clf()

            df = self.ct.data.loc[:,["logFC", "p"]].copy()
            df['p10'] = - df['p'].apply(np.log10)

            sns.set_style('whitegrid')

            alpha = 0.05
            pal = palette()

            colors = [pal['red'] if i else pal['black']
                        for i in (df['p'] < alpha).tolist()]

            plt.scatter(df['logFC'], df['p10'], c = colors, alpha = 0.5, s=15)
            plt.axhline(y=-np.log10(0.05), color=pal['red'], linestyle='--')

            plt.tick_params(top=False, bottom=False, left=False, right=False,
                            labelleft=True, labelbottom=True)

            for spine in plt.gca().spines.values():
                spine.set_visible(False)


            plt.tight_layout()
            plt.xlabel("$log_{2}$(fold change)")
            plt.ylabel("$-log_{10}$(p)")


        def bar(self, sort_by='logFC', show_n=20, show_specific=None):
            '''
            sort_by :   'logFC', 'p' (name of column in data)
            '''
            plt.clf()
            sns.set_style('whitegrid')
            alpha = 0.05

            pal = palette()

            df = self.ct.data.loc[:,["logFC", "p"]].copy()

            df.sort_values('logFC', inplace=True)

            if isinstance(show_n, int):
                if show_specific is not None:
                    if not hasattr(show_specific, "__iter__"):
                        show_specific = [show_specific]
                    assert all([i in df.index for i in show_specific])
                    tmp = df.ix[show_specific]
                if show_n < df.shape[0]:
                    df = pd.concat([
                        df.sort_values('logFC').head(round(show_n/2)),
                        df.sort_values('logFC').tail(round(show_n/2))
                        ])
                if show_specific is not None:
                    df = pd.concat([df, tmp]).drop_duplicates()

                if sort_by == "p":
                    df.sort_values(sort_by, ascending=False, inplace=True)
                else:
                    df.sort_values(sort_by, ascending=True, inplace=True)

            pos = [i for i in range(df.shape[0])]

            #colors = [palette['blue'] if i else palette['red']
            #        for i in (df["logFC"] < 0).tolist()]
            colors = ['blue' if i else 'red'
                    for i in (df["logFC"] < 0).tolist()]

            colors = ['gray' if not i else keep
                    for i, keep in zip((df["p"] < alpha).tolist(), colors)]

            colors = [pal[c] for c in colors]


            bars = plt.barh(pos, df['logFC'], height=0.4, #xerr = ses,
                        align='center', linewidth=0, color=colors)

            for bar in bars:
                if bar.get_width() < 0:
                    shift = 2
                elif bar.get_width() > 0:
                    shift = -2
                else:
                    shift = 0

            tick_labels = fix_index(df.index)
            plt.yticks(pos, tick_labels, fontsize=200/show_n, rotation='horizontal');
            #plt.yticks(pos, tick_labels, rotation='horizontal');
            plt.tick_params(top=False, bottom=False, left=False, right=False,
                            labelleft=True, labelbottom=True)

            for spine in plt.gca().spines.values():
                spine.set_visible(False)

            plt.xlabel("$log_{2}$(fold change)")
            plt.ylabel(self.ct.level.name)

            #FIXME
            #plt.tight_layout()
            #plt.subplots_adjust(bottom=0.4)
            plt.subplots_adjust(left=  0.6)
            #plt.subplots_adjust(top=   1)
            #plt.subplots_adjust(right= 1)


        def globalFC(self, show_n=20):
            plt.clf()
            sns.set_style('whitegrid')
            alpha = 0.05

            pal = palette()

            df = self.ct.data.loc[:,["logFC", "p"]].copy()

            df.sort_values('logFC', inplace=True)
            pos = [i for i in range(df.shape[0])]

            colors = ['blue' if i else 'red'
                    for i in (df["logFC"] < 0).tolist()]
            colors = ['gray' if not i else keep
                    for i, keep in zip((df["p"] < alpha).tolist(), colors)]
            colors = [pal[c] for c in colors]


            bars = plt.barh(pos, df['logFC'], height=1, #xerr = ses,
                        align='center', linewidth=0, color=colors)

            plt.tick_params(top=False, bottom=False, left=False, right=False,
                            labelleft=True, labelbottom=True)

            for spine in plt.gca().spines.values():
                spine.set_visible(False)

            plt.tight_layout()
            plt.xlabel("$log_{2}$(fold change)")
            plt.ylabel(self.ct.level.name)

        def bar_protraits(self, show_n=20, show_specific=None):
            '''
            '''
            sort_by = "p"

            plt.clf()
            sns.set_style('whitegrid')
            alpha = 0.05

            pal = palette()

            df = self.ct.data.loc[:,["ExplainedVariance", "p"]].copy()

            df.sort_values(sort_by, inplace=True)

            if isinstance(show_n, int):
                if show_specific is not None:
                    if not hasattr(show_specific, "__iter__"):
                        show_specific = [show_specific]
                    assert all([i in df.index for i in show_specific])
                    tmp = df.ix[show_specific]
                if show_n < df.shape[0]:
                    df = df.sort_values(sort_by).head(show_n)
                if show_specific is not None:
                    df = pd.concat([df, tmp]).drop_duplicates()

                if sort_by == "p":
                    df.sort_values(sort_by, ascending=False, inplace=True)
                else:
                    df.sort_values(sort_by, ascending=True, inplace=True)

            pos = [i for i in range(df.shape[0])]

            #colors = [palette['blue'] if i else palette['red']
            #        for i in (df["ExplainedVariance"] < 0).tolist()]
            colors = ['blue' if i else 'red'
                    for i in (df["ExplainedVariance"] < 0).tolist()]

            colors = ['gray' if not i else keep
                    for i, keep in zip((df["p"] < alpha).tolist(), colors)]

            colors = [pal[c] for c in colors]


            bars = plt.barh(pos, df['ExplainedVariance'], height=0.4, #xerr = ses,
                        align='center', linewidth=0, color=colors)

            for bar in bars:
                if bar.get_width() < 0:
                    shift = 2
                elif bar.get_width() > 0:
                    shift = -2
                else:
                    shift = 0

            tick_labels = fix_index(df.index)
            plt.yticks(pos, tick_labels, fontsize=200/show_n, rotation='horizontal');
            #plt.yticks(pos, tick_labels, rotation='horizontal');
            plt.tick_params(top=False, bottom=False, left=False, right=False,
                            labelleft=True, labelbottom=True)

            for spine in plt.gca().spines.values():
                spine.set_visible(False)

            plt.xlabel("explained variance")
            plt.ylabel(self.ct.level.name)

            #FIXME
            #plt.tight_layout()
            #plt.subplots_adjust(bottom=0.4)
            plt.subplots_adjust(left=  0.6)
            #plt.subplots_adjust(top=   1)
            #plt.subplots_adjust(right= 1)

        def diversity(self, metric="ShannonEntropy"):
            '''
            This doesn't really belong here - not based on the contrast idea
            TODO: move elsewhere
            '''
            plt.clf()
            sns.set_style('whitegrid')
            alpha = 0.05

            pal = palette()

            df = mnemonic.data.AGP.AGPDataset()
            DS = mnemonic.data.AGP.dataset()
            adiv = df.alpha_diversity()
            des = DS.design('physical disease')
            contrast_samples = des[des['DIABETES'] == 0].index
            healthy_samples = des[des.sum(0) == 0].index
            plt.hist(adiv.loc[adiv.index.isin(contrast_samples)][metric])
            plt.hist(adiv.loc[adiv.index.isin(healthy_samples)][metric])

            plt.tight_layout()
            #plt.xlabel("$log_{2}$(fold change)")
            #plt.ylabel(self.ct.level.name)


    def __init__(self, name, data, level: TaxonomyLevel):
        self.name = name
        self.data = data
        self.level = level
        #self.data.index = [ix.replace("_", "\\_") for ix in self.data.index]
        self.data.index.name = level.name
        self.plot = Contrast.Plot(self)

    def literature_associations(self, key, binarize=False, param="SLPV"):
        assert self.level == TaxonomyLevel.SPECIES
        L = mnemonic.literature.IMI()[key]
        L, data = L.align(self.data, join="inner", axis=0)
        L = L.dropna(thresh=int(0.25*L.shape[0]), axis=1)
        q = data[param].copy()
        if binarize:
            ix = q.abs() > q.abs().quantile(0.9)
            q[ix] = 1
            q[~ix] = 0
        o = L.corrwith(q).sort_values()
        o.name = "r"
        return o

    def ProTraits(self, param="logFC", filter="simple"):
        import mnemonic.data.ProTraits
        param = self.data["logFC"] * self.data["logCPM"]
        df = mnemonic.data.ProTraits.predictions()
        df, param = df.align(param, axis=0, join="inner")
        df = df.dropna(thresh=10, axis=1)
        #ix = df.apply(lambda x: x.dropna().value_counts()).fillna(0).min() >= 3
        #df = df.loc[:,ix]
        def fn(x):
            return mnemonic.distance.CC(x, param)
        o = df.apply(fn).T
        o.N = o.N.astype(int)
        #o = o.loc[:,["N","Ratio","AUC","rho","t","p"]]\
        #        .sort_values("p")
        if filter == "simple":
            o = o.loc[[ix for ix in o.index
                if not (("=" in ix)
                    or ("(" in ix)
                    or ("pathogenic" in ix))],:]
            #o = o.loc[[ix for ix in o.index if not ("(" in ix)],:]
        return o.sort_values("p")

class ContrastSet(dict):
    """
    Contains modeling results for a set of contrasts/parameters/LM terms
    (ideally, but not necessarily, obtained from the same dataset).
    """
    # TODO: add getitem to get single param and run on that so it doesnt need to be passed all over?

    def __init__(self, contrasts: dict, level: TaxonomyLevel, default_parameter="logFC"):
        self.level = level
        self.default_parameter = default_parameter
        contrasts = {k:Contrast(k, v, level) for (k,v) in contrasts.items()}
        super().__init__(contrasts)

    def to_frame(self, param=None):
        param = param or self.default_parameter
        ix = list(self.keys())
        X = pd.concat([self.__getitem__(k).data[param] for k in ix], axis=1, sort = True)
        X.columns = ix
        return X

    def dendrogram(self, param=None, transpose=False, metric="ward"):
        plt.clf()
        X = self.to_frame(param)
        # By default, cluster by contrasts, not OTU
        if transpose is False:
            X = X.T
        Z = scipy.cluster.hierarchy.linkage(X, metric)
        labels = [k.replace("_", " ") for k in X.columns]

        scipy.cluster.hierarchy.dendrogram(Z, labels=labels, orientation="left")
        #f = scipy.cluster.hierarchy.dendrogram(Z, labels=labels, orientation="left")
        #return f

    '''
    def bar(self, contrast, param=None, k=20):
        param = param or self.default_parameter
        plt.clf()
        ct = self.__getitem__(contrast)
        df = ct.data.loc[:,[param]].reset_index().dropna()
        df = df.sort_values(param)
        if k is not None:
            df = pd.concat([df.iloc[:k,:], df.iloc[-k:,:]])
        ax = sns.barplot(x=param, y=df.columns[0], data=df, orient="h", color="blue")
        plt.xlabel(param)
        return ax
    '''

    def clustermap(self, param=None, k=10, subsetAGP=None, groups=None, **kwargs):
        # FIXME: if a lot of features, subset features by most distinguishing
        plt.clf()
        X = self.to_frame(param)
        # FIXME: kind of a hack to figure out which axis to prune
        X = only_printable(X)

        # Remove low variance features
        var = X.var(axis=1)
        X = X.loc[var > var.quantile(0.1),:]

        # Use most discriminative features
        if k is not None:
            t = X.shape[0] > X.shape[1]
            if t:
                X = X.T
            df = mnemonic.statistics.discriminative_features(X, k=k, groups=groups)
            X = X.loc[:,df.index]
            if t:
                X = X.T

        if isinstance(subsetAGP, list):
            X = X[subsetAGP]
        # FIXME: label color key with param
        #cbar_kws={"title": param}
        return mnemonic.plot.clustermap(X, **kwargs)

    def literature_associations(self, key, **kwargs):
        assert self.level == TaxonomyLevel.SPECIES
        o = {}
        for k,ct in self.items():
            o[k] = ct.literature_associations(key, **kwargs).to_frame()
        return ContrastSet(o, self.level)

    def ProTraits(self, param=None):
        param = param or self.default_parameter
        assert self.level == TaxonomyLevel.SPECIES
        o = {}
        for k,ct in self.items():
            o[k] = ct.ProTraits(param=param)
        return ContrastSet(o, self.level)

    def cluster(self, axis=0, param=None, k=2):
        X = self.to_frame(param).fillna(0)
        if axis == 1:
            X = X.T
        model = sklearn.cluster.Birch(n_clusters=k)
        return pd.Series(model.fit_predict(X), index=X.index)

    def distance(self, query, param=None):
        """
        Return distance metrics between the selected contrast and all other
        constrasts in this :class:`ContrastSet`.
        """
        X = self.to_frame(param)
        if isinstance(query, str):
            query = X.loc[:,query]
        elif isinstance(query, pd.Series):
            pass
        else:
            raise Exception
        X, query = X.align(query, axis=0, join="inner")
        assert X.shape[0] > 3

        o = X.apply(lambda x: mnemonic.distance.CC(x, query)).T
        return o.sort_values("p")

    def compare_individual(self, o):
        # FIXME: for now, just plot a bar
        #df = self.
        raise NotImplementedError

    def compare_set(self, o, param=None):
        """
        Compare with another ContrastSet.
        """
        # FIXME: for now, just plot a clustermap,
        # maybe a contrastcomparison object or something is needed?
        assert self.level == o.level
        df1 = self.to_frame(param=param)
        df2 = o.to_frame(param=param)
        df1, df2 = df1.align(df2, axis=0, join="inner")
        r = mnemonic.statistics.corr(df1, df2)
        #r = mnemonic.statistics.spearman(df1, df2)
        #df1 = df1.apply(lambda x: (x - x.mean()) / x.std())
        #df2 = df2.apply(lambda x: (x - x.mean()) / x.std())
        #r = mnemonic.distance.pairwise(df1,df2,metric="minkowski",p=2)
        r.index = fix_index(r.index)
        r.columns = fix_index(r.columns)
        return sns.clustermap(r, cmap="RdBu_r")

    def compare(self, o, **kwargs):
        if isinstance(o, type(self)):
            return self.compare_set(o, **kwargs)
        elif isinstance(o, Contrast):
            return self.compare_individual(o, **kwargs)
        else:
            raise TypeError()
