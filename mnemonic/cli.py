import click

import mnemonic.report
import mnemonic.data.AGP

@click.group()
def cli():
    pass

@cli.command()
def precompute():
    """
    Precompute statistics for known contrasts.
    """
    print("hi")
    mnemonic.data.AGP.refresh()

@cli.command()
def report():
    """
    Produce a report with figures and tables.
    """
    mnemonic.report.main()

def main():
    cli()

if __name__ == "__main__":
    main()
