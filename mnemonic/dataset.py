import base64

import pandas as pd

import mnemonic.R
import mnemonic.statistics
import mnemonic.diversity
from mnemonic.model import *
from mnemonic.taxonomy import Taxonomy, TaxonomyLevel

class Dataset(object):
    def __init__(self, A, X, remove_low=3):
        # Iteratively remove close-to-empty samples/OTUs
        while True:
            shape = X.shape
            A, X = A.align(X, join="inner", axis=0)
            X = X.loc[(X > 0).sum(axis=1) >= remove_low,:]
            X = X.loc[:,(X > 0).sum(axis=0) >= remove_low]
            if (X.shape == shape):
                break

        A, X = A.align(X, join="inner", axis=0)

        self.A = A
        self.X = X
        self.taxonomy = Taxonomy(self.X.columns)

    def design(self):
        return self.A.copy()

    def data(self, normalize=False, level=TaxonomyLevel.SPECIES):
        o = Taxonomy.reindex(self.X, level=level)

        if normalize is True:
            o = o.apply(lambda x: x / x.sum(), axis=1)
            #o = mnemonic.statistics.pseudonormal(o)
        return o

    def multinomial_transform(self):
        pass

    def alpha_diversity(self, subunits=None):
        # Can be done over blocks, currently just per-sample
        assert subunits is None
        X = self.data(level=TaxonomyLevel.SPECIES)
        return X.apply(mnemonic.diversity.alpha, axis=1)

    """
    def fit(self, coef):
        formula = coef
        y = self.A[coef]
        ix = ~y.isnull()
        A = self.A.loc[ix,:]
        X = self.X.loc[ix,:]
        if len(y.loc[ix].unique()) < 2:
            return
        if A.shape[0] < 50:
            return
        o = wrenlab.R.wrap.edgeR.edgeR(X.T,A,formula=formula,coefficients=[coef])
        o.index = [self.taxonomy.M[ix] for ix in o.index]
        o.index.name = "OTUHash"
        return o
    """
    def fit(self, formula, level=TaxonomyLevel.SPECIES):
        print('This is broken')
        A = self.design()
        X = self.data(level=level)
        #return MFitSM.fit(formula, X, A, sm.GLM, family=sm.families.NegativeBinomial())
        #return MFitSM.fit(formula, X, A, family=sm.families.NegativeBinomial())
        return MFitEdgeR.fit(formula, X, A, family=sm.families.NegativeBinomial())

        """
        import patsy
        y = self.X.iloc[:,1]
        D = patsy.dmatrix(formula, data=self.A, return_type="dataframe")
        D,y = D.align(y, axis=0, join="inner")
        model = sm.GLM(y,D,family=sm.families.NegativeBinomial())
        return model.fit()
        """


