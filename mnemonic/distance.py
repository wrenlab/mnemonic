import sklearn.metrics as skm
import sklearn.neighbors
import scipy.stats
import pandas as pd
import numpy as np
import scipy.spatial.distance
import scipy.cluster.hierarchy

import mnemonic.util

METRICS = {
    "real": [
        "euclidean",
        "manhattan",
        "chebyshev",
        "minkowski",
        "wminkowski",
        #"seuclidean",
        "mahalanobis"
    ],
    "integer": [
        "hamming",
        "canberra",
        "braycurtis"
    ],
    "boolean": [
        "jaccard",
        "matching",
        "dice",
        "kulsinksi",
        "rogerstanimoto",
        "russellrao",
        "sokalmichener",
        "sokalsneath"
    ]
}

def optimize_clustering(X, groups=None):
    """
    Test combinations of clustering algorithm and distance metric
    against the Cophenetic distance for the given matrix `X` 
    (columns are clustered).

    Returns
    =======
    """
    # hamming, jaccard, several others exist for boolean
    # mahalanobis ?
    METRICS = [
            "euclidean", "minkowski", "cityblock", "seuclidean", "sqeuclidean", 
            "cosine", "correlation", "canberra", "braycurtis", "wminkowski",
            "mahalanobis"
    ]
    METHODS = ["single", "complete", "average", "ward", "median"]

    X = Xs = np.array(X)
    if X.shape[1] > 1000:
        ix = np.arange(X.shape[1])
        np.random.shuffle(ix)
        Xs = X[:,ix[:1000]]
    if X.shape[0] > 1000:
        ix = np.arange(X.shape[0])
        np.random.shuffle(ix)
        Xs = X[ix[:1000],:]

    def fn(metric, method):
        # This is because some metric-method combinations aren't allowed
        try:
            D = scipy.spatial.distance.pdist(Xs, metric)
            Z = scipy.cluster.hierarchy.linkage(D, method=method, metric=metric)
            c, _ = scipy.cluster.hierarchy.cophenet(Z, D)
        except ValueError:
            c = np.nan
        o = {"c": c}
        """
        if groups is not None:
            n = len(set(groups))
            model = sklearn.cluster.Birch(n_clusters=n)
            y_hat = model.fit_predict(X)
            o["AMI"] = sklearn.metrics.adjusted_mutual_info_score(groups, y_hat)
        """
        return o

    return mnemonic.util.apply_parameters(fn, parameters={"metric": METRICS, "method": METHODS})\
            .dropna()\
            .sort_values("c", ascending=False)

def pairwise(X, Y, metric="euclidean", **kwargs):
    X,Y = X.align(Y, axis=0, join="inner")
    fn = lambda x,y: sklearn.neighbors.DistanceMetric.get_metric(metric,**kwargs).pairwise(x,y)
    assert (X.shape[0] == Y.shape[0])
    nx = X.shape[1]
    ny = Y.shape[1]
    
    xi = X.columns
    yi = Y.columns
    X = np.array(X)
    Y = np.array(Y)

    o = np.zeros((nx,ny))
    for i in range(nx):
        v1 = X[:,i]
        for j in range(ny):
            v2 = Y[:,j]
            ix = ~(np.isnan(v1) | np.isnan(v2))
            if (ix.sum() < 3):
                continue
            vv1 = np.reshape(v1[ix], (1,ix.sum()))
            vv2 = np.reshape(v2[ix], (1,ix.sum()))
            r = fn(vv1,vv2)
            r = r[0,0]
            o[i,j] = r
    o = pd.DataFrame(o, index=xi, columns=yi)
    o = o.loc[:,~(o == 0).all(axis=0)]
    o = o.loc[~(o == 0).all(axis=1),:]
    return o

def BC(y: pd.Series, y_hat: pd.Series):
    """
    Return distance metrics for the comparison between `x` and `y`,
    where `y` is binary and `y_hat` is continuous.
    """
    y,y_hat = mnemonic.util.align_series(y, y_hat)
    AUC = skm.roc_auc_score(y, y_hat)
    AUC = max(AUC, 1-AUC)
    y_hat0 = y_hat.loc[y == 0]
    y_hat1 = y_hat.loc[y == 1]
    t, p = scipy.stats.ttest_ind(y_hat1, y_hat0)
    return pd.Series({
        "N": len(y),
        "Ratio": y.mean(),
        "AUC": AUC,
        "rho": scipy.stats.spearmanr(y, y_hat)[0],
        "t": t,
        "p": p
    })

def CC(y, y_hat):
    y,y_hat = mnemonic.util.align_series(y, y_hat)
    r, p = scipy.stats.pearsonr(y, y_hat)
    return pd.Series({
        "N": len(y),
        "ExplainedVariance": skm.explained_variance_score(y, y_hat),
        "MAD": skm.mean_absolute_error(y, y_hat),
        "MSE": skm.mean_squared_error(y, y_hat),
        "r2": skm.r2_score(y, y_hat),
        "r": r,
        "p": p
    })
