import pandas as pd

from mnemonic.util import memoize
from mnemonic.data.AGP import dataset

@memoize
def MI():
    DS = dataset()
    I = pd.read_csv("data/literature.direct.tsv", sep="\t", header=None)
    I.columns = ["A", "AName", "AType", "nA", "B", "BName", "BType", "nB", "nAB", "MI"]
    o = {}
    for t, df in I.groupby("BType"):
        oo = df.drop_duplicates(subset=["AName", "BName"])\
                .pivot("AName", "BName", "MI")
        oo = oo.loc[list(set(oo.index) & set(DS.taxonomy.common_name.values())),:]
        ix = (~oo.isnull()).mean() > 0.1
        o[t] = oo.loc[:,ix]
    return o

@memoize
def IMI():
    DS = dataset()
    I = pd.read_csv("data/literature.implicit.tsv", sep="\t", header=None)
    I.columns = ["A", "AName", "AType", "nA", "C", "CName", "CType", "nC", "nLinks", "MI"]
    o = {}
    I = I.query("MI > 0 & nLinks > 3")
    for t, df in I.groupby("CType"):
        oo = df.drop_duplicates(subset=["AName", "CName"])\
                .pivot("AName", "CName", "MI")
        oo = oo.loc[list(set(oo.index) & set(DS.taxonomy.common_name.values())),:]
        ix = (~oo.isnull()).mean() > 0.1
        o[t] = oo.loc[:,ix]
    return o
