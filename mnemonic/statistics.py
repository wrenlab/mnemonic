import numpy as np
import pandas as pd
import scipy.stats
import sklearn.cluster
import sklearn.feature_selection
import fancyimpute

from mnemonic.util import align_series

def align_series(x, y, dropna=True):
    """
    Align two :class:`pandas.Series` using an inner join of their indices.

    Arguments
    =========
    x, y : :class:`pandas.Series`
    dropna : bool, optional
        Whether to drop missing elements from both :class:`pandas.Series`
        objects before performing the join.

    Returns
    =======
    A 2-tuple of :class:`pandas.Series`, the aligned versions of x and y.

    """
    assert isinstance(x, pd.Series)
    assert isinstance(y, pd.Series)
    if dropna is True:
        x = x.dropna()
        y = y.dropna()

    ix = list(set(x.index) & set(y.index))
    ox, oy = x.loc[ix], y.loc[ix]
    assert len(ox) == len(oy), "Duplicate entries in index"
    return ox, oy


def corr1(X,Y):
    nx, ny = X.shape[1], Y.shape[1]
    o = np.empty((nx,ny))
    for i in range(nx):
        print(i)
        x = X.iloc[:,i]
        for j in range(ny):
            y = Y.iloc[:,j]
            xx,yy = align_series(x,y)
            if len(xx) < 5:
                continue
            o[i,j] = scipy.stats.spearmanr(xx,yy)[0]
    return pd.DataFrame(o, index=X.columns, columns=Y.columns)

def corr(X,Y):
    o = []
    for j in range(Y.shape[1]):
        o.append(X.corrwith(Y.iloc[:,j]))
    o = pd.concat(o, axis=1)
    o.columns = Y.columns
    return o

def spearman(x):
    nc = x.shape[1]
    o = np.zeros((nc,nc))
    for i in range(nc):
        for j in range(nc):
            if j > i:
                continue
            if j == i:
                o[i,j] = 0
                continue
            v1 = x.iloc[:,i]
            v2 = x.iloc[:,j]
            r = scipy.stats.spearmanr(*align_series(v1,v2))[0]
            distance = 2 * (1 - abs(r))
            o[i,j] = distance
            o[j,i] = distance
    o = pd.DataFrame(o, index=x.columns, columns=x.columns)
    o = o.loc[:,~(o == 0).all(axis=0)]
    o = o.loc[~(o == 0).all(axis=1),:]
    return o

def impute(X):
    if X.isnull().sum().sum() > 0:
        model = fancyimpute.IterativeSVD(verbose=False)
        X = pd.DataFrame(model.complete(X), index=X.index, columns=X.columns)
    return X

import heapq
import math

class Heap(object):
    def __init__(self):
        self._items = []

    def push(self, value, priority):
        heapq.heappush(self._items, (priority, value))

    def pop(self):
        return heapq.heappop(self._items)[1]

    def __bool__(self):
        return bool(self._items)

class EqualKMeans(object):
    def __init__(self, n_clusters=2):
        self.n_clusters = n_clusters

    def fit_predict(self, X):
        # Based on:
        # https://stackoverflow.com/questions/8796682/group-n-points-in-k-clusters-of-equal-size
        # but with simpler M step
        X = np.array(X)
        nr = X.shape[0]
        max_items = math.ceil(nr / self.n_clusters)

        def fn(centroid):
            o = np.empty(nr)
            o[:] = -1
            D = scipy.spatial.distance.cdist(X, centroid)

            heap = Heap()
            best = np.argsort(D)[:,0]
            for i,c in enumerate(best):
                distance = D[i,c]
                heap.push((i,c), distance)
            while heap:
                i,c = heap.pop()
                dx = D[i,:].copy()
                while (o == c).sum() == max_items:
                    dx[c] = np.finfo(D.dtype).max
                    c = np.argsort(dx)[0]
                o[i] = c
            return o

        # initialize
        np.random.seed(0)
        centroid = X[np.random.choice(np.arange(nr), size=self.n_clusters, replace=False),:]
        o = fn(centroid)

        # EM
        n_iter = 100
        for iteration in range(n_iter):
            for c in range(self.n_clusters):
                centroid[c,:] = X[o == c,:].mean(axis=0)
            prev = o
            o = fn(centroid)
            if (o == prev).all():
                return o
        return o


def fn(X):
    model = EqualKMeans(n_clusters=2)
    return model.fit_predict(X)

def discriminative_features(X, k=None, groups=None):
    """
    Find the k most discriminative features partitioning the index in X 
    into groups.
    """
    X = impute(X)
    n = X.shape[0]
    groups = groups or 2
    #model = sklearn.cluster.Birch(n_clusters=groups)
    model = EqualKMeans(n_clusters=groups)
    y = pd.Series(model.fit_predict(X), index=X.index)
    F, p = sklearn.feature_selection.f_classif(X, y)
    r = X.corrwith(y)
    o = pd.DataFrame({
        "F": F, 
        "p": p,
        "r": r
    }, index=X.columns)
    if k is not None:
        o_up = o.query("r > 0").sort_values("p").index[:k]
        o_down = o.query("r < 0").sort_values("p").index[:k]
        ix = set(o_up) | set(o_down)
        o = o.loc[ix,:]
    return o.sort_values("p")

def pseudonormal(X, margin=0.01):
    X = X.apply(lambda x: x / x.sum(), axis=1)
    X = margin + (X * (1 - 2 * margin))
    return X.apply(scipy.stats.norm.ppf)
