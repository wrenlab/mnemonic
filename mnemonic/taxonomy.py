"""
.. module: taxonomy

taxonomy
--------------------------------------------------------------------------------

"""

import enum
import re
import collections
import hashlib

import bidict
import pandas as pd

class TaxonomyLevel(enum.Enum):
    KINGDOM = "k"
    PHYLUM = "p"
    CLASS = "c"
    ORDER = "o"
    FAMILY = "f"
    GENUS = "g"
    SPECIES = "s"

class Taxonomy(object):
    ORDERING = collections.OrderedDict([
        ("k","Kingdom"), ("p", "Phylum"), ("c", "Class"),
        ("o", "Order"), ("f", "Family"), ("g", "Genus"), ("s", "Species")
    ])

    def __init__(self, otus):
        self.M = bidict.bidict({k:"T"+hashlib.md5(k.encode("utf-8")).hexdigest() for k in otus})
        self.level_map = bidict.bidict({x.value:x.name for x in TaxonomyLevel.__members__.values()})

        self.common_name = bidict.bidict()
        self.level = {}
        ix, frame = [], []
        for k,v in self.M.items():
            level, t = self._parse_otu(k)
            if ("g" in t) and ("s" in t):
                common_name = t["g"] + " " + t["s"]
                try:
                    self.common_name[v] = common_name
                except bidict.ValueDuplicationError:
                    pass
            self.level[v] = self.level_map[level]
            ix.append(self.M[k])
            frame.append([t.get(k) for k in self.ORDERING.keys()])

        self.frame = pd.DataFrame.from_records(frame, index=ix)
        self.frame.columns = [self.level_map[k] for k in self.ORDERING.keys()]
        self.frame.index.name = "OTUHash"

        # TODO: create graph?

    def to_frame(self):
        return self.frame.copy()

    @staticmethod
    def reindex(X, reducer=lambda df: df.sum(), level=TaxonomyLevel.SPECIES):
        """
        Reindex a DataFrame from QIIME format to the given TaxonomyLevel.
        """
        X = X.copy()
        taxonomy = Taxonomy(X.columns)
        X.columns = [taxonomy.M[ix] for ix in X.columns]

        # FIXME: assumes all units on all levels are unique

        levels = [level.name]
        if level == TaxonomyLevel.SPECIES:
            levels.insert(0, TaxonomyLevel.GENUS.name)
        taxonomy = taxonomy.to_frame().dropna(subset=levels)
        groups = pd.Series([" ".join(row) for row in taxonomy.loc[:,levels].to_records(index=False)],
                index=taxonomy.index)
        X = X.loc[:,groups.index]

        ix,o = [],[]
        for k, df in X.T.groupby(groups):
            # FIXME: need flexible reducer
            x = reducer(df)
            x.name = k
            o.append(x)
        o = pd.DataFrame(o)
        o.index.name = level.name
        return o.T

    def _parse_otu(self, otu):
        o = {}
        for chunk in re.split(";\s?", otu):
            k,v = chunk.split("__")
            v = v.replace("[","").replace("]","")
            if v:
                o[k] = v
        for k in self.ORDERING:
            level = k
            if not k in o:
                break
        return level, o
