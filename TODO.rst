Workflow
========

1. Fetch data & parse

2. Preprocess, [DONE] collapse

3a. Contrast matrices: logFC, p, t/stat, (or distance) per OTU
[DONE, in mnemonic.contrast] Core data type: OTU vs term/contrast/binary comparison

3b. Sample or sample batch (experiment) descriptive metrics
- [DONE] alpha diversity
- beta diversity

3c. Sample-sample distance metrics/matrix
- "Perturbation" (in "skbio.composition" module probably)
- delta diversity (delta any sample descriptive)

Queries
=======

User-provided or DB dataset
---------------------------
Input: matrix + metadata + formula OR matrix + binary contrast vector

Allow creation of Dataset object by user

Contrast results (without reference to EBI/AGP, basic "DE")
- [DONE] logFC/p/t per OTU

DEseq-style plots: 
- [DONE] DE bar
- [DONE] MA plot
- [DONE] volcano

Similar AGP (maybe EBI) contrasts

Similar EBI samples/experiments

Contrast-centric query:
-----------------------
Given a contrast vector (from user or from precomputed AGP), provide:

[DONE] Similar contrasts
[DONE EXCEPT NOT WITH QUERY] Hierarchical clustering (highlight/different color for query contrast)
[DONE] Contrast vector "enrichment" / annotation methods
- Literature

OTU-centric query:
------------------

Which contrasts is this OTU most/least changed?
[DONE, BUT NEEDS TWEAKING / BETTER METRICS] Correlated or anti-correlated OTUs by "expression" behavior

Possible additional plots
=========================

Pie chart for OTU abundances
PCA / t-SNE / DR
Hive (for something...)

Other / bonus
============

[DONE]
Categorize AGP metadata; tentatively:
- Dietary
- Physical disease
- Mental disease
- Allergies
- Demographic (age, weight, etc.)

[DONE] SUBSET GUT ONLY FOR AGP

Repeat above with sample vs GO AGP/EBI data instead of sample vs OTU

Test distance metrics for contrast similarity

EBI label extraction

[DONE] In above, "OTU"-based analyses should be stratified by level

Batch correction

Summary / annotation / EA for species
- [DONEISH] literature
- [DONE] ProTraits

[DONE] To plot heatmaps, select k most discriminative features.

Analyses
========

- hclust / similarity plot / PCA / heatmap of terms/contrasts
- [PROTOTYPE DONE] literature based analysis, e.g. drug x contrast, pathway x contrast, etc
- validation: OTUxOTU or term x term similarities, EBI vs IRIDESCENT
- case study(s)

Case Study
==========

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3751348/
With age:
- Increase in anaerobes, decrease in aerobes
- Decrease in microbial diversity

References
==========

Modeling:
- https://arxiv.org/pdf/1603.00974.pdf
- https://www.caister.com/cimb/v/v24/17.pdf

Technical
=========

Finish M stage of EqualKMeans
